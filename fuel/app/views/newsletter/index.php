
<br>
<br>

   <section class="section-preview">  
       <div class="row">

           <div class="col-md-12">
      <div class="btn-group btn-group-sm" role="group" style="margin-left: 10px;">
      <button onclick="sendMail()"  class="btn btn-info btn-sm"   >
                             <span>Send&nbsp;Mail</span>
       </button>
    
      <button onclick="showLog()"  class="btn btn-info btn-sm btn-rounded" style="display: none;"  >
                             <span>Show&nbsp;Log</span>
       </button>
      </div>
           </div>
       </div>
   </section>

<section class="section-preview">  
    <div class="row"> 
     <div class="col-lg-5 col-md-10">
 
         <select class="custom-select custom-select-sm" id="adv_id" style="margin_left: 20px;"  >
          <option value=""  selected id ="0" >No advertiser selected</option>
          <?php  foreach($advertisers as $advertiser):  ?>
             <option value="<?php echo $advertiser['platform_id']?>"  id="<?php echo $advertiser['id']?>" ><?php echo $advertiser['title'];?> </option> 
           <?php endforeach;?> 
      </select>
  
 
         <input id="offer_box" class="form-control form-control-sm" placeholder="Filter Offer Ids" >
         <input id="offer_name_box" class="form-control form-control-sm" placeholder="Filter Offer Name" >
     
    
 </div>
     
     <div class="col-lg-5 col-md-10" >

         <input id ="fltr_cntr" placeholder="Filter Country"  class="form-control form-control-sm"  />
         <input id="fltr_os" placeholder="Filter Os"  class="form-control form-control-sm"  />
  
 </div>
  </div>
    <div class="btn-group btn-group-sm" role="group" style="margin-left: 10px;">
 
         <button onclick="onClickSearch()"  class="btn btn-info btn-sm"  style="margin-top:20px;margin-right: 20px;" >
                         <span>Search</span>
      </button> 
     <div> 
         <button   class="btn btn-info btn-sm"  style="margin-top:20px;" data-toggle="modal" data-target="#modalPartner" >
                         <span>Add&nbsp;Partner</span>
         </button>
        
     </div>
    </div>
    <!-- Modal -->
<div class="modal fade" id="modalPartner" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Add New Partner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" value ="" id="adv" class="form-control validate">
          <label data-error="wrong" data-success="right" for="adv">Advertiser/Affiliate name</label>
        </div>
        <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" value ="" id="aff_id" class="form-control validate">
          <label data-error="wrong" data-success="right" for="aff_id">Advertiser/Affiliate Id</label>
        </div>  
        <div class="md-form mb-5">
          <i class="fas fa-user prefix grey-text"></i>
          <input type="text" value ="" id="contact" class="form-control validate">
          <label data-error="wrong" data-success="right" for="contact">Contact name</label>
        </div>  

         <div class="md-form mb-4">
          <i class="fas fa-envelope prefix grey-text"></i>
          <input type="email" value ="" id="mail" class="form-control validate">
          <label data-error="wrong" data-success="right" for="mail">Email</label>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button onclick="addPartner()" type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

    <!-- Modal Edit-->
<div class="modal fade" id="modalEdit" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true" >
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Edit Partner</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
         <input type="text" value ="" id="id" class="form-control" style="display: none;">
          <label  for="adv" style="color: #01579b">Advertiser/Affiliate name</label>
          <input type="text" value ="" id="adv" class="form-control"></br>

          <label    for="aff_id" style="color: #01579b">Advertiser/Affiliate Id</label>
          <input type="text" value ="" id="aff_id" class="form-control"></br>
  
           <label    for="contact" style="color: #01579b">Contact name</label>
           <input type="text" value ="" id="contact" class="form-control"></br>
 
          <label    for="email" style="color: #01579b">Email</label>
          <input type="email" value ="" id="mail" class="form-control"></br>
   
          <div class="custom-control custom-checkbox">
    <input type="checkbox" class="custom-control-input" id="subscribed">
    <label class="custom-control-label" for="subscribed" style="color: #01579b">Subscribed</label>
    </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button onclick="updatePartner()" type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal -->

</section>

<div class="card-group">
<div class="card" style="flex-grow: 1.3">
     <!--<div style="width: 90%; margin-left: 10px;"><h2>Partners</h2></div>-->
     <h4 class="card-title">Partners</h4>
  
  <div class="card-body">

  <!--<div class="px-4 ">-->
  
<div class="table-wrapper-scroll-y my-custom-scrollbar" >
    
      <!-- Table  -->
      
<table class="table  table-bordered table-sm " id="tbl_partners"  >
  <!-- Table head -->
  <thead>
          <tr class="d-flex">
            <th class="col-1">#</th>
            <th class="col-3">Title</th>
            <th class="col-7">E-mail</th>
            <th class="col-1">Edit</th>
           </tr>
   </thead>
  <!-- Table head -->

  <!-- Table body -->
  <tbody >
      <?php  foreach($partners as $partner):  ?>
      <tr class="d-flex">
      <th scope="row" class="col-1">
        <!-- Default unchecked -->
        <div class="custom-control custom-checkbox"  >
          <input type="checkbox" class="custom-control-input" value="<?php echo $partner['id']?>" id="partner<?php echo $partner['id']?>"  >
          <label class="custom-control-label" for="partner<?php echo $partner['id']?>" ></label>
        </div>
      </th>
      <td  class="col-3" id="info_<?php echo $partner['id']?>"  ><?php echo $partner['title'] ?></td>
      <td  class="col-7" style="background-color:<?php if($partner['subscribed'] == 1) echo '#c3e6cb'; else echo '#ffeeba' ?>" ><?php echo $partner['email'] ?></td>

      <td class="col-1">
          <a href="#!" class="indigo-text"  id="<?php echo $partner['title'].'|'.$partner['id'].'|'.$partner['subscribed'].'|'.$partner['email'].'|'.$partner['platform_id'].'|'.$partner['contactname'];?>"  
                      data-toggle="modal" data-target="#modalEdit" onclick="EditPartner(this.id)">edit</a></span>
       </td>
    </tr>
     <?php endforeach;?>  
  </tbody>
  <!-- Table body -->
</table>
<!-- Table  -->
 


    </div>

  </div>

</div>
    
<div class="card" style='float:right;  width:45%; margin-left: 20px;' >
     <div style="width: 90%; margin-left: 10px;"><h2>Offers</h2></div>

  
  <div class="card-body ">

    <div class="table-wrapper-scroll-y my-custom-scrollbar ">
      <!-- Table  -->
      
<table class="table  table-bordered table-sm" id="tbl_offers"  >
  <!-- Table head -->
  <thead >
          <tr class="d-flex">
            <th class="col-1">#</th>
            <th class="col-3">Id</th>
            <th class="col-7">Title</th>
           </tr>
        </thead>
  <!-- Table head -->

  <!-- Table body -->
  <tbody id="offer_body">
      <?php  foreach($offers as $offer):  ?>
    <tr class="d-flex" id="offer<?php echo $offer['platform_entity_id']?>">
      <th scope="row" class="col-1">
        <!-- Default unchecked -->
        <div class="custom-control custom-checkbox" >
            <input type="checkbox" class="custom-control-input" value="<?php echo $offer['platform_entity_id']?>" id="off_<?php echo $offer['platform_entity_id']?>" onclick="updateSelected(this)">
          <label class="custom-control-label" for="off_<?php echo $offer['platform_entity_id']?>"></label>
        </div>
      </th>
      <td class="col-3" ><?php echo $offer['platform_entity_id'] ?></td>
      <td class="col-7"><?php echo $offer['title'] ?></td>
      
    </tr>
     <?php endforeach;?>  
  </tbody>
  <!-- Table body -->
</table>
<!-- Table  -->  
    </div>
</div>
</div>
    
    
    
    
<div class="card" style="float:right;   width:45%; margin-left: 20px; font-size: 8px;" >
     <div style="width: 90%; margin-left: 10px;"><h2>Selected</h2></div>

  
  <div class="card-body ">

 <div class="table-wrapper-scroll-y my-custom-scrollbar ">
      <!-- Table  -->
      
<table class="table  table-bordered table-sm table-condensed" id="tbl_selected"  >
  <!-- Table head -->
  <thead >
          <tr class="d-flex">
            <th class="col-1 ">#</th>
            <th class="col-3 ">Id</th>
            <th class="col-7 ">Title</th>
           </tr>
        </thead>
  <!-- Table head -->

  <!-- Table body -->
  <tbody id="selected_body" >
    
  </tbody>
  <!-- Table body -->
</table>
<!-- Table  -->
 


    </div>

  <!--</div>-->

</div>
</div>    
</div>
<script type="text/javascript">
   var selectedOffers = []; 
   
   function onClickSearch() {
       let  offerid =  $("#offer_box").val();
       let country =$('#fltr_cntr').val();
       let os = $('#fltr_os').val();
       let offername =  $("#offer_name_box").val();
       let advertiser = $("#adv_id option:selected").attr('value');
       

   var postdata = {};
   if(!offerid && !os && !country && !advertiser && !offername)
         {
             alert ("Please select at least one parameter");
             return;
         }
         else
         {
              postdata['offerid'] =offerid;
              postdata['os'] =os;
              postdata['country'] = country;
              postdata['advertiser'] = advertiser;
              postdata['offername'] = offername;
               $.ajax({
                    url: "/newsletter/search",
                    type: "POST",
                    data:   postdata,
                    dataType: "json",
                    complete: function(data) { //console.log(data);
                       $("#tbl_offers > tbody").html("");
                       $.each(data.responseJSON,function(i,row)
                       {
                          // console.log(row);
                           var tr=  '<tr class="d-flex" id="offer'+ row.platform_entity_id +'">\n';
                           tr+= '<th scope="row" class="col-1">\n';
                           tr+= '<div class="custom-control custom-checkbox" >\n';
                           tr+= '<input type="checkbox" class="custom-control-input" value="'+ row.platform_entity_id +'" id="off_'+ row.platform_entity_id +'" onclick="updateSelected(this)">\n';
                           tr+= ' <label class="custom-control-label" for="off_'+ row.platform_entity_id +'"></label>\n';
                           tr+='</div>\n</th>\n';
                           tr+='<td class="col-3">'+ row.platform_entity_id +'</td>\n';
                           tr+= '<td class="col-7">'+ row.title +'</td>\n';
                           tr+= '</tr>\n';
//                           console.log(tr);
                           $('#tbl_offers tbody').prepend(tr);
                       }
                       )
                    }
                });
         }
 
// this.wait_ball.nativeElement.style.display = "inline";
//// console.log(this.wait_ball.nativeElement);
//   let baseOffers = this.restangular.all('offer');
//		baseOffers.customGET("searchNl",filterParams).subscribe(offers => {
//                    console.log(offers);
//                    this.offers = offers;
//                    this.wait_ball.nativeElement.style.display  = "none";
//                  //  console.log(this.wait_ball.nativeElement);
//    });  
   }
   
function addPartner(){
postdata={};

       let  advertiser =  $("#modalPartner #adv").val();
       let contact =$('#modalPartner #contact').val();
       let mail = $('#modalPartner #mail').val();
       let platform =$('#modalPartner #aff_id').val();
       postdata['advertiser'] = advertiser;
              postdata['contact'] = contact;
              postdata['platform'] = platform;
              postdata['mail'] = mail;
              
               $.ajax({
                    url: "/newsletter/addpartner",
                    type: "POST",
                    data:   postdata,
                    dataType: "json",
                    complete: function(data) { 
                       
                       $("#modalPartner #adv").val("");
                       $('#modalPartner #contact').val("");
                       $('#modalPartner #mail').val("");
                       $('#modalPartner #aff_id').val("");
                       $('#modalPartner').modal('hide'); 
                       location.reload();
                 
                    }
    });
}

    function ResetSelected(){
//        console.log($("#adv_id option:selected").attr('value'));
        if($("#adv_id option:selected").attr('value') != "") $("#adv_id option:selected").val("");
    }
    function EditPartner(row){
        
//   console.log(row);
   
    var arr =row.split('|');
//    console.log(arr);
    $("#modalEdit #adv").val(arr[0]);
    $("#modalEdit #aff_id").val(arr[4]);
    $("#modalEdit #contact").val(arr[5]);
    $("#modalEdit #mail").val(arr[3]);
    if(arr[2] == 1)$("#modalEdit #subscribed").prop("checked", true); else $("#modalEdit #subscribed").prop("checked", false);
    $("#modalEdit #id").val(arr[1]);
    }
    
   function updatePartner(){
   
    postdata={};

       let  advertiser =  $("#modalEdit #adv").val();
       let contact =$('#modalEdit #contact').val();
       let mail = $('#modalEdit #mail').val();
       let platform =$('#modalEdit #aff_id').val();
       let id = $('#modalEdit #id').val();
              postdata['advertiser'] = advertiser;
              postdata['contact'] = contact;
              postdata['platform'] = platform;
              postdata['mail'] = mail;
              postdata['id'] = id;
              postdata['subscribed'] = $("#modalEdit #subscribed").prop("checked");
              
                $.ajax({
                    url: "/newsletter/updatepartner",
                    type: "POST",
                    data:   postdata,
                    dataType: "json",
                    complete: function(data) { 
                        //console.log(data); 
                       if(data.responseText == '500') alert("email address "+mail+" already exists. Pls enter another e-mail address."); 
                       else{
                           $("#modalEdit #adv").val("");
                           $('#modalEdit #contact').val("");
                           $('#modalEdit #mail').val("");
                           $('#modalEdit').modal('hide'); 
                           location.reload();
                       }
                    }
                });
       }
   
   
      
      function unselectSelected(status){
       
     var s=status.id.replace("off_","");
     $("#offer" + s ).children('td').each(function(i) {
          if(i == 0) id = $(this).html();
          if(i == 1) name = $(this).html();
         
        });
//        console.log(name + id);
      if(status.checked){
        var found = 0;
        for( var i = 0; i < selectedOffers.length; i++){ 
//            console.log(name + id);
               if ( selectedOffers[i].platform_entity_id === id) {
                 found =1;
               }
        }  
        if (!found) selectedOffers.push({platform_entity_id : id , title: name});
      } 
      else{
        //selectedOffers.({platform_entity_id : id , title: name});
        
        for( var i = 0; i < selectedOffers.length; i++){ 
               if ( selectedOffers[i].platform_entity_id === id) {
                 selectedOffers.splice(i, 1); 
               }
        }
      }
//      console.log(selectedOffers);
      
    }
    
    
    function updateSelected(status){
       
     var s=status.id.replace("off_","");
     $("#offer" + s ).children('td').each(function(i) {
          if(i == 0) id = $(this).html();
          if(i == 1) name = $(this).html();
         
        });
//        console.log(name + id);
      if(status.checked){
        var found = 0;
        for( var i = 0; i < selectedOffers.length; i++){ 
               if ( selectedOffers[i].platform_entity_id === id) {
                 found =1;
               }
        }  
        if (!found) selectedOffers.push({platform_entity_id : id , title: name});
      } 
      else{
        //selectedOffers.({platform_entity_id : id , title: name});
        
        for( var i = 0; i < selectedOffers.length; i++){ 
               if ( selectedOffers[i].platform_entity_id === id) {
                 selectedOffers.splice(i, 1); 
               }
        }
      }
//      console.log(selectedOffers);
      $("#tbl_selected > tbody").html("");
                       $.each(selectedOffers,function(i,row)
                       {
                          // console.log(row);
                           var tr=  '<tr class="d-flex" id="sel'+ row.platform_entity_id +'">\n';
                           tr+= '<th scope="row" class="col-1">\n';
                           tr+= '<div class="custom-control custom-checkbox" >\n';
                           tr+= '<input type="checkbox" class="custom-control-input" value="'+ row.platform_entity_id +'" id="off_'+ row.platform_entity_id +'" checked onclick="unselectSelected(this)">\n';
                           tr+= ' <label class="custom-control-label" for="off_'+ row.platform_entity_id +'"></label>\n';
                           tr+='</div>\n</th>\n';
                           tr+='<td class="col-3">'+ row.platform_entity_id +'</td>\n';
                           tr+= '<td class="col-7">'+ row.title +'</td>\n';
                           tr+= '</tr>\n';
                           //console.log(tr);
                           $('#tbl_selected tbody').prepend(tr);
                       }
                       );
    }
    
    function sendMail(){
    var offers = new Array();
    var partners = new Array();

    $('#tbl_selected').find('tr').each(function () {
   
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
  
          //console.log($(this).find('input[type="checkbox"]').val());
          offers.push($(this).find('input[type="checkbox"]').val());
        }
    });
    
     $('#tbl_partners').find('tr').each(function () {
   
        if ($(this).find('input[type="checkbox"]').is(':checked')) {
  
//          console.log($(this).find('input[type="checkbox"]').val());
          partners.push($(this).find('input[type="checkbox"]').val());
        }
    });
         var postdata = {};
        
         if(partners === undefined || partners.length == 0 || offers ===  undefined || offers.length == 0)
         {
             alert ("Please select at least one partner/offer");
             return;
         }
         else
         {
              postdata['offers'] =JSON.stringify(offers);
              postdata['partners'] =JSON.stringify(partners);
              

               $.ajax({
                    url: "/newsletter/sendmail",
                    type: "POST",
                    data:   postdata,
                    dataType: "json",
                    complete: function(data) { //console.log(data.responseText);
                      if (data.responseText == 'true') {
                          alert("Mail has been sent");
                          location.reload();
                      }
                      else  alert("Mail has not been sent. try again.");
     
                    }
                });
         }
   
       };
    
  
</script>