<!DOCTYPE html> 
<html lang = "en"> 
   <head> 
<!--      <meta charset = "utf-8"> 
      <meta http-equiv = "X-UA-Compatible" content = "IE = edge"> -->
      <!--<meta name = "viewport" content = "width = device-width, initial-scale = 1,  shrink-to-fit=no">-->  
      
      
      
    <!-- Font Awesome -->
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css">
<!-- Bootstrap core CSS -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/css/bootstrap.min.css" rel="stylesheet">
<!-- Material Design Bootstrap -->
<link href="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.5/css/mdb.min.css" rel="stylesheet">

<style>
    .my-custom-scrollbar {
  position: relative;
  height: 600px;
  overflow: auto;
  overflow-y: auto;
}
.pt-3-half {
padding-top: 1.4rem;
}
.table-wrapper-scroll-y {
  display: block;
}
table.table td, table.table th {   padding-top: .1rem;   padding-bottom: .1rem;}
.form-check-input[type=checkbox]+label, 
label.btn input[type=checkbox]+label {

height: .1rem;
line-height: 1.5;
}
.form-check-input[type=checkbox]+label:before, 
.form-check-input[type=checkbox]:not(.filled-in)+label:after, 
label.btn input[type=checkbox]+label:before, 
label.btn input[type=checkbox]:not(.filled-in)+label:after {

width: 10px;
height: 10px;
border: 1px solid #8a8a8a;
}
</style>
   </head>  
   
   <body> 
      <header>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark  primary-color scrolling-navbar">
        
        <a class="navbar-brand" href="#"><strong><?php echo $m ?></strong></a>
        <button class="navbar-toggler" 
                type="button" 
                data-toggle="collapse" 
                data-target="#navbar" 
                aria-controls="navbar" 
                aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon">$mode</span>
        </button>
        <div class="collapse navbar-collapse" id="navbar">
            <ul class="navbar-nav mr-auto">
                <!--<li class="nav-item active"><a class="nav-link waves-effect waves-light" href="/general/index">  Home<span class="sr-only">(current)</span></a></a></li>-->
                <li class="nav-item active"><a class="nav-link waves-effect waves-light" href = "/newsletter/show">  Newsletter</a></li> 
                
            </ul>
            
        </div>
    </nav>

</header>

 
      <div class = "container"> 
         <div style="padding-top: 50px;  padding-right: -100px;  padding-bottom: 50px;  padding-left: 20px; margin-left:-300px;" > 
             
            <?php  echo $content; ?> 
         </div> 
      
      </div><!-- /.container --> 

   <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- Bootstrap tooltips -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.4/umd/popper.min.js"></script>
<!-- Bootstrap core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.3.1/js/bootstrap.min.js"></script>
<!-- MDB core JavaScript -->
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mdbootstrap/4.7.5/js/mdb.min.js"></script>

 </body>

   
</html>