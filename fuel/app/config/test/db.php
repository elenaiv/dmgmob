<?php
/**
 * The test database settings. These get merged with the global settings.
 *
 * This environment is primarily used by unit tests, to run on a controlled environment.
 */

return array(
	'default' => array(
		'connection'  => array(
			'dsn'        => 'postgres:host=35.245.181.78;dbname=dmg_xcng',
			'username'   => 'dmg',
			'password'   => 'd5nrmg',
		),
	),
);

