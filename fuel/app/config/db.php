<?php
/**
 * The production database settings. These get merged with the global settings.
 */

return array(
	'default' => array(
            'type' => 'pdo',
		'connection'  => array(
                 'dsn'            => 'pgsql:host=35.245.181.78;dbname=dmg_xcng',
                 'username'   => 'dmg',
                 'password'   => 'd5nrmg',
		),
            'identifier'     => '"',
            'table_prefix'   => '',
            'charset'        => 'utf8',
            'enable_cache'   => true,
            'profiling'      => false,
	),
);