<?php
return array(
	'_root_'  => 'newsletter/show',  // The default route
	'_404_'   => 'newsletter/404',    // The main 404 route
	'/' => 'newsletter/show',
	'newsletter/search' => 'newsletter/search', 
        'search' => 'newsletter/search',
        'email' => 'partnermail/email',
        'integration' => 'integration'

);
