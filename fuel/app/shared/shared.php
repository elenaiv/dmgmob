<?php
 
 
 define('DEFAULT_ICON' , 'http://35.245.181.78:9000/images/logo.png');
 
 function _getAppDailyLimit($affiseOffer,$address){
         $dLimit = -1;
        if (isset($address['platform_id']) && !is_null($address['platform_id']) && strlen($address['platform_id']) < 10 & !empty($address['platform_id'])){
           $dLimit = _getPartnerCapByOfferId($affiseOffer,$address['platform_id']);
         }
          If($dLimit > -1 ) return $dLimit;
        if(isset($affiseOffer->caps) && !empty($affiseOffer->caps) && isset($affiseOffer->caps[0])  ){
            return($affiseOffer->caps[0]->value);
          }
         else return null;
    }

    function _getPartnerPayoutByOfferId($offer, $platformId){
        $payout = -1;

        foreach ($offer->partner_payments as $_partnerPayment){

            if (in_array($platformId, $_partnerPayment->partners) === true ){
                 $payout =  $_partnerPayment->revenue;
                 break;
            }
        }
      return $payout;
    }
    
    function _getPartnerCapByOfferId($offer, $platformId){
        $dailyLimit = -1;
     
        foreach ($offer->caps as $_cap){
            if (in_array($platformId, $_cap->affiliates) === true ){
                   $dailyLimit =  $_cap-> value;
                   break;
            }
        }
          return $dailyLimit;
    }
    
 function _createAppOffersArray($offers,$address){

       $_arrOffers = array();

       foreach($offers as $offer){
        
           $tmpArr = array(
               "id" => $offer->id,
               "offer_id" => $offer->offer_id,
               "title" => $offer->title,
               "platform" => _getPlatform($offer->payments),
               "payout" => _getAppPayout($offer,$address),
               "countries" => count($offer->countries) > 4 ? "Worldwide" : json_encode($offer->countries),
               "preview_url" => $offer->preview_url,
               "dailylimit" => _getAppDailyLimit($offer,$address),
               "request_url" => "http://dsnrmg.affise.com/offer/{$offer->id}"
           );
           if (isset($offer->logo) && ($offer->logo != '')){
               $tmpArr['logo'] =  "http://offers.dsnrmg.affise.com{$offer->logo}";
            }
            else {
                $tmpArr['logo'] = _getLogo($offer->preview_url);
            }
            $_arrOffers[] = $tmpArr;
        }
        return $_arrOffers;
    }
    
      function _getAppPayout($affiseOffer,$address){
        $payout = -1;
        if (isset($address['platform_id']) && !is_null($address['platform_id']) && strlen($address['platform_id']) < 10 & !empty($address['platform_id'])){
            $payout = _getPartnerPayoutByOfferId($affiseOffer,$address['platform_id']);
        }
        If($payout > -1 ) return $payout;
        return $affiseOffer->payments[0]->revenue;
    }
    
    
     function _getLogo($previewUrl){
       
        if (!isset($previewUrl)){

            return DEFAULT_ICON;
        }
        try{
            $content = file_get_contents($previewUrl);
           
        } catch(Exception $e){

            return DEFAULT_ICON;
        }

        if ($content == FALSE || $content == NULL || $content == false){

            return DEFAULT_ICON;
        }
        if(stristr($previewUrl, "play.google.com")){

            $arr = explode('div class="dQrBL">', $content);
           
            if(isset($arr) && count($arr) > 1){
                $tmpStr = $arr[1];
            }
            else{

                return DEFAULT_ICON;
            }

            $posEnd = strpos($tmpStr, " srcset=");
            $posStart = strpos($tmpStr, "<img src=");

            $return = substr($tmpStr, $posStart + 10, $posEnd - $posStart -11);
            if (strlen($return) > 0){

                return $return;
            }
            else{

                return DEFAULT_ICON;
            }
        }
        elseif(stristr($previewUrl, "itunes.apple.com")){
            $arr = explode('class="we-artwork ember-view product-hero__artwork we-artwork--fullwidth we-artwork--ios-app-icon"', $content);
            if(isset($arr) && count($arr) > 0){
                $tmpStr = $arr[1];
            }
            else{

                return DEFAULT_ICON;
            }
            $posEnd = strpos($tmpStr, " 1x");
            $posStart = strpos($tmpStr, " srcset=");

            $return = substr($tmpStr, $posStart + 9, $posEnd - $posStart -9);
            if (strlen($return) > 0){

                return $return;
            }
            else{

                return DEFAULT_ICON;
            }
        }
        else{

            return DEFAULT_ICON;
        }
    }
    
     function _getPlatform($payments) {
    
        $dev = implode(",",$payments[0]->devices);
        $os =implode(",",$payments[0]->os);
        $return = (isset($dev) && isset($os) && !empty($dev) && !empty($os)) ? $dev. " ( ".$os. " ) " : '';
        return $return;
    }



