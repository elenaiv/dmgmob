<?php
namespace Model;
class Offer extends \Model
{
     public static function fetchAll($mode) { 
         switch ($mode){
             case 'app':
                 $table= 'offer';
                 break;
             case 'dmi':
                 $table= 'offer_dmi';
                 break; 
            case 'mob':
                 $table= 'offer_dmgmob';
                 break;             
         }                   // Code to fetch employee from database 
           $query = \Fuel\Core\DB::query("SELECT * FROM ".$table." where status = 'active' order by id desc limit 200");
         
            $res=$query->execute();

            return $res->as_array();
      } 
      
      public static function fetchByQueryString($q) { 
         
         // Code to fetch employee from database 
           $query = \Fuel\Core\DB::query($q);
         
            $res=$query->execute();

            return $res->as_array();
      } 
      
      public static function getOfferByOfferId($offerId, $mode){
          
          switch ($mode){
             case 'app':
                 $table= 'offer';
                 break;
             case 'dmi':
                 $table= 'offer_dmi';
                 break; 
            case 'mob':
                 $table= 'offer_dmgmob';
                 break;             
         }                   // Co
          $q="SELECT * from ".$table." WHERE platform_entity_id in (".$offerId.")" ;
          $query = \Fuel\Core\DB::query($q);
         
            $res=$query->execute();

            return $res->as_array();
      }
   }


