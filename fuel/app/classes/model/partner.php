<?php
namespace Model;
class Partner extends \Model
{
     public static function fetchAll($mode) { 
         switch ($mode){
             case 'app':
                 $table= 'partner';
                 break;
             case 'dmi':
                 $table= 'partner_dmi';
                 break; 
            case 'mob':
                 $table= 'partner_dmgmob';
                 break;             
         }  
         // Code to fetch employee from database 
           $query = \Fuel\Core\DB::query("SELECT * FROM ".$table." where status = 'active' order by id desc");
         
            $res=$query->execute();

            return $res->as_array();
      } 
   }


