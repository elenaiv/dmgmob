<?php



namespace Model;
class Affisetrafficback extends \Model{
public static function fetchAllOffers($mode) { 
        
           $query = \Fuel\Core\DB::query("SELECT * FROM trafficback where status = 'active' and affise_mode = '".$mode."' order by id desc"  );
         
            $res=$query->execute();

            return $res->as_array();
      } 
public static function add($mode,$id,$country,$status,$payout,$conversions) { 

           $q =   "INSERT INTO trafficback  (affise_mode,offer_id,country, status, payout,conversions) "
                   . "VALUES('".$mode."','".$id."','".$country."','".$status."','".$payout."','".$conversions."') "
                . "ON CONFLICT (affise_mode,offer_id,country)"
                . "DO UPDATE "
                . "SET status ='".$status."' , payout='".$payout."' , conversions='".$conversions."'";
        
           $query = \Fuel\Core\DB::query($q);
  
            $res=$query->execute();

            return $res;
      } 
      
      public static function add_new($data) { 
          $q1='';
          $q =  "INSERT INTO trafficback  (affise_mode,offer_id,country, status, payout,conversions,os,device,update_time) "
                   . "VALUES";
  
           foreach ($data as $d){
   
           $q1.="('".$d[0]."','".$d[1]."','".$d[2]."','".$d[3]."','".$d[4]."','".$d[5]."','".$d[6]."','".$d[7]."','".$d[8]."'),";
                
           }
          
           $que=$q.substr($q1,0, strlen($q1)-1)." ON CONFLICT (affise_mode,offer_id,country) DO UPDATE SET status=EXCLUDED.status, payout = EXCLUDED.payout, conversions = EXCLUDED.conversions, update_time = EXCLUDED.update_time"; 
      
           $query = \Fuel\Core\DB::query($que);
 
            $res=$query->execute();

            return $res;
      } 
      
      public static function updateConversions($mode, $country,$id, $conversions,$updateTime){
          $que = "UPDATE trafficback ".  
         "SET  conversions = '".$conversions."' , update_time='".$updateTime."'
          WHERE affise_mode = '".$mode."' and offer_id = '".$id."' and country = '".$country."' and exists (select from trafficback WHERE affise_mode = '".$mode."' and offer_id = '".$id."' and country = '".$country."')"; 
 //echo $que.PHP_EOL; 
       $query = \Fuel\Core\DB::query($que);
           $res=$query->execute();

            return $res;

      }
      public static function changestatus($mode) { 
        
           $query = \Fuel\Core\DB::query("UPDATE trafficback "
                   . "SET status = 'disabled' where affise_mode = '".$mode."'"  );
         
            $res=$query->execute();

            return $res;
      } 
      public static function delete($mode, $updateTime) { 
          $upd = date("m/d/Y H:00",strtotime("-2 hour"));

           $query = \Fuel\Core\DB::query("DELETE FROM trafficback "
                   . " where affise_mode = '".$mode."' and update_time < '".$upd."'"  );
         
            $res=$query->execute();

            return $res;
      } 
}
