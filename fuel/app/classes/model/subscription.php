<?php
namespace Model;

class Subscription extends \Model
{
    
     public static function fetchAll($mode) { 
         switch ($mode){
             case 'app':
                 $table= 'subscription';
                 break;
             case 'dmi':
                 $table= 'subscription_dmi';
                 break; 
            case 'mob':
                 $table= 'subscription_dmgmob';
                 break;             
         }
        
         // Code to fetch employee from database 
           $query = \Fuel\Core\DB::query("SELECT * FROM ".$table."  order by title");
         
            $res=$query->execute();

            return $res->as_array();
      }
       public static function addPartner($email,$partner,$contact,$platform, $mode) { 
         switch ($mode){
             case 'app':
                 $table= 'subscription';
                 break;
             case 'dmi':
                 $table= 'subscription_dmi';
                 break; 
            case 'mob':
                 $table= 'subscription_dmgmob';
                 break;             
         }
        
           $q =   "INSERT INTO  ".$table." (email,title,contactname, subscribed, platform_id) VALUES('".$email."','".$partner."','".$contact."','1','".$platform."') "
                . "ON CONFLICT (email)"
                . "DO UPDATE "
                . "SET contactname ='".$contact."' , title='".$partner."' ,platform_id='".$platform."'";
        
           $query = \Fuel\Core\DB::query($q);
         
            $res=$query->execute();

            return $res;
      } 
      
      public static function updatePartner($email,$partner,$contact,$platform,$subscribed,$id, $mode) { 
         switch ($mode){
             case 'app':
                 $table= 'subscription';
                 break;
             case 'dmi':
                 $table= 'subscription_dmi';
                 break; 
            case 'mob':
                 $table= 'subscription_dmgmob';
                 break;             
         }

           $q =   " UPDATE ".$table
                . " SET contactname ='".$contact."' , title='".$partner."' ,platform_id='".$platform."' ,email='".$email."' ,subscribed='".$subscribed."'"
                . " WHERE id='".$id."'"   ;
            $query = \Fuel\Core\DB::query($q);
         try{
            $res=$query->execute();
             return 200;
         }
         catch(\Fuel\Core\FuelException $e) {
             return 500;

        }
       
           
      } 
      
       public static function getPartnerByPartnerrId($partnerId,$mode){
          switch ($mode){
             case 'app':
                 $table= 'subscription';
                 break;
             case 'dmi':
                 $table= 'subscription_dmi';
                 break; 
            case 'mob':
                 $table= 'subscription_dmgmob';
                 break;             
         }
          $q="SELECT * from  ".$table."  WHERE id in (".$partnerId.")" ;

          $query = \Fuel\Core\DB::query($q);
         
            $res=$query->execute();

            return $res->as_array();
      }
      
       public static function getPartnerByPlatformId($partnerId,$mode){
         switch ($mode){
             case 'app':
                 $table= 'subscription';
                 break;
             case 'dmi':
                 $table= 'subscription_dmi';
                 break; 
            case 'mob':
                 $table= 'subscription_dmgmob';
                 break;             
         }
          $q="SELECT * from  ".$table."  WHERE platform_id in (".$partnerId.")" ;

          $query = \Fuel\Core\DB::query($q);
         
            $res=$query->execute();

            return $res->as_array();
      }
   }


