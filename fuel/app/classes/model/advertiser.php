<?php
namespace Model;
class Advertiser extends \Model
{
     public static function fetchAll($mode) { 
        switch ($mode){
             case 'app':
                 $table= 'advertiser';
                 break;
             case 'dmi':
                 $table= 'advertiser_dmi';
                 break; 
            case 'mob':
                 $table= 'advertiser_dmgmob';
                 break;             
         }           
         // Code to fetch employee from database 
           $query = \Fuel\Core\DB::query("SELECT * FROM ".$table." where status = 'active' order by id desc"  );
         
            $res=$query->execute();

            return $res->as_array();
      } 
       public static function fetchAllActiveIntegrations($mode) { 
        switch ($mode){
             case 'app':
                 $table= 'advertiser';
                 break;
             case 'dmi':
                 $table= 'advertiser_dmi';
                 break; 
            case 'mob':
                 $table= 'advertiser_dmgmob';
                 break;             
         }           
         // Code to fetch employee from database 
           $query = \Fuel\Core\DB::query("SELECT filename FROM ".$table." where status = 'active'  and integration_status = '1' order by id desc"  );
         
            $res=$query->execute();

            return $res->as_array();
      } 
   }


