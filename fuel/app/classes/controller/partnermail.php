<?php


use Model\Partner;
use Model\Offer;
use Model\Advertiser;
use Model\Subscription;
use Fuel\Core\Package;
use Fuel\Core\Session as Session;
use Platforms\Affise; 
require  APPPATH.'shared'.DS.'shared.php';

class Controller_Partnermail extends Controller {
 
   private $_arrOffers = array();
   private function _createOffersArray($offers){

       $this->_arrOffers = array();

       foreach($offers as $offer){

           //echo($offer['caps']); 
           if (isset($offer['logo']) && ($offer['logo'] != '') && (strstr($offer['logo'],".png") != FALSE || strstr($offer['logo'],".jpeg") != FALSE)){
                        $logo =  "http://offers.dmgmob.affise.com{$offer['logo']}";
                   }
                   else {
                         $logo = $this->_defaultIcon;
                   }
           $tmpArr = array(
               "id" => $offer['platform_entity_id'],
               "offer_id" => $offer['offer_id'],
               "title" => $offer['title'],
               "platform" => $this->_getPlatform(json_decode($offer['payments'])),
               "payout" => $this->_getPayout(json_decode($offer['payments'])),
               "countries" => count(json_decode($offer['countries'])) > 4 ? "Worldwide" : json_decode($offer['countries']),
                "logo" => $logo,
               "dailylimit" => $this->_getDailyLimit(json_decode($offer['caps'])),
               "flow" => $this->_getFlow(json_decode($offer['categories'])),
           );
           
                  
                   
            $this->_arrOffers[] = $tmpArr;
        }
    }
   
    public function action_email(){
        $action = array();
        $mode = 'app';
        Package::load('email');
       
 
       $arr=(json_decode(file_get_contents('php://input')));
       if(!isset($arr) || empty($arr)) return json_encode(array("error"=> "PostData was not set"));
       if(isset($arr->system))        $mode =$arr->system;
        foreach ($arr->action as $key=>$value){
            $action = ($key);
            $data = array(
            "limit" => 500,
            "int_id" => $value->offers
          );
    
       $a = new \Platforms\Affise($mode);
       $offers = $a->_getAffiseOffersByList($data); 
                        
       $partnerIds= "'". implode("','", $value->partner)."'";
      
       if(!isset($offers) || count($offers->offers) == 0) {
             return json_encode(array("error"=>"offers were not found"));
       }
      $partners = Subscription::getPartnerByPlatformId($partnerIds,$mode);
     if(!isset($partners) || empty($partners)) {
             return json_encode(array("error"=>"partners were not found"));
   
      }

              foreach($partners as $address){
          
                    $this->_arrOffers =  _createAppOffersArray($offers->offers, $address);     

                    $email = \Email\Email::forge(array(
                                'driver'   =>'smtp',
                                'host'     => 'ssl://in.mailjet.com',
                                'port'     => 465,
                                'username' => 'a970e49837023e5cfa67092c8d7e87f0',
                                'password' => '210eaf20a289b19e31efa0a616d232dd',
                                'timeout'  => 50,
                                'starttls' => false,));

                    $email->to($address['email']);
                    $email->bcc('taloz@dsnrmg.com');
                    $email->from('partner@dsnrmg.com');

                    
                     switch ($action){
                        case "payout":
                            $view =\View::forge('newsletter/email_payoutupdate');  
                            $view->title_message = "Payout Update.";
                            $email->subject("Payout Update ".date('d-m-y',strtotime("today")));
                            break;
                        case "cap":
                            $view =\View::forge('newsletter/email_payoutupdate'); 
                            $view->title_message = "Daily Limit Update.";
                            $email->subject("Daily Limit Update ".date('d-m-y',strtotime("today")));
                            break;
                        default:
                            $view =\View::forge('newsletter/email_payoutupdate');
                            $view->title_message = "New Offers Activated";
                            $email->subject("New Offers Activated ".date('d-m-y',strtotime("today")));
                                                 }
                    $view->parameters = $this->_arrOffers;
                    $view->title = $address['title'];
                   // return $view;
                      $email->html_body($view);
                      $ret =$email->send();
//                      
         }
         return json_encode(array("success"=>"mail sent"));
      
    }
   
}
}