<?php


//use Model\Partner;
//use Model\Offer;
//use Model\Advertiser;
//use Model\Subscription;
//use Fuel\Core\Package;
//use Fuel\Core\Session as Session;
use Platforms\Affise; 
//require  APPPATH.'shared'.DS.'shared.php';

class Controller_Integration extends Controller {
 
   private $_arrOffers = array();
   
   
    public function action_get(){
 
 
        $advertiserId=$_GET['adv_id'];
        
        switch($advertiserId){
            ////////////////////////////////////////////////////
            //                Adxperience                    //
            ///////////////////////////////////////////////////
            case '59f832ee668f0fb6118b4600': //Adxperience
                 require  APPPATH.'tasks'.DS.'partners'.DS.'adxperience.php';
                 $tmp = new \Fuel\Tasks\Adxperience; 
                 // // echo $tmp->_url.PHP_EOL;
                 $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');
                 $curlResult = $curl->execute();
                 $offersResult=json_decode($curlResult->response()->body); 
                 //print_r($offersResult);
                if (isset($offersResult->response) && isset($offersResult->response->data) && isset($offersResult->response->data->offers) ){

                    foreach ($offersResult->response->data->offers as $_offer){
                    if ($_offer->status != 'running') continue;
                            $offer = array();
                            $offer['externalId'] = $tmp->getOfferId($_offer);
                            $offer['title'] = $tmp->getOfferName($_offer);
                            $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                            $offer['os'] = $tmp->getOfferOs($_offer);
                            $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                            $offer['capType'] = $tmp->getOfferCapType($_offer);
                            $offer['cap'] = $tmp->getOfferCap($_offer); 
                            $offer['payout'] = $tmp->getOfferPayout($_offer);
                            $this->_arrOffers[$offer['externalId']] = $offer;
                    } 
                   // print_r($this->_arrOffers);
                    return json_encode($this->_arrOffers,true);
                }
                else return json_encode(array("error"=>"offers were not found"));
            break; 
            
            
            ////////////////////////////////////////////////////
            //                Apetab                    //
            ///////////////////////////////////////////////////
            
            case '5cebd3ee668f0f4a008b4a45': //Apetab
                 require  APPPATH.'tasks'.DS.'partners'.DS.'apetab.php';
                 $tmp = new \Fuel\Tasks\Apetab(); 
                 $_options = array(
                 CURLOPT_COOKIE => 1,
                 CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
                 CURLOPT_RETURNTRANSFER => 1,
                 );       

                  $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');
                  $curl->set_method('get')  ;
                  $curl->set_options($_options);  
                  $curl->set_header('Content-Type', 'text/html');
                  $curlResult = $curl->execute();
                  $offersResult=json_decode($curlResult->response()->body); 

                    if (isset($offersResult->campaigns) && (count((array)$offersResult->campaigns) > 0) ){

                        foreach ($offersResult->campaigns as $_offer){
                                        $offer = array();
                                        $offer['externalId'] = $tmp->getOfferId($_offer);
                                        $offer['title'] = $tmp->getOfferName($_offer);
                                        $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                                        $offer['os'] = $tmp->getOfferOs($_offer);
                                        $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                                        $offer['capType'] = $tmp->getOfferCapType($_offer);
                                        $offer['cap'] = $tmp->getOfferCap($_offer); 
                                        $offer['payout'] = $tmp->getOfferPayout($_offer);
                                        $this->_arrOffers[$offer['externalId']] = $offer;
                                }
                               // print_r($this->_arrOffers);
                                return json_encode($this->_arrOffers,true);
                    }  
                    
                    else return json_encode(array("error"=>"offers were not found"));
            break;
           ////////////////////////////////////////////////////
            //               Appalgo                   //
            ///////////////////////////////////////////////////
            case '5cfe1500668f0fba008b48e0': //Appalgo !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
                 require  APPPATH.'tasks'.DS.'partners'.DS.'appalgo.php';
                 $tmp = new \Fuel\Tasks\Appalgo(); 
                $curl = \Fuel\Core\Request::forge($tmp->_url."session",'curl');
                         $curl->set_method('post');
                         $curl->set_header('Content-Type', 'application/x-www-form-urlencoded');
                         $data = "platform_name=appalgo&user_email=taloz@dsnrmg.com&user_password=Tal@123";
                         $curl->set_params(array('form-data' => $data));

                         $curlResult =$curl->execute();
  
   
                        if(null == $curlResult) return json_encode(array("error"=>"offers were not found"));
                         $curlRes=json_decode($curlResult->response()->body);         
                         $sessionId = $curlRes->session_id;
                         $curl = \Fuel\Core\Request::forge($tmp->_url."offerdetails",'curl');
                         $curl->set_method('get')  ;
                         $curl->set_header('X-Session-Id', $sessionId);
                         $curlResult = $curl->execute();
                         if(null == $curlResult) return json_encode(array("error"=>"offers were not found"));
                         $offersResult=json_decode($curlResult->response()->body); 


                     if (isset($offersResult) && (count((array)$offersResult) > 0) ){

                                foreach ($offersResult as $_offer){
                                        $offer = array();
                                        $offer['externalId'] = $tmp->getOfferId($_offer);
                                        $offer['title'] = $tmp->getOfferName($_offer);
                                        $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                                        $offer['os'] = $tmp->getOfferOs($_offer);
                                        $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                                        $offer['capType'] = $tmp->getOfferCapType($_offer);
                                        $offer['cap'] = $tmp->getOfferCap($_offer); 
                                        $offer['payout'] = $tmp->getOfferPayout($_offer);
                                        $this->_arrOffers[$offer['externalId']] = $offer;
                                }
                               // print_r($this->_arrOffers);
                                return json_encode($this->_arrOffers,true);
                    }  
                    
                    else return json_encode(array("error"=>"offers were not found"));
            break; 
            
            ////////////////////////////////////////////////////
            //                CURATEMOBILE                    //
            ///////////////////////////////////////////////////
            case '5b2917b8668f0f9fb58b50ba': //CurateMobile
                 require  APPPATH.'tasks'.DS.'partners'.DS.'curatemobile.php';
                 $tmp = new \Fuel\Tasks\CurateMobile(); 
                 $_headers = array(
                "Authorization: Token token=b930e870391c2bd9c7d0707ce46a9242",
                "Content-Type: application/json"    
                 ); 
                            $loop = true;
                            $page = 1;
                          while ($loop){
                                $this->_url = "{$tmp->_url}&page={$page}";


                             $curl = \Fuel\Core\Request::forge($this->_url,'curl');
                             $curl->set_header("Authorization" , "Token token=b930e870391c2bd9c7d0707ce46a9242") ;  
                             $curl->set_header("Content-Type" , "application/json") ; 
                             //$curl->set_options($_options);
                             $curl->set_method("get");
                             $curlResult = $curl->execute();
                             $offersResult=json_decode($curlResult->response()->body); 

                            if (isset($offersResult->pagination->next_link)){
                                    $page++;
                                }
                                else{
                                    $loop = false;
                                }   
                            if (!isset($offersResult->data->offers) || (count((array)$offersResult->data->offers) == 0 ) ) break;
                                    if (isset($offersResult->data->offers) && (count((array)$offersResult->data->offers) > 0 ) ){
                                                      $arrChanged = array();
                                        foreach ($offersResult->data->offers as $_offer){

                                                    $offer = array();
                                                    $offer['externalId'] = $tmp->getOfferId($_offer);
                                                    $offer['title'] = $tmp->getOfferName($_offer);
                                                    $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                                                    $offer['os'] = $tmp->getOfferOs($_offer);
                                                    $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                                                    $offer['capType'] = $tmp->getOfferCapType($_offer);
                                                    $offer['cap'] = $tmp->getOfferCap($_offer); 
                                                    $offer['payout'] = $tmp->getOfferPayout($_offer);
                                                    $this->_arrOffers[$offer['externalId']] = $offer;

                                         }
                                    }
                          }               

                         if (count($this->_arrOffers) > 0) return json_encode($this->_arrOffers,true);
                         else return json_encode(array("error"=>"offers were not found"));
            break; 
            ////////////////////////////////////////////////////
            //               Hitapps                   //
            ///////////////////////////////////////////////////
            
            case '5cf52bab668f0f49008b46ea': //Hitapps
                 require  APPPATH.'tasks'.DS.'partners'.DS.'hitapps.php';
                 $tmp = new \Fuel\Tasks\Hitapps(); 
                     $loop = true;
                     $page = 1;
                  while ($loop){
                     $tmp->_url = "{$tmp->_url}&page={$page}";
                     $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');

                     $curl->set_method("get");
                     $curlResult = $curl->execute();
                     $offersResult=json_decode($curlResult->response()->body); 

                    if (isset($offersResult->pagination->next_link)){
                            $page++;
                        }
                        else{
                            $loop = false;
                        }   

                    if (!isset($offersResult->offers) || (count((array)$offersResult->offers) == 0 ) ) break;
                    if (isset($offersResult->offers) && (count((array)$offersResult->offers) > 0 ) ){

                        foreach ($offersResult->offers as $_offer){

                                $offer = array();
                                $payment=$tmp->getOfferPayments($_offer);
                                
                                    
                                $offer['externalId'] = $tmp->getOfferId($_offer);
                                $offer['title'] = $tmp->getOfferName($_offer);
                                $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                                //$offer['os'] = $tmp->getOfferOs($_offer);
                                if(isset($payment[0]->os) && count($payment[0]->os) > 0) {
                                  $offer['os'] = implode(',', $payment[0]->os) ;
                                }
                                $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                                $offer['capType'] = $tmp->getOfferCapType($_offer);
                                $offer['cap'] = $tmp->getOfferCap($_offer); 
                                $offer['payout'] = round($payment[0]->revenue / 0.7,2);
                                $this->_arrOffers[$offer['externalId']] = $offer;

                        }

                    }
                  }
                   if (count($this->_arrOffers) > 0) return json_encode($this->_arrOffers,true);
                         else return json_encode(array("error"=>"offers were not found"));
            break;            
            
            ////////////////////////////////////////////////////
            //                KONEO                          //
            ///////////////////////////////////////////////////
            
            case '5c6c0672668f0f69008b4c13': //Koneo
  
                 require  APPPATH.'tasks'.DS.'partners'.DS.'koneo.php';
                  $tmp = new \Fuel\Tasks\Koneo(); 
                  $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');
                  $curlResult = $curl->execute();
                  $offersResult=json_decode($curlResult->response()->body); 
                if (isset($offersResult) && isset($offersResult) && isset($offersResult) ){
                     $arrChanged = array();
                    foreach ($offersResult as $_offer){
                                                $offer = array();
                                                $offer['externalId'] = $tmp->getOfferId($_offer);
                                                $offer['title'] = $tmp->getOfferName($_offer);
                                                $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                                                $offer['os'] = $tmp->getOfferOs($_offer);
                                                $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                                                $offer['capType'] = $tmp->getOfferCapType($_offer);
                                                $offer['cap'] = $tmp->getOfferCap($_offer); 
                                                $offer['payout'] = $tmp->getOfferPayout($_offer);
                                                $this->_arrOffers[$offer['externalId']] = $offer;
                                        }
                                       // print_r($this->_arrOffers);
                                        return json_encode($this->_arrOffers,true);
                            }  

                            else return json_encode(array("error"=>"offers were not found"));
            break;
            
            ////////////////////////////////////////////////////
            //                Mobusi                         //
            ///////////////////////////////////////////////////
            
            case '59c8e6e8668f0f97298b45ac': //Mobusi
                 require  APPPATH.'tasks'.DS.'partners'.DS.'mobusi.php';
                  $tmp = new \Fuel\Tasks\Mobusi(); 
                  $_options = array(
                        CURLOPT_COOKIE => 1,
                        CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
                        CURLOPT_RETURNTRANSFER => 1,
                        CURLOPT_CUSTOMREQUEST => "post",
                        CURLOPT_URL => $tmp->_url,
                        CURLOPT_POSTFIELDS => json_encode($tmp->_body),
                        CURLOPT_VERBOSE => false);
        
                 $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');

                 $curl->set_options($_options);
                 $curlResult = $curl->execute();
                 
                 $offersResult=json_decode($curlResult->response()->body); 
      
        if (isset($offersResult->answer) && (count((array)$offersResult->answer) > 0) ){
           
            foreach ($offersResult->answer as $_offer){
                    $offer = array();
                    $offer['externalId'] = $tmp->getOfferId($_offer);
                    $offer['title'] = $tmp->getOfferName($_offer);
                    $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                    $offer['os'] = $tmp->getOfferOs($_offer);
                    $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                    $offer['capType'] = $tmp->getOfferCapType($_offer);
                    $offer['cap'] = $tmp->getOfferCap($_offer); 
                    $offer['payout'] = $tmp->getOfferPayout($_offer);
                    $this->_arrOffers[$offer['externalId']] = $offer;
            }
                                       // print_r($this->_arrOffers);
                    return json_encode($this->_arrOffers,true);
                            }  

                    else return json_encode(array("error"=>"offers were not found"));
            break;
            
             case '5cebd3ee668f0f4a008b4a45': //Koneo
                 require  APPPATH.'tasks'.DS.'partners'.DS.'koneo.php';
                  $tmp = new \Fuel\Tasks\Koneo(); 
                  $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');
                  $curlResult = $curl->execute();
                  $offersResult=json_decode($curlResult->response()->body); 
                if (isset($offersResult) && isset($offersResult) && isset($offersResult) ){
                     $arrChanged = array();
                    foreach ($offersResult as $_offer){
                                                $offer = array();
                                                $offer['externalId'] = $tmp->getOfferId($_offer);
                                                $offer['title'] = $tmp->getOfferName($_offer);
                                                $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                                                $offer['os'] = $tmp->getOfferOs($_offer);
                                                $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                                                $offer['capType'] = $tmp->getOfferCapType($_offer);
                                                $offer['cap'] = $tmp->getOfferCap($_offer); 
                                                $offer['payout'] = $tmp->getOfferPayout($_offer);
                                                $this->_arrOffers[$offer['externalId']] = $offer;
                                        }
                                       // print_r($this->_arrOffers);
                                        return json_encode($this->_arrOffers,true);
                            }  

                            else return json_encode(array("error"=>"offers were not found"));
            break;
            
            ////////////////////////////////////////////////////
            //               Nasmedia                        //
            ///////////////////////////////////////////////////
            
            case '59bfac33668f0f894a8b4567': //Nasmedia
                 require  APPPATH.'tasks'.DS.'partners'.DS.'nasmedia.php';
                  $tmp = new \Fuel\Tasks\Nasmedia(); 
                  $_options = array(
                    CURLOPT_COOKIE => 1,
                    CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
                    CURLOPT_RETURNTRANSFER => 1,
                    );       

                  $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');
                  $curl->set_method('get')  ;
                  $curl->set_options($_options);  
                  $curl->set_header('Content-Type', 'text/html');
                  $curlResult = $curl->execute();

                 $offersResult=json_decode($curlResult->response()->body); 
        //        print_r($offersResult); exit(0);
                if (isset($offersResult->body->lists) && (count((array)$offersResult->body->lists) > 0) ){
                     $arrChanged = array();
                    foreach ($offersResult->body->lists as $_offer){
 
                    $offer = array();
                    $offer['externalId'] = $tmp->getOfferId($_offer);
                    $offer['title'] = $tmp->getOfferName($_offer);
                    $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                    $offer['os'] = $tmp->getOfferOs($_offer);
                    $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                    $offer['capType'] = $tmp->getOfferCapType($_offer);
                    $offer['cap'] = $tmp->getOfferCap($_offer); 
                    $offer['payout'] = $tmp->getOfferPayout($_offer);
                    $this->_arrOffers[$offer['externalId']] = $offer;
            }
                     return json_encode($this->_arrOffers,true);
                            }  

                    else return json_encode(array("error"=>"offers were not found"));
            break;
            
            ////////////////////////////////////////////////////
            //               Resultsmedia                     //
            ///////////////////////////////////////////////////
            
            case '5c6956ee668f0f11988b498c': //Resultsmedia
                 require  APPPATH.'tasks'.DS.'partners'.DS.'resultsmedia.php';
                  $tmp = new \Fuel\Tasks\Resultsmedia(); 
                          $_options = array(
            
                    CURLOPT_RETURNTRANSFER => 1,
                    CURLOPT_CUSTOMREQUEST => "get",
                    CURLOPT_URL => $tmp->_url,
                    );

                 $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');

                 $curl->set_options($_options);
                 $curlResult = $curl->execute();
                 $offersResult=json_decode($curlResult->response()->body); 
              // print_r($offersResult); exit(0);

                if (isset($offersResult) && (count((array)$offersResult->products) > 0 && isset($offersResult->products)) ){
                
                foreach ($offersResult->products as $_bundle){
 
                   $_offer = $_bundle->attributes ;
                    $offer = array();
                    $offer['externalId'] = $tmp->getOfferId($_offer);
                    $offer['title'] = $tmp->getOfferName($_offer);
                    $offer['geos'] = $tmp->getOfferGeos($_bundle->targeting);                    
                    $offer['os'] = $tmp->getOfferOs($_offer);
                    $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                    $offer['capType'] = $tmp->getOfferCapType($_bundle->capping);
                    $offer['cap'] = $tmp->getOfferCap($_bundle->capping); 
                    $offer['payout'] = $tmp->getOfferPayout($_offer);
                    $this->_arrOffers[$offer['externalId']] = $offer;
                }
                     return json_encode($this->_arrOffers,true);
                            }  

                    else return json_encode(array("error"=>"offers were not found"));
            break;
            
              ////////////////////////////////////////////////////
             //               Samurai                          //
            ////////////////////////////////////////////////////
            
            case '5aeac9da54528925493a3f76': //Samurai
                 require  APPPATH.'tasks'.DS.'partners'.DS.'samurai.php';
                  $tmp = new \Fuel\Tasks\Samurai(); 
                         $curl = \Fuel\Core\Request::forge($tmp->_url,'curl');
                 $curl->set_method('get')  ;
                 $curlResult = $curl->execute();
 
                if($curlResult->response()->body == "") {
                    exit(0);
                }
                 $offersResult=json_decode($curlResult->response()->body); 
                //  print_r($offersResult); exit(0);
                if (isset($offersResult) && (count((array)$offersResult) > 0) ){

                    foreach ($offersResult as $_offer){

                    $offer = array();
                    $offer['externalId'] = $tmp->getOfferId($_offer);
                    $offer['title'] = $tmp->getOfferName($_offer);
                    $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                    $offer['os'] = $tmp->getOfferOs($_offer);
                    $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                    $offer['capType'] = $tmp->getOfferCapType($_offer);
                    $offer['cap'] = $tmp->getOfferCap($_offer); 
                    $offer['payout'] = $tmp->getOfferPayout($_offer);
                    $this->_arrOffers[$offer['externalId']] = $offer;
                }
                     return json_encode($this->_arrOffers,true);
                            }  

                    else return json_encode(array("error"=>"offers were not found"));
            break;
            
            
              ////////////////////////////////////////////////////
             //               Youmi                            //
            ////////////////////////////////////////////////////
            
            case '5d230350668f0f9d008b484a': //Youmi
                 require  APPPATH.'tasks'.DS.'partners'.DS.'youmi.php';
                  $tmp = new \Fuel\Tasks\Youmi(); 
                  $loop = true;
                     $page = 1;
                     $limit=500;
                  while ($loop){

                    $page_url = "{$tmp->_url}&page={$page}&page_size={$limit}";
                   //echo $page_url.PHP_EOL;  

                 $curl = \Fuel\Core\Request::forge($page_url,'curl');

                 $curl->set_method("get");
                 $curlResult = $curl->execute();
                 $offersResult=json_decode($curlResult->response()->body); 
                 if (!isset($offersResult->offers) || (count((array)$offersResult->offers) == 0 ) ) break;
                if (isset($offersResult->total) && $offersResult->n < $limit){
                        $loop = false;
                    }
                    else{
                        $page++;
                    }   

                if (isset($offersResult->offers) && (count((array)$offersResult->offers) > 0 ) ){

                    foreach ($offersResult->offers as $_offer){
                    $offer = array();
                    $offer['externalId'] = $tmp->getOfferId($_offer);
                    $offer['title'] = $tmp->getOfferName($_offer);
                    $offer['geos'] = $tmp->getOfferGeos($_offer);                    
                    $offer['os'] = $tmp->getOfferOs($_offer);
                    $offer['bizModel'] = $tmp->getOfferBizModel($_offer); //cpa,cpi
                    $offer['capType'] = $tmp->getOfferCapType($_offer);
                    $offer['cap'] = $tmp->getOfferCap($_offer); 
                    $offer['payout'] = $tmp->getOfferPayout($_offer);
                    $this->_arrOffers[$offer['externalId']] = $offer;

                    }

                }
              }
              if (count($this->_arrOffers) > 0) return json_encode($this->_arrOffers,true);
              else return json_encode(array("error"=>"offers were not found"));
            break;            
        }
        

}
    public function action_createcaps(){
       $return =array();    
       $arr=(json_decode(file_get_contents('php://input'))); 
      // print_r($arr);
       $offerId=$arr->id;
       $mode = $arr->system;
       $arrAffiliates=$arr->aff;
 
               $aff = new \Platforms\Affise($mode);
               $offer = $aff->_getAffiseOffer($offerId);
 
               if(!isset($offer) ) return json_encode($return,true);
               if(!isset($offer->offer->caps) || count($offer->offer->caps) == 0) {

                   $cap=new stdClass();
                   $caps = array();
                   $cap->period = "day";
                   $cap->type = "conversions";
                   $cap->goal_type = "all";
                   $cap->affiliates = [];
                   $cap->affiliate_type = "all";
                   $cap->value = '50';
                   $cap->goals = [];
                  // print_r($cap);
                   $caps[] = $cap;
               }
               else $caps=$offer->offer->caps;
               foreach($caps as $cap){
                   if($cap->affiliate_type == "all"){
//                       print_r($cap);
                       $return[] = $cap;
                       foreach($arrAffiliates as $aff){
                         $return[] = array("period" => $cap->period, "type" => $cap->type,"value" => $cap->value, "goals" => $cap->goals, "goal_type" => $cap->goal_type, "affiliates" => array($aff),"affiliate_type" => "exact");
                       }
                   }
              
               }
               return json_encode($return,true); 
               
    }  
    
    
    
    public function action_getoffers(){
        $return =array();    
        $arr=(json_decode(file_get_contents('php://input')));
        $advertiser=$arr->advertiser;
        $arrExternals=$arr->id;
        $mode = $arr->system;
        $data = array(
                 "limit" => 1000,
                 "advertiser" => array($advertiser),
            
                );
     
               $aff = new \Platforms\Affise($mode);
               $offers = $aff->_getAffiseOffersByAdvertiser($data);
               if(!isset($offers) || count((array) $offers) == 0) return json_encode($return,true);
               foreach($offers->offers as $offer){
                   
                   if(in_array($offer->external_offer_id, $arrExternals)){
                     
                       $return[] = $offer;
                   }
               }
             return json_encode($return,true);

               
    }
    public function action_getaffiliates(){
         $affId=$_GET['aff_id'];
         $mode=$_GET['system'];
         $affise = new \Platforms\Affise($mode);
         $res= $affise->_getAffApiKey($affId);
         $_offers = array();
         $dbRow =array();
         if(isset($res) && $res->status == 1) $key =$res->partner->api_key;
         if($key){
              $limit = 200;
              $page = 1;
            
                $data = array(
                        "limit" => $limit,
                        "page" => $page,
                        "API-Key" => $key,
  

                    );
                   
  $_offers = $affise->_getAffiliateOffers($data);
                    
                 foreach($_offers->offers as $offer){
                     if(!is_object($offer)) continue;
                     $id = $offer->id;
                     $payout = $offer->payments[0]->revenue;
                     $os = implode(",",$offer->payments[0]->os);
                     $title =$offer->title;
                     $dbRow[$id] = array( $id, $title,  $payout, $os);
 
         }
                 return json_encode($dbRow, true);

         }
            
    }
}