<?php
/**
 * Fuel is a fast, lightweight, community driven PHP 5.4+ framework.
 *
 * @package    Fuel
 * @version    1.8.1
 * @author     Fuel Development Team
 * @license    MIT License
 * @copyright  2010 - 2018 Fuel Development Team
 * @link       http://fuelphp.com
 */

/**
 * The Welcome Controller.
 *
 * A basic controller example.  Has examples of how to set the
 * response body and status.
 *
 * @package  app
 * @extends  Controller
 */

use Model\Partner;
use Model\Offer;
use Model\Advertiser;
use Model\Subscription;
use Fuel\Core\Package;
use Fuel\Core\Session as Session;
use Platforms\Affise; 
require  APPPATH.'shared'.DS.'shared.php';

class Controller_Newsletter extends Controller
{
   //private $_mode;	
   private $_view ;
   private $_arrOffers = array();
   private $_defaultIcon = 'http://35.245.181.78:9000/images/logo.png';
   private $_mobIconPath ='http://offers.dmgmob.affise.com';
   private $_dmiIconPath ='http://offers.appsdmi.affise.com';
   
//   private function _getPlatform($payments) {
//
//        $dev = implode(",",$payments[0]->devices);
//        $os =implode(",",$payments[0]->os);
//        $return = (isset($dev) && isset($os) && !empty($dev) && !empty($os)) ? $dev. " ( ".$os. " ) " : '';
//        return $return;
//    }
    
//     private function _getPayout($payments){
//
//        return $payments[0]->revenue;
//    }
    
//    private function _getDailyLimit($caps){
////        var_dump($caps); 
//        if(isset($caps) && !empty($caps)){
//            return($caps[0]->value);
//            
//        }
//        else return null;
//    }
   
  
    
    private function _getFlow($categories){
      //  var_dump($categories); 
        $tmp =array();
        
        if(isset($categories) && !empty($categories)){
            foreach($categories as $category){
            //    var_dump($category->title);
                $tmp[] = $category->title;
            }
           return implode(',',$tmp);
            
        }
        else return null;
    }
    

    private function _createOffersArray($offers,$mode,$address){

       $this->_arrOffers = array();
       switch($mode){
           case "dmi":
               $logopath = $this->_dmiIconPath;
               break;
       
           case "mob":
               $logopath=$this->_mobIconPath;
               break;
       }
       

       foreach($offers as $offer){

           //echo($offer['caps']); 
           if (isset($offer->logo) && ($offer->logo != '') && (strstr($offer->logo,".png") != FALSE || strstr($offer->logo,".jpeg") != FALSE)){
                        $logo =  $logopath.$offer->logo;
           }
           else {
                  
                 $logo = _getLogo($offer->preview_url);
                //else $logo = $this->_defaultIcon;
            
            }
           $tmpArr = array(
               "id" => $offer->id,
               "offer_id" => $offer->offer_id,
               "title" => $offer->title,
               "platform" => _getPlatform($offer->payments),
               "payout" => _getAppPayout($offer,$address),
               "countries" => count($offer->countries) > 4 ? "Worldwide" : $offer->countries,
               "logo" => $logo,
               "dailylimit" => _getAppDailyLimit($offer,$address),
               "flow" => $this->_getFlow($offer->full_categories),
               "link" => "https://trc.dmgmob.com/click?pid={$address['platform_id']}&offer_id={$offer->id}",
               "kpi"  => strip_tags($offer->description_lang->en) ,   
           );
                   
            $this->_arrOffers[] = $tmpArr;
        }
    //    print_r($this->_arrOffers); die;
    }

    

      public function action_show() { 
         
          if(empty($_GET)) $mode ='mob';
          else             $mode=$_GET['mode'];
          Session::_init();
          Session::set('mode', $mode);
         //echo "This is the show method of employee controller"; 
    
         $partners = Subscription::fetchAll($mode);
         $offers = Offer::fetchAll($mode);
         $advertisers = Advertiser::fetchAll($mode);
         $selected =array();
         switch ($mode){
             case 'app':
                 $m = 'DMG Apps';
                 break;
             case 'dmi':
                 $m = 'DMI Mobile';
                 break; 
            case 'mob':
                 $m = 'DMG Mobile';
                 break;             
         }
         
         $Gview = View::forge('layout',array('mode' => $m,) );
        $Gview->m = $m;
//         print_r($offers);
   
         $this->_view = View::forge('newsletter/index');

        $this->_view->partners= $partners;
        $this->_view->offers= $offers;
        $this->_view->advertisers = $advertisers;
        $this->_view->selected = $selected;
         $Gview->content = $this->_view;
        
         
         return $Gview;
         
      } 
    public function action_search() { 
         $mode =Session::get('mode'); 
       //  echo $mode.PHP_EOL; 
         switch ($mode){
             case 'app':
                 $table= 'offer';
                 break;
             case 'dmi':
                 $table= 'offer_dmi';
                 break; 
            case 'mob':
                 $table= 'offer_dmgmob';
                 break;             
         }
        
         
        $query = "SELECT platform_entity_id,title FROM ".$table." WHERE status = 'active' ";  
          if($_POST['advertiser']  ) {
      
            $query = $query."AND advertiser_platform_id = '".$_POST['advertiser'] ."' ";
          }
           if($_POST['offername']  ) {
      
            $query = $query."AND to_tsvector(title) @@ to_tsquery('%".$_POST['offername']."%') ";
          }
          
          if($_POST['offerid'] ){
             $offerIds =$_POST['offerid'];
             $arrOffers=explode(",",$offerIds);
             $strOffers ="'". implode("','", $arrOffers)."'";
             $query = $query."AND platform_entity_id IN (".$strOffers .") ";
          }
         if($_POST['os'] || $_POST['country']){
              if ($_POST['os'] && !$_POST['country']){
 
                    $query = $query."AND to_tsvector(payments) @@ to_tsquery('%".$_POST['os']."%') ";
              }
              else if(!$_POST['os'] && $_POST['country']){
 
                   $query = $query."AND to_tsvector(countries) @@ to_tsquery('%".$_POST['country']."%') ";
              }
              else {

 

                  $query = $query."AND to_tsvector(payments) @@ to_tsquery('%".$_POST['os']."%') ";
                  $query = $query."AND to_tsvector(countries) @@ to_tsquery('%".$_POST['country']."%') ";
              }
               
          }  
//              echo $query;
               $offers = Offer::fetchByQueryString($query);
//               var_dump($offers);
               return json_encode($offers);

      }   
    
      public function action_addpartner(){
         $mode =Session::get('mode'); 
         $advertiser= $_POST['advertiser'];
         $mail= $_POST['mail'];
         $contact= $_POST['contact'];
         $platform = $_POST['platform']; 
         $ret =Subscription::addPartner($mail, $advertiser, $contact, $platform,$mode);
       // var_dump($ret);
         return json_encode($ret);
      }
      
       public function action_updatepartner(){
         //  var_dump($_POST);
         $mode =Session::get('mode'); 
         $advertiser= $_POST['advertiser'];
         $mail= $_POST['mail'];
         $contact= $_POST['contact'];
         $platform = $_POST['platform']; 
         $id=$_POST['id'];
         $subscribed = $_POST['subscribed'] == 'true' ? 1 : 0;
         $ret =Subscription::updatePartner($mail, $advertiser, $contact, $platform,$subscribed,$id,$mode);
   
       
         return json_encode($ret);
      }
      
       public function action_sendmail(){
           $ret = 0;
           Package::load('email');
           $mode =Session::get('mode');
          
         // if($mode == 'app') {
               $data = array(
                 "limit" => 500,
                 "int_id" => json_decode($_POST['offers']),
                );
     
               $aff = new \Platforms\Affise($mode);
               $offers = $aff->_getAffiseOffersByList($data); 
         // }
         // else{
         //      $offerIds= "'". implode("','", json_decode($_POST['offers']))."'";
         //      $offers = Offer::getOfferByOfferId($offerIds,$mode);
         // } 
           
           $partnerIds= "'". implode("','", json_decode($_POST['partners']))."'";
           $partners = Subscription::getPartnerByPartnerrId($partnerIds,$mode);

        foreach($partners as $address){
           if ($mode == 'app')
                  $this->_arrOffers = _createAppOffersArray($offers->offers, $address);     
           else        
                  $this->_createOffersArray($offers->offers,$mode,$address);
  
            $email = \Email\Email::forge(array(
                        'driver'   =>'smtp',
			'host'     => 'ssl://in.mailjet.com',
			'port'     => 465,
			'username' => 'a970e49837023e5cfa67092c8d7e87f0',
			'password' => '210eaf20a289b19e31efa0a616d232dd',
			'timeout'  => 50,
			'starttls' => false,
		));
            
            $email->to($address['email']); 
            $email->from('partner@dsnrmg.com');

            switch ($mode){
                case "app":
                   $email->subject("DMG New Campaign ".date('d-m-y',strtotime("today")));
                   $view =\View::forge('newsletter/email_topoffers'); 
                   break;
            
               case "mob":
                    $email->subject("DMG New Campaign ".date('d-m-y',strtotime("today")));
                    $view =\View::forge('newsletter/email_offers');
                    break;
               case "dmi":
                    $email->subject("AppsDmi New Campaign ".date('d-m-y',strtotime("today")));
                    $view =\View::forge('newsletter/email_offers');
                    break; 
           }
            $view->parameters = $this->_arrOffers;
            $view->title = $address['title'];
        //    return $view;
             $email->html_body($view);
             $ret =$email->send();
              return json_encode($ret);
             
        }
        
	
      }
    
    

   
	public function action_404()
	{
		return Response::forge(Presenter::forge('newsletter/404'), 404);
	}
        

}
