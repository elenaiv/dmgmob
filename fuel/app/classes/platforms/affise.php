<?php
namespace Platforms;


use \Fuel\Core\Request as Curl;
 
 
class Affise {
private $_baseUrl = "http://api.dsnrmg.affise.com/";
private $_mob_baseUrl = "https://api-dmgmob.affise.com/";
private $_dmi_baseUrl = "https://api-appsdmi.affise.com/";
private $_api_version = "3.0/";
private $_api_apiKey = "8b80e0b3395eb96c59d041e8c131f4493ae78761";//"6a66df0d64a6798651d03b39ad143fe724d225bf";
private $_api_mob_apiKey = "f1b33796e8db3440611acc39430bdaeda247191a";
private $_api_dmi_apiKey = "4cc671357dc906d60dcf823d28d1d2c8fd712c15";
private $_api_advertisersPath = "advertisers";
private $_api_affiliatesPath = "partner/";
private $_api_offerPath = "offer/";
private $_api_offersPath = "offers";
private $_api_reportsPath = "stats/";
private $_api_adminPath = "admin/";
private $_categoriesPath ="categories";
private $_url;
private $_get_options ;
private $_key;

public function __construct($mode='app'){

 switch ($mode){
     case 'app':
          $this->_url = $this->_baseUrl.$this->_api_version;
          $this->_key = $this->_api_apiKey;
         break;
     case 'dmi':
         $this->_url = $this->_dmi_baseUrl.$this->_api_version;
         $this->_key = $this->_api_dmi_apiKey;
         break;
     case 'mob':
         $this->_url = $this->_mob_baseUrl.$this->_api_version;
         $this->_key = $this->_api_mob_apiKey;
         break;
 }
           $this->_get_options = array(
            CURLOPT_COOKIE => 1,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_VERBOSE => false);
    }
public function _getCurrency()  {
     $url = $this->_url."admin/currency";
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curl->set_header('API-Key', $this->_key);
    $curl->set_options($this->_get_options);
    $curlResult =$curl->execute();
//     var_dump($curlResult);exit(0);
    $curlRes=json_decode($curlResult->response()->body);    

     return $curlRes;
}  
public function _getCategories($data){
 
    foreach ($data as $dataName => $dataValue){

           $_data[$dataName] = $dataValue;
        }
    $url = $this->_url.$this->_api_offerPath.$this->_categoriesPath."?" . http_build_query($_data);
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curl->set_header('API-Key', $this->_key);
    $curl->set_options($this->_get_options);
    $curlResult =$curl->execute();
//     var_dump($curlResult);exit(0);
    $curlRes=json_decode($curlResult->response()->body);    

     return $curlRes;
        
    } 
   public function _getConversions($data){
  
    foreach ($data as $dataName => $dataValue){

           $_data[$dataName] = $dataValue;
        }
    $url = $this->_url.$this->_api_reportsPath."conversions?" . http_build_query($_data);
   // echo $url.PHP_EOL; 
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curl->set_header('API-Key', $this->_key);
    $curl->set_options($this->_get_options);
    try{
      $curlResult =$curl->execute();
      $curlRes=json_decode($curlResult->response()->body);    
      return $curlRes;
    }
    catch(Fuel\Core\RequestStatusException $e) {
         \Fuel\Core\Log::write("Error",$e->getMessage());
         return 0;
    }
    
        
    }  
    public function _getAffApiKey($id){
    $url = $this->_url."admin/partner/{$id}";
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_header('API-Key', $this->_key);
    $curl->set_method('get');
    $curlResult =$curl->execute();
    
    $curlRes=json_decode($curlResult->response()->body);   

     return $curlRes;
     
    }
    
    public function _getAffiliateOffers($_data){
    $url = $this->_url.$this->_api_affiliatesPath.$this->_api_offersPath."?" . http_build_query($_data)."&sort[id]=desc";
   // echo $url.PHP_EOL;die;
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curlResult =$curl->execute();
    $curlRes=json_decode($curlResult->response()->body);   

     return $curlRes;
        
    }
  public function _getConversionsForAff($data){
      $url =  $this->_url.$this->_api_reportsPath."conversions?" . http_build_query($data);
//    echo $url."\n";
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curl->set_header('API-Key', $this->_key);
    try{
      $curlResult =$curl->execute();
      $curlRes=json_decode($curlResult->response()->body);    
      return $curlRes;
    }
    catch(Fuel\Core\RequestStatusException $e) {
         \Fuel\Core\Log::write("Error",$e->getMessage());
         return NULL;
    }
  }
    ///////////////////////////////
public  function _getAffiseOffer($offerId){
    
    $curl = \Fuel\Core\Request::forge($this->_url.$this->_api_offerPath.$offerId,'curl');
    
        $curl->set_method('get');
        $curl->set_header('API-Key', $this->_key);
        $curl->set_options($this->_get_options);
       
        $curlResult =$curl->execute();
     
        $curlRes=json_decode($curlResult->response()->body); 
        return $curlRes;
    
}
public function _getAffiseOffersByList($data){

       foreach ($data as $dataName => $dataValue){

           $_data[$dataName] = $dataValue;
        }
 
    $url = $this->_url.$this->_api_offersPath."?" . http_build_query($_data);
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curl->set_header('API-Key', $this->_key);
    $curl->set_options($this->_get_options);
    $curlResult =$curl->execute();
    if($curlResult == null) {
        echo "Offers by list Data is not set\n"; return null;
    }
    $curlRes=json_decode($curlResult->response()->body);    

     return $curlRes;
}
  
public function _getAffiseOffersByAdvertiser($data){

       foreach ($data as $dataName => $dataValue){

           $_data[$dataName] = $dataValue;
        }
 
    $url = $this->_url.$this->_api_offersPath."?" . http_build_query($_data);
    $curl = \Fuel\Core\Request::forge($url,'curl');
    $curl->set_method('get');
    $curl->set_header('API-Key', $this->_key);
    $curl->set_options($this->_get_options);
    $curlResult =$curl->execute();
     
    $curlRes=json_decode($curlResult->response()->body);    

     return $curlRes;
}

 public  function _updateOffer($offerId, $data){

        $url = $this->_url.$this->_api_adminPath.$this->_api_offerPath.$offerId;
        $curl = \Fuel\Core\Request::forge($url,'curl');
        $curl->set_method('post');
        $curl->set_header('API-Key', $this->_key);
        $curl->set_params(array('form-data' => $data));
        $curlResult =$curl->execute();
        if($curlResult->response() == null) return null;
        $curlRes=json_decode($curlResult->response()->body);    
        return $curlRes;
    }
    
  public  function _createOffer($data){
         
        $url = $this->_url.$this->_api_adminPath."offer";
     
        $curl = \Fuel\Core\Request::forge($url,'curl');
        $curl->set_method('post');
        $curl->set_header('API-Key', $this->_key);
        $curl->set_params(array('form-data' => $data));
//        echo $url.PHP_EOL;
//        echo $data.PHP_EOL;
     
 try {
        $curlResult =$curl->execute();
      if( $curlResult == null ) {
          echo "Failed to add Offer \n".$data.PHP_EOL;
          return null;
      }
        $curlRes=json_decode($curlResult->response()->body);    
        return $curlRes;
      }
    catch(Fuel\Core\RequestStatusException $e) {
         \Fuel\Core\Log::write("Error",$e->getMessage());
         return null;
    }
  }

}
