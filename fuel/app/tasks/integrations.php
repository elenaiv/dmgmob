<?php
namespace Fuel\Tasks;

use Model\Partner;
use Model\Offer;
use Model\Advertiser;
use Model\Subscription;
use Fuel\Core\Package;
use Fuel\Core\Session as Session;
use Platforms\Affise; 
use \Fuel\Core\Request as Curl;
use Model\Integrationofficer;
require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';


class Integrations
{
    public static function run(){


 $advertisers = Advertiser::fetchAllActiveIntegrations('app');
 foreach($advertisers as $adv){
     $name= \Fuel\Core\Str::strtolower($adv["filename"]);
     if(file_exists(APPPATH.'tasks'.DS.'partners'.DS.$name.".php")){
          require APPPATH.'tasks'.DS.'partners'.DS.$name.".php";
          echo 'Run '.$name.PHP_EOL;
          $class = "\\Fuel\\Tasks\\".$name;
          $tmp = new $class;
          $tmp->getOffers();
     }
 }
 $advertisers = Advertiser::fetchAllActiveIntegrations('mob');
 foreach($advertisers as $adv){
     $name= \Fuel\Core\Str::strtolower($adv["filename"]);
       if(file_exists(APPPATH.'tasks'.DS.'partners'.DS.'mob'.DS.$name.".php")){
          require APPPATH.'tasks'.DS.'partners'.DS.'mob'.DS.$name.".php";
          echo 'Run '.$name.PHP_EOL;
          $class = "\\Fuel\\Tasks\\".$name;
          $tmp = new $class;
          $tmp->getOffers();
       }
  
 } 
 $advertisers = Advertiser::fetchAllActiveIntegrations('dmi');
 foreach($advertisers as $adv){
     $name= \Fuel\Core\Str::strtolower($adv["filename"]);
     if(file_exists(APPPATH.'tasks'.DS.'partners'.DS.'dmi'.DS.$name.".php")){   
          require APPPATH.'tasks'.DS.'partners'.DS.'dmi'.DS.$name.".php";
          echo 'Run '.$name.PHP_EOL;
          $class = "\\Fuel\\Tasks\\".$name;
          $tmp = new $class;
          $tmp->getOffers();
     }
  
 } 
 
 
 
 
 
 $advertisers = Advertiser::fetchAllActiveIntegrations('app');
 chdir('//opt//affiseIntegrations');
 foreach($advertisers as $adv){
     $name= $adv["filename"];
     $filename='//opt//affiseIntegrations'.DS.$name.".php";
     if(file_exists($filename)){
         
        shell_exec('php '.$filename);
          echo 'Run '.$name.PHP_EOL;

     }
 }
 $advertisers = Advertiser::fetchAllActiveIntegrations('dmi');
 chdir('//opt//affiseIntegrations//appsDmi');
 foreach($advertisers as $adv){
     $name=$adv["filename"];
     $filename='//opt//affiseIntegrations//appsDmi'.DS.$name.".php";
     if(file_exists($filename)){
        shell_exec('php '.$filename);
          echo 'Run '.$name.PHP_EOL;

     }
 }
 $advertisers = Advertiser::fetchAllActiveIntegrations('mob');
 chdir('//opt//affiseIntegrations//appsMob');
 foreach($advertisers as $adv){
     $name=$adv["filename"];
     $filename='//opt//affiseIntegrations'.DS.$name.".php";
     if(file_exists($filename)){
        shell_exec('php '.$filename);
          echo 'Run '.$name.PHP_EOL;

     }
 }
//     $partner =  func_get_args();
//    echo $partner[0].PHP_EOL;   
//      switch ($partner[0]){
//      case "Mobusi":
//          require  APPPATH.'tasks'.DS.'partners'.DS.'mobusi.php';
//          $tmp = new Mobusi;
//              break;
//       case "Resultsmedia" :
//       require  APPPATH.'tasks'.DS.'partners'.DS.'resultsmedia.php';
//         $tmp = new Resultsmedia;   
//         break;
//        case "Samurai" :
//       require  APPPATH.'tasks'.DS.'partners'.DS.'samurai.php';
//         $tmp = new Samurai;   
//         break;
//       case "Nasmedia" :
//       require  APPPATH.'tasks'.DS.'partners'.DS.'nasmedia.php';
//         $tmp = new Nasmedia;   
//         break;
//      case "Adxperience" :
//       require  APPPATH.'tasks'.DS.'partners'.DS.'adxperience.php';
//         $tmp = new Adxperience;   
//         break;
//      case "Apetab":
//          require  APPPATH.'tasks'.DS.'partners'.DS.'apetab.php';
//          $tmp = new Apetab;
//              break;
//      case "CurateMobile":
//          require  APPPATH.'tasks'.DS.'partners'.DS.'curatemobile.php';
//          $tmp = new CurateMobile;
//              break;  
//      case "Koneo":
//          require  APPPATH.'tasks'.DS.'partners'.DS.'koneo.php';
//          $tmp = new Koneo;
//              break;
//     case "hitapps":
//          require  APPPATH.'tasks'.DS.'partners'.DS.'hitapps.php';
//          $tmp = new hitapps;
//              break; 
//      case "Appalgo":
//      require  APPPATH.'tasks'.DS.'partners'.DS.'appalgo.php';
//      $tmp = new Appalgo;
//          break; 
//
//     case "Youmi":
//      require  APPPATH.'tasks'.DS.'partners'.DS.'youmi.php';
//      $tmp = new Youmi;
//          break;      
//     case "Shisham":
//      require  APPPATH.'tasks'.DS.'partners'.DS.'shisham.php';
//      $tmp = new Shisham;
//          break;                   
//      }
//       $tmp->getOffers();
       
    }
    
       
    }
