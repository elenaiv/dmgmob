<?php

namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
use Fuel\Core\Package;

class PublisherFeed {
   private $_loginurl = "https://login.rtb2dmg.net/admin/auth?";
   private $_baseUrl="https://login.rtb2dmg.net/admin/api/FeedReports/feed?token=";
   private $_baseUrl1 ='https://login.rtb2dmg.net/admin/api/FeedReports/remotefeed?token=';
   private $_remoteFeedUrl ='https://login.rtb2dmg.net/admin/api/RemoteFeed/';
   private $_feedArr = array();
   private $_login = "rtb2dmg";
   private $_password = "5gU6wCpk3M4c";
   public function getFeeds(){
        $curl = \Fuel\Core\Request::forge($this->_loginurl,'curl');
        $curl->set_params(array('login' => $this->_login, 'password' => $this->_password));
        $curl->set_method("get");
        $curlResult = $curl->execute();
   
        $token=$curlResult->response()->body; 
        //////////////////////  Email /////////////////////////
       if (isset($token)){
           $start = 0;
           $finish = 500;
            $today= date('Y-m-d',strtotime("today"));
            $yesterday =  date('Y-m-d',strtotime("-12 hour"));
            do{
                  
                   $url = $this->_baseUrl.$token."&filters=date:".$today."_".$yesterday."&range=".$start."-".$finish;
                 //  echo $url.PHP_EOL;
                   $curl = \Fuel\Core\Request::forge($url,'curl');
                   $curlResult = $curl->execute();
                   $offersResult=json_decode($curlResult->response()->body); 
                   
                 // echo count(get_object_vars($offersResult->response->list)).PHP_EOL;
                  if(empty($offersResult->response->list) || count(get_object_vars($offersResult->response->list)) == 0 ) break;

                 foreach($offersResult->response->list as $feed){
                   //  echo round($feed->pub_gross,2)."  ".round($feed->pub_revenue,2).PHP_EOL ;
                    if(round($feed->pub_gross,2) > (round($feed->pub_revenue,2) + 80)) { 
                        $this->_feedArr[]= $feed->feed_id;
                    }
                 }
                 if( count(get_object_vars($offersResult->response->list)) < $finish ) break;
                 $start = $start + 500;
                 $finish = $finish + 500;
           } while(1);
            
            
            if(count($this->_feedArr)> 0){
                \Package::load('email');
                $email = \Email\Email::forge(array(
                        'driver'   =>'smtp',
			'host'     => 'ssl://in.mailjet.com',
			'port'     => 465,
			'username' => 'a970e49837023e5cfa67092c8d7e87f0',
			'password' => '210eaf20a289b19e31efa0a616d232dd',
			'timeout'  => 50,
			'starttls' => false,
		));
                $email->to('hagitmiz@dsnrmg.com'); 
                $email->bcc('elenaiv@dsnrmg.com');
                $email->from('partner@dsnrmg.com');
                $email->subject("Revenue exceptions for the Publisher feeds: ".implode(",", $this->_feedArr));
                $ret =$email->send();
            }
       }
    ///////////////////////////  Change underpay //////////////////////////////////////
 
 if (isset($token)){
           $start = 0;
           $finish = 500;
            $today= date('Y-m-d',strtotime("yesterday"));
            do{
                  
                   $url = $this->_baseUrl1.$token."&filters=date:".$today."_".$today."&range=".$start."-".$finish;
                   $curl = \Fuel\Core\Request::forge($url,'curl');
                   $curlResult = $curl->execute();
                   $offersResult=json_decode($curlResult->response()->body); 
                   
                   if(empty($offersResult->response->list) || count(get_object_vars($offersResult->response->list)) == 0 ) break;

                 foreach($offersResult->response->list as $feed){
  
                       $feedId = $feed->remotefeed_id;
                       $url = $this->_remoteFeedUrl.$feedId."?token=".$token;
                       $curl = \Fuel\Core\Request::forge($url,'curl');
                       $curl->set_options(array(CURLOPT_POSTFIELDS => "{'underpay' : 1 }", CURLOPT_RETURNTRANSFER => true,CURLOPT_CUSTOMREQUEST => "PUT"));
                       $curl->set_header('Content-Type', 'application/json');
                       $curlResult = $curl->execute();
                        
                 }
                 if( count(get_object_vars($offersResult->response->list)) < $finish ) break;
                 $start = $start + 500;
                 $finish = $finish + 500;
           } while(1);
           
            
            
            }
       
 
   }
}
