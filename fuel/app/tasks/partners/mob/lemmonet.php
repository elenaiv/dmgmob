<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class lemmonet  extends Shared {
    //put your code here
    private $_url = "https://lemmonetapi.azurewebsites.net/myoffers";
 
    private $_post = array(
     "clientid" => "DMGMC7255hgs",
     "apikey" => "DF004C26120641228340CE91A0043E09"
);
    public $_affiseadvertiserId = '5c17a0faf19ccb99ca8b458b';
     
    public $_macros = '&subpubid={pid}_{sub1}_{sub2}&clickid={clickid}&gaid={sub4}&idfa={sub4}'; 
   
    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('mob');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->OfferID;
    }
 
    public function getOfferName($offer){
       return $offer->OfferName;
    }
    
    public function getOfferDescription($offer){
        if(isset($offer->AppDescritpion) && $offer->AppDescritpion != null && strlen($offer->AppDescritpion) > 0)  return $offer->AppDescritpion;      
        return $offer->OfferName;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = "";
         if (count($offer->Countries) == 0 ) return "";
         return implode(",", $offer->Countries);
    }

    public function getOfferOs($offer){
        
         return '';
    
    }


   public function getOfferBizModel($offer){

            return strtoupper($offer->Type);

    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      return isset($offer->DailyCap) ? $offer->DailyCap : 0;
    }

    public function getOfferKpi($offer){
     $kpi = '';
        if (isset($offer->Restrictions) && count($offer->Restrictions) > 0){
            $o = implode(',',$offer->Restrictions);
             $kpi .= " Restrictions: {$o}\n";
        }
       
        if (isset($offer->Notes) && $offer->Notes != ""){
            $kpi .= " Flow: {$offer->Notes}\n";    
        }
        if (isset($offer->Targetings) && count($offer->Targetings) >0 ){
            $o = implode(',',$offer->Targetings);
            $kpi .= " Targetings: {$o}\n";    
        }
        return $kpi;
        
    }

    public function getOfferMinOsVersion($offer){

        return  0;
    }
    
     public function getOfferPreviewLink($offer){

        return $offer->AppPreview;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->Payout;
    }
    
    public function getOfferCurrency($offer){
      return "USD";
    }
    
    public function getOfferTrackingLink($offer){
       if (isset($offer->TrakingURL)) return substr($offer->TrakingURL,0,strpos($offer->TrakingURL,"&subpubid"));
        else return "";
    }


    public function getOfferCategories($offer){
//        $categories = array();
//        foreach ($offer->categories as $tag){
//            $neededObject = current(array_filter( $this->_categories, function ($e)  use ($tag) { return $e->title == $tag;    }));  
//            if(isset($neededObject) && is_object($neededObject)){
//               $categories[] = $neededObject->id; 
//            }
//            else $categories[] = "5a3b86ac375b1239000000b6";
//            
//        }
//          return implode(",", $categories);
        return "";
        
    }
    
     public function getOfferLogo($offer){

         return "";
    }
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";

    }

     public function getBlacklistedAffiliates($offer){
         return "";

    }

  
    public function getOffers(){
  
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          //$curl->set_method('get')  ;
          $curl->set_header('Content-Type', 'application/json');
          $curl->set_options(array(CURLOPT_POSTFIELDS => json_encode($this->_post), CURLOPT_RETURNTRANSFER => true,CURLOPT_CUSTOMREQUEST => "POST"));
          $curlResult = $curl->execute();
      
         $offersResult=json_decode($curlResult->response()->body); 
        // print_r($offersResult); 
   
        if (isset($offersResult->result) && (count($offersResult->result) > 0) ){
                $arrChanged = array();
            foreach ($offersResult->result as $_offer){
//                if(strstr($_offer->preview_url, 'http://') == FALSE 
//                        && strstr($_offer->preview_url,'https://') ==FALSE 
//                        && strstr($_offer->preview_url, 'itms-apps://') == FALSE 
//                        && strstr($_offer->preview_url,'market://') == FALSE 
//                        && strstr($_offer->preview_url, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = "active";
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
      //  print_r($this->_offers); 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);

        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateMobAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateMobAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateMobAffiseOfferData($arr);
                                   
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }

            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                 print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("mob",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
