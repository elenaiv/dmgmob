<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobusi
 *
 * @author lenaivch
 */
namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class resultsmedia  extends Shared {
    //put your code here
    public $_url = "http://s.resultsmedia.com/xml/cpa_feeds/feed.php?feed_id=604&hash=9cc8a48ae9c3d0f35b84f048118dc9f6&format=json";

    public $_affiseadvertiserId = '5c6956ee668f0f11988b498c';
    
    public $_macros = '&p1={clickid}&p2={pid}&p3={sub3}&p4={sub4}&p6={sub4}&$cad[app_name]={Sub5}';

     public function __construct(){
        $this->_affise = new \Platforms\Affise();
        $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

        return $offer->title;
    }
    public function getOfferDescription($offer){

        return $offer->description;
    }  
    public function getOfferGeos($offer){ 
         $geos = array();
       if (isset($offer->countries) && !empty($offer->countries)){
             foreach ($offer->countries as $country){
                array_push($geos, $country);
            }
        }
        
        return implode(",", $geos);
    }

    public function getOfferOs($offer){
       if ($offer->domain == "itunes.apple.com"){

            return "iOS";
        }

        return "Android";
    }



   public function getOfferBizModel($offer){
       
       $bizModel ="CPI"  ; 
       if(isset($offer->business_model)) $bizModel = $offer->business_model;
        return $bizModel;
    }

    public function getOfferCapType($offer){

                return "daily_cap";
 
        }

      
    

    public function getOfferCap($offer){
    
        if (isset($offer->cap_amount)){
            return $offer->cap_amount; 
        }
        return 50;

       }

    public function getOfferKpi($offer){
     return $offer->description;
    }

    public function getOfferMinOsVersion($offer, $domain){
       
        if (isset($offer->MinOs_version) && !empty($offer->MinOs_version)){

            foreach ($offer->MinOs_version as $_preference){
                    $minOs = array(
                        $_preference
                    );
            }
        }
        else{
 
            if($domain == "tunes.apple.com"){
   
                   $minOs = array( "4.4");
            } else {
                 $minOs = array("8");
            }
        }
        return implode(",", $minOs);

    }
    
     public function getOfferPreviewLink($offer){

        return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        
        return $payoutType;
    }

    public function getOfferPayout($offer){

         return $offer->rate;
    }
    
    public function getOfferCurrency($offer){

        if (!isset($offer->currency) || ($offer->currency === null || $offer->currency == "null")){

            return "USD";
        }

        return $offer->currency;
    }
    
    public function getOfferTrackingLink($offer){

        return $offer->tracking_url;
    }



    public function getOfferCategories($offer){

        $categories = array();
        foreach ($offer->categories as $tag){
            $neededObject = current(array_filter( $this->_categories, function ($e)  use ($tag) { return $e->title == $tag;    }));  
            if(isset($neededObject) && is_object($neededObject)){
               $categories[] = $neededObject->id; 
            }
            else $categories[] = "5a3b86ac375b1239000000b6";
            
        }
          return implode(",", $categories);
    }
    
     public function getOfferLogo($offer){

       if (isset($offer->thumbnail)){

            return $offer->thumbnail;
        }

        return "";
    }
    
     public function getOfferActionRequire($offer){ //KPMBRO

//        if (isset($offer->action_require)){
//            return $offer->action_require;
//        }

        return "Install&Open";
    }
    



     public function getBlacklistedSources($offer){
         $blacklisted_sources =array();
        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
            foreach($offer->black_list_sources as $value)   {
                $pos=stripos("$value","_");
                $sources= substr($value,$pos + 1);
                $blacklisted_sources[] = $sources;
           }

        }
       
        $tmp = implode(",", $blacklisted_sources);
        $tmpstr = str_replace("?", "", $tmp);
        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         $affiliates =array();
        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
            foreach($offer->black_list_sources as $value)   {
                $pos=stripos("$value","_");
                $aff = substr($value,0,$pos);
                $affiliates[] = $aff;
           }

        }
       
        return implode(",", $affiliates);
    }

    
    public function getOffers(){
        
        $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;             
       
        $_options = array(
            
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CUSTOMREQUEST => "get",
            CURLOPT_URL => $this->_url,
            );
        
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
 
	 $curl->set_options($_options);
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
      // print_r($offersResult); exit(0);

        if (isset($offersResult) && (count($offersResult->products) > 0 && isset($offersResult->products)) ){
        $arrChanged = array();
            foreach ($offersResult->products as $_bundle){

                 $_offer = $_bundle->attributes ; 
                 $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;                    
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_bundle->targeting);                   
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_bundle->capping);
                    $offer->cap = $this->getOfferCap($_bundle->capping);  
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_bundle->mobile_attributes, $_offer->domain); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = $_offer->status;
                    $offer->categories = $this->getOfferCategories($_bundle->targeting);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_bundle->targeting);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_bundle->targeting);
                    }
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
//        print_r($this->_offers); exit(0);
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
           $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
           $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "Live":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);            
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){ 
                            $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);                                        
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);

                               }
                        //edit
                         }
                         else {
                             echo "create new offer".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;
                default :
                     echo "offer inactive  - should be stopped ".$externalOfferId.PHP_EOL;
                  //search in active -if exists =>stop it
                  $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));  
        
                if (isset($neededObject) && is_object($neededObject)) {
                    $arr=json_decode(json_encode($neededObject), True);
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);
                                      
                }
                    break;
             
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);      
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}

