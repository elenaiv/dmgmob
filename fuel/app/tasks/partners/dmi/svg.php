<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class svg  extends Shared {
    //put your code here
    private $_url = 'http://api.svg.performancecentral.mobi/www/api/v3/API.php?requestParams={"ads":[{"adViewId":"3006","excludedCreatives":[],"size":"1000","startIndex":"0"}],"apiVersion":"12","deviceLanguage":"en","hashCode":"QEfsvwoVFQjpC0VCU8EAPExTHzjz9UtDTD/hQklKBuj0PUBDGQm5uwoV","isMobile":"true","currencyCode":"USD","requestSource":"PUBLIC","directImageUrl":1}'; 

    
    public $_affiseadvertiserId = '5d05e38fc91b187a008b456b';
     
    public $_macros = '&gaid={sub4}&IDFA={sub4}&subid1={clickid}&subid2={pid}_{sub1}&subid3={sub5}';
    

    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->campaignid;
    }
 
    public function getOfferName($offer){
       return $offer->campaignname;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->description;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = "";
       $g = array();
       if (null != $offer->geoInc && ( strpos($offer->geoInc, " ") == FALSE)) {
           $g = explode(',', $offer->geoInc);
           $i=0;
           foreach ($g as $tmp){
               if (strlen($tmp) > 2) unset($g[$i]);
               $i++;
           }
           $geos = implode(',', $g);   
          }
        return $geos;
    }

    public function getOfferOs($offer){
                $os = 'Android';
         if (null != $offer->os){
           if(strtolower($offer->os) == 'ios') $os='iOS';
         }
//         echo $os.PHP_EOL;
         return $os;
    
    }


   public function getOfferBizModel($offer){

            return "CPI";

    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      return (null != $offer->dailyGoal) ? $offer->dailyGoal : 0;
    }

    public function getOfferKpi($offer){
     $kpi = $offer->description;
       
        return $kpi;
        
    }

    public function getOfferMinOsVersion($offer){
          $osVer = "";
         if (null != $offer->minOsVersion) {
               
                 $osVer =$offer->minOsVersion;
            }
      
         return $osVer;

    }
    
     public function getOfferPreviewLink($offer){
//echo substr($offer->packagename,2).PHP_EOL;
        if(strtolower($offer->os) == "ios")
       {
           return "http://itunes.apple.com/app/id".substr($offer->packagename,2);
       }
       elseif(strtolower($offer->os) == "android"){
           return "https://play.google.com/store/apps/details?id=".substr($offer->packagename,2);
       }
       else return "";
    
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
        $tmparr = explode(" ", $offer->pricing);
         return $tmparr[1];
    }
    
    public function getOfferCurrency($offer){
      $tmparr = explode(" ", $offer->pricing);
         return $tmparr[0];
    }
    
    public function getOfferTrackingLink($offer){
        

        
       
       if (null != $offer->targeturl)   {
           
           $arrmacro= explode("&", $offer->targeturl);
         //  print_r($arrmacro); 
           foreach($arrmacro as $key=>$m){
               //echo $key."   ".(string)strpos($m,"gaid=").PHP_EOL;
               if(strstr($m,"gaid=") != FALSE || strstr($m,"androidId=") != FALSE || 
                  strstr($m,"IDFA=")!= FALSE || strstr($m,"subid1=") !=FALSE  ||
                  strstr($m,"subid2=") !=FALSE || 
                  strstr($m,"subid5=") !=FALSE || strstr($m,"pkgName=") !=FALSE ||
                  strstr($m,"subid3=") !=FALSE || strstr($m,"subid4=") !=FALSE || strstr($m,"optionalParams=") !=FALSE    
                       ){
                   //echo $key."   ".$m.PHP_EOL;
                   unset($arrmacro[$key] );
               }
           }
          // print_r($arrmacro); 
          return implode("&", $arrmacro);
       }
        else return "";
        
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
       
                 return $offer->imageattributes->directimageurl;
        
    }
     public function getOfferActionRequire($offer){ 
         
        return "Install&Open";
         
    }
    

     public function getBlacklistedSources($offer){
         return "";
    }

     public function getBlacklistedAffiliates($offer){
         return "";
    }

  
    public function getOffers(){

           $_options = array(
            CURLOPT_COOKIE => 1,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
            CURLOPT_RETURNTRANSFER => 1,
            );       
         
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          $curl->set_method('get')  ;
          $curl->set_options($_options);  
          $curl->set_params(array('requestParams' => '{"ads":[{"adViewId":"3006","excludedCreatives":[],"size":"1000","startIndex":"0"}],"apiVersion":"12","deviceLanguage":"en","hashCode":"QEfsvwoVFQjpC0VCU8EAPExTHzjz9UtDTD/hQklKBuj0PUBDGQm5uwoV","isMobile":"true","currencyCode":"USD","requestSource":"PUBLIC","directImageUrl":1}'));
          $curlResult = $curl->execute();
       
         $offersResult=json_decode($curlResult->response()->body); 
  //print_r($offersResult); exit(0);
        if (isset($offersResult->updatelist) && (count($offersResult->updatelist) > 0) ){
             $arrChanged = array();   
            foreach ($offersResult->updatelist as $_offer){
                
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
// print_r($this->_offers); 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                        //  break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
