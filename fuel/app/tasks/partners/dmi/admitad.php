<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class admitad  extends Shared {
    //put your code here
    private $_url = 'https://api.admitad.com/advcampaigns/website/1120842/?connection_status=active'; 
    private $_tokenUrl ='https://api.admitad.com/token/';            
    
    public $_affiseadvertiserId = '5d484792c91b188c008b45b4';
     
    public $_macros = '?subid4={clickid}&subid={pid}&website_name={sub5}';//????????
    
    public $_currency = array() ;           
    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
        $this->_currency = $this->_affise->_getCurrency();
   
        
    } 
    
 public function _CreateDmiAffiseOfferData($offer,$update = false){
     
        
  $title =  urlencode($offer['title']);
  $description = urlencode($offer['description']);
  $trackingLink = isset($offer['macros']) ? urlencode($offer['trackingLink'] . $offer['macros']) : urlencode($offer['trackingLink'] );
  $previewLink = urlencode($offer['previewLink']);
//  $strictlyOs = strtolower($offer['os']);
  $external_offer_id = $offer['offerId'];
  $status = "active";

  //print_r(urldecode($trackingLink));die;
    if(!$update ){
      $data = "title={$title}&advertiser={$offer['affiseAdvertiserId']}&url={$trackingLink}&url_preview={$previewLink}&description_lang[en]={$description}&status={$status}&privacy={$offer['privacy']}&strictly_country=1&redirect_type=http302hidden&external_offer_id={$external_offer_id}&uniqIpOnly=1&rejectNotUniqIp=1&click_session=6h&minimal_click_session=15s";
    } else {
      $data = "status={$status}&uniqIpOnly=1&rejectNotUniqIp=1&is_redirect_overcap=1&url={$trackingLink}&strictly_country=1";//&url={$trackingLink}&url_preview={$previewLink}&status={$status}&strictly_os[os][0]={$strictlyOs}&advertiser={$offer['affiseAdvertiserId']}";
    }

  if ($offer['bizModel'] == 'CPI'){
      $data .= "&is_cpi=1";
  }
  else{
      $data .= "&is_cpi=0";
  }


  //$revenue = $offer['payout'] * 0.7;
  
  $goal = 1;
   $i = 0 ;
  
 foreach($offer['payout'] as $t){
 
     if($t['name'] == 'Rejection') continue;
     $j=0;
  foreach($t['tariffs'] as $tt) {
       
          if ($j > 0) continue;      
      foreach($tt['rates'] as $payment){
 
     // if (count($payment['countriy']) == 0) continue;
      $curOs = $offer['os'];
     // if(count($curOs1) > 0) $curOs = implode(",",$curOs1 ); else $curOs='';
    //  echo ($curOs).PHP_EOL;
      $curGoal = $goal;
      $curCurrency = $offer['currency'];
      
      $curTotal = $payment['size'] ;
      // echo "currency=".$curCurrency."\n";
      if ($curCurrency == "RUB") {
         $curTotal =   round(($curTotal / $this->_currency->quotes->RUB), 2);
         $curCurrency ="USD";
     // echo "converted ".$curTotal.PHP_EOL;
      }
       if ($curCurrency == "EUR") {
         $curTotal =   round(($curTotal / $this->_currency->quotes->EUR), 2);
         $curCurrency ="USD";
     // echo "converted ".$curTotal.PHP_EOL;
      }
      $curRevenue = $curTotal * 0.7;
//      $curCurrency = $offer['currency'];
      $curPayoutType = $offer['payoutType'];
      
      if(strlen($curOs) > 0)
          $data .= "&payments[{$i}][os][]={$curOs}&payments[{$i}][goal]={$curGoal}&payments[{$i}][total]={$curTotal}&payments[{$i}][revenue]={$curRevenue}&payments[{$i}][currency]={$curCurrency}&payments[{$i}][type]={$curPayoutType}&payments[{$i}][devices][]=mobile&payments[{$i}][devices][]=tablet";
      else 
          $data .= "&payments[{$i}][goal]={$curGoal}&payments[{$i}][total]={$curTotal}&payments[{$i}][revenue]={$curRevenue}&payments[{$i}][currency]={$curCurrency}&payments[{$i}][type]={$curPayoutType}&payments[{$i}][devices][]=mobile&payments[{$i}][devices][]=tablet";
          $geo = $payment['country'];      
          if(strtolower($geo) == 'uk') $geo = "GB";
          if ( strlen($geo) > 1){
          $geo = strtoupper($geo);
          $data .= "&payments[{$i}][countries][]={$geo}";
          }
        //  $data .= "&countries[]={$geo}";
    $i++; 
  }   
  $j++;
  }
   
     }
     $data .="&strictly_country=1";
 
if((isset($offer['cap']) && $offer['cap'] != 0 ) || isset($offer['budget']) && $offer['budget'] > 0){

 if(isset($offer['cap']) && $offer['cap'] != 0 ){
    $newCap[] = [
      "period" => "day",
      "type" => "conversions",
      "goal_type" => "all",
      "affiliates" => [],
      "affiliate_type" => "all",
      "value" => $offer['cap'],
      "goals" => []
    ];
 }
 elseif(isset($offer['budget']) && $offer['budget'] > 0){
      $newCap[] = [
        "period" => "day",
        "type" => "budget",
        "goal_type" => "all",
        "affiliates" => [],
        "affiliate_type" => "all",
        "value" => $offer['budget'],
        "goals" => []
      ];
    }
   
    $caps = array("caps" => $newCap);
 
    $caps = urldecode(http_build_query($caps));
    $data .= "&" . $caps;
 

}



  $kpi = urlencode($offer['kpi']);
  $data .= "&kpi[en]={$kpi}";
  if(isset($offer['is_redirect_overcap'])) $data .= "&is_redirect_overcap=1";    
if(isset($offer['categories']) && !empty($offer['categories']) && strlen($offer['categories'] > 0)){
  $categories = explode(",",$offer['categories']);
        foreach ($categories as $_category){
            $category = urlencode($_category);
            $data .= "&categories[]={$category}";
        }
}

      if (isset($offer['subAccounts']) && strlen($offer['subAccounts']) > 0 && $offer['subAccountsExcept'] == 1){
          $data .= "&sub_account_1={$offer['subAccounts']}&sub_account_1_except={$offer['subAccountsExcept']}" ;
       }

 return $data;


        }    
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){
       return $offer->name;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->description;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = array();
     
       if (null != $offer->action_countries) {
           foreach ($offer->action_countries as $country){
           array_push($geos, $country); 
           }
        }
 
       if (empty($geos)) return "";
         return implode(",", $geos);
    }

    public function getOfferOs($offer){
       if(strstr($offer->name,"Android") != FALSE)  $platform = "Android"; 
       else   $platform = "iOS";
       return $platform;
    
    }


   public function getOfferBizModel($offer){

            return "CPI";

    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      return  $offer->action_testing_limit;
    }

    public function getOfferKpi($offer){
     $kpi = $offer->raw_description;
       
        return $kpi;
        
    }

    public function getOfferMinOsVersion($offer){
    if(strstr($offer->name,"Android") != FALSE) return "8";
             else   return "4.4";
            
    }
    
     public function getOfferPreviewLink($offer){

      return $offer->site_url;
    
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
        return $offer->actions_detail;
       
//        $rate=  preg_replace("/[^0-9\.]/", '', $offer->actions[0]->payment_size);
//     //   echo $offer->currency."  ".$rate.PHP_EOL;
//        if ($offer->currency == "RUB")         return $this->convertRubToUsd($rate);
//        else return $rate;

    }
    
    public function getOfferCurrency($offer){
     
         return $offer->currency;
    }
    
    public function getOfferTrackingLink($offer){
        

         return $offer->gotolink;
        
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
       
                 return $offer->image;
        
    }
     public function getOfferActionRequire($offer){ 
         
        return "Install&Open";
         
    }
    

     public function getBlacklistedSources($offer){
         return "";
    }

     public function getBlacklistedAffiliates($offer){
         return "";
    }
    
    public function getCurrencyArray(){
        
    }

  public function convertRubToUsd($base_price){
 
    $req_url = 'https://api.exchangerate-api.com/v4/latest/RUB';
    $response_json = file_get_contents($req_url);

// Continuing if we got a result
    if(false !== $response_json) {

        // Try/catch for json_decode operation
        try {

            // Decoding
            $response_object = json_decode($response_json);
          //  print_r($response_object);
            $USD_price = round(($base_price * $response_object->rates->USD), 2);

        }
        catch(Exception $e) {
            $USD_price = 0;
        }

    }
                return $USD_price;
}


    public function getOffers(){
        

            $curl = \Fuel\Core\Request::forge($this->_tokenUrl,'curl');
            $curl->set_method('post')  ;
            $curl->set_header('Authorization', 'Basic MmQ2YjIxNjNjNzBmNGIyZWNjYmJkZjc0ZDBkNjRlOmEyNjJkMzlhZDY5YjFlYzAwMjllNGI2OWM1MTdmOQ==');
            $curl->set_params(array('form-data' => 'grant_type=client_credentials&client_id=2d6b2163c70f4b2eccbbdf74d0d64e&scope=manage_tickets manage_opt_codes arecords broker_application deeplink_generator private_data manage_lost_orders broken_links manage_offline_receipts advcampaigns_for_website statistics banners_for_website advcampaigns public_data announcements manage_broken_links tickets validate_links manage_broker_application private_data_phone webmaster_retag opt_codes manage_advcampaigns manage_webmaster_retag offline_sales coupons landings manage_websites referrals private_data_balance manage_payments websites private_data_email payments coupons_for_website banners offline_receipts lost_orders'));
            $curlResult =$curl->execute();
 
            $curlRes=json_decode($curlResult->response()->body);  

            $token = $curlRes->access_token;
           $offset=0; 
           $limit = 50;
           $loop = true;
  while ($loop){             
          $curl = \Fuel\Core\Request::forge($this->_url."?limit={$limit}&offset={$offset}",'curl');
          $curl->set_method('get')  ;
          $curl->set_header('Authorization', 'Bearer '.$token);
         // $curl->set_options($_options); 
          
         // $curl->set_params(array('limit' => '500', 'offset' => $offset ));
          $curlResult = $curl->execute();
       
         $offersResult=json_decode($curlResult->response()->body); 
     //    echo ($curlResult->response()->body); exit(0);
        
         if (isset($offersResult->results) && (count($offersResult->results) > 0) ){
             
             $arrChanged = array();   
            foreach ($offersResult->results as $_offer){
                //if($_offer->connected == false ) continue;
                 if (!isset($_offer->gotolink) or strlen($_offer->gotolink) == 0) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
        
        //echo "offset = ".$offersResult->_meta->offset." limit = ".$offersResult->_meta->limit." count = ".$offersResult->_meta->count;
        if (($offersResult->_meta->offset + $offersResult->_meta->limit) > $offersResult->_meta->count){
            $loop =false;
            break;
            }   
        $offset = $offset + $limit;
        }
        echo count($this->_offers);
//print_r($this->_offers);
 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                   
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->_CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                        //  break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->_CreateDmiAffiseOfferData($arr,true);
                                  $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   
                                   $data = $this->_CreateDmiAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                   
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
