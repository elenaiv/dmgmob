<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class crunchie extends Shared {
    //put your code here
    private $_url = 'http://api.crunchiemedia.com/api/index.cfm?token=92a84d9f9e1114aabf05572698331086'; 
 
    
    public $_affiseadvertiserId = '5d2d816cc91b185b008b4971';
     
    public $_macros = '&s={clickid}&placement={pid}_{sub1}&s1={sub5}&s2={sub5}&deviceid={sub4}&p=2927d16e1b111c1cfb35269804485bfc'; //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->ID;
    }
 
    public function getOfferName($offer){
       return $offer->AppName;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->BannerText;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = array();
       if(isset($offer->SupportedCountries) && count($offer->SupportedCountries) > 0 ) {
           foreach ($offer->SupportedCountries as $country){
               
                  array_push($geos, $country);
            }
       }
       if (empty($geos)) return "";
         return implode(",", $geos);
    
    }

    public function getOfferOs($offer){
        $_os= array();
        if (isset($offer->SupportedPlatforms) && count($offer->SupportedPlatforms) >0)
            foreach($offer->SupportedPlatforms as $os){
             if(strtolower($os)== 'ios' || strtolower($os)== 'ipad' || strtolower($os)== 'iphone') {
              if(!in_array("iOS",$_os)) array_push($_os, "iOS");
             }
             else
               if(!in_array("Android",$_os))  array_push($_os, "Android"); 
            }
            if (empty($_os)) return "";
            return implode(",", $_os);
    }


   public function getOfferBizModel($offer){
                return strtoupper($offer->PayoutType);
       

    }

    public function getOfferCapType($offer){
             
        return 'daily_cap';
    }

    public function getOfferCap($offer){
  
     if (isset($offer->AvailableLeads)  && $offer->AvailableLeads > 0 ) return $offer->AvailableLeads;
     return 0;
    }

    public function getOfferKpi($offer){
        
       
        return $offer->PublisherNotes;
        
    }

    public function getOfferMinOsVersion($offer){
          if(strstr($offer->PromoVideoURL,"play.google.com") != FALSE) return "4.4";
             else   return "8";

    }
    
     public function getOfferPreviewLink($offer){

        return $offer->PreviewLink;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->Payout;
    }
    
    public function getOfferCurrency($offer){
      return "USD";//$offer->currency;
    }
    
    public function getOfferTrackingLink($offer){ //!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       if (null != $offer->TrackingLink)   {
           
           $arrmacro= explode("&", $offer->TrackingLink);
        //   print_r($arrmacro); 
           foreach($arrmacro as $key=>$m){
              
               if(strstr($m,"o=") == FALSE  ){
                
                   unset($arrmacro[$key] );
               }
           }
           //print_r($arrmacro); exit(0);
         //  echo implode("&", $arrmacro).PHP_EOL;
           return implode("&", $arrmacro);
       }
        else return "";
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
       return $offer->AppIconURL;
    }
     public function getOfferActionRequire($offer){ 
        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

  
    public function getOffers(){
$arrTmp =[9545085,9545078,9545066,9545035,9545028,9545021,9853754,9858626,9545148,9545135,9545124,9545116,9545099,9545098,9873941,9817844,9874137,9761594,9761585];
              $curl = \Fuel\Core\Request::forge($this->_url,'curl');
              $curl->set_method('get')  ;
              $curlResult = $curl->execute();

       
         $offersResult=json_decode($curlResult->response()->body); 
        
         if (isset($offersResult->Offer) && (count($offersResult->Offer) > 0) ){
            $arrChanged = array();
            foreach ($offersResult->Offer as $_offer){
               if(strstr($_offer->TrackingLink, 'http://') == FALSE 
                        && strstr($_offer->TrackingLink,'https://') ==FALSE 
                        && strstr($_offer->TrackingLink, 'itms-apps://') == FALSE 
                        && strstr($_offer->TrackingLink,'market://') == FALSE 
                        && strstr($_offer->TrackingLink, 'links\\n')  == FALSE ) continue;
                   if (!in_array($_offer->ID, $arrTmp)) continue;
                    $offer = new \stdClass();
                    

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = "active";
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;
              //  print_r($this->_offers); 
                
            }
           
        }
   
    
  
       // print_r($this->_offers); exit(0);
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                      //    break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                 //  echo($data).PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
//              exit(0);  
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged);                 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
      
}
