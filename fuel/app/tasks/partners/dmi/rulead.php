<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class rulead extends Shared {
    //put your code here
    private $_url = 'http://api.rulead.com/affiliate/offer/findAll/?token=ES4mEVQUp0nz9B4fYoycgVb293TUyJCD'; 
 
    
    public $_affiseadvertiserId = '5cfe5c5cc91b184c008b456f';
     
    public $_macros = '&aff_sub={clickid}&aff_sub2={pid}_{sub2)&idfa={sub4}&aff_sub4={sub5}';
                       
    

     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->ID;
    }
 
    public function getOfferName($offer){
       return $offer->Name;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->Description;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = "";
       if(isset($offer->Countries) && strlen($offer->Countries) > 0 ) $geos= $offer->Countries;
       return $geos;
    }

    public function getOfferOs($offer){
             if(strpos($offer->Preview_url,'itunes') > 0){// - Optional
           $osName = "iOS";
    } else {
           $osName = "Android";
    }
       return $osName;
    
    }


   public function getOfferBizModel($offer){

            return "CPI";

    }

    public function getOfferCapType($offer){
        if (isset($offer->Daily_cap)  && $offer->Daily_cap > 0 ) return 'daily_cap';
        if (isset($offer->Monthly_cap)  && $offer->Monthly_cap > 0 ) return 'monthly_cap';         
        return 'daily_cap';
    }

    public function getOfferCap($offer){
  
     if (isset($offer->Daily_cap)  && $offer->Daily_cap > 0 ) return $offer->Daily_cap;
     if (isset($offer->Daily_cap)  && $offer->Monthly_cap > 0 ) return $offer->Monthly_cap; 
     return 0;
    }

    public function getOfferKpi($offer){
        
        $restrictions = array();
        foreach($offer->Restrictions as $rest){
           array_push($restrictions, $rest->name) ;
        }
        if (count($restrictions) == 0 ) $kpi = "";
        else   $kpi = "Restrictions: ".implode(",", $restrictions);
       
        return $kpi;
        
    }

    public function getOfferMinOsVersion($offer){
          if(strstr($offer->Preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";

    }
    
     public function getOfferPreviewLink($offer){

        return $offer->Preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->Payout;
    }
    
    public function getOfferCurrency($offer){
      return $offer->Currency;
    }
    
    public function getOfferTrackingLink($offer){
       
        return $offer->Tracking_url;
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
       return $offer->Icon_url;
    }
     public function getOfferActionRequire($offer){ 
        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

  
    public function getOffers(){

                   
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          $curl->set_method('get')  ;
          $curlResult = $curl->execute();
        
       
         $offersResult=json_decode($curlResult->response()->body); 
//  print_r($offersResult); exit(0);
        if (isset($offersResult->offers) && (count($offersResult->offers) > 0) ){
            $arrChanged = array();    
            foreach ($offersResult->offers as $_offer){
               if(strstr($_offer->Tracking_url, 'http://') == FALSE 
                        && strstr($_offer->Tracking_url,'https://') ==FALSE 
                        && strstr($_offer->Tracking_url, 'itms-apps://') == FALSE 
                        && strstr($_offer->Tracking_url,'market://') == FALSE 
                        && strstr($_offer->Tracking_url, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();
                    

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = $_offer->Status;
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;
               // print_r($this->_offers); break;
                
            }
           
        }
 //print_r($this->_offers); 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                       //   break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                 //  echo($data).PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged);                 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
