<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class adinall extends Shared {
    //put your code here
    private $_url = 'http://g.cpi.adinall.com/api/v1/offers.do?key=fcdeea2ea0a8ec0bb8d2e15393e2af85&aff_id=473'; 
 
    
    public $_affiseadvertiserId = '5d06236bc91b18c6008b456e';
     
    public $_macros = '&aff_sub1={clickid}&source_id={pid}_{sub2}&ios_idfa={sub4}&google_aid={sub4}&app_name={sub5}';
    
                       
    

     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->offer_id;
    }
 
    public function getOfferName($offer){
       return $offer->offer_name;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->description;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = array();
       if(isset($offer->countries) && count($offer->countries) > 0 ) {
           foreach ($offer->countries as $country){
               
                  array_push($geos, $country->country);
            }
       }
       if (empty($geos)) return "";
         return implode(",", $geos);
    
    }

    public function getOfferOs($offer){
             if(strtolower($offer->os)== 'ios') return "iOS";
             else   return "Android";
  
    }


   public function getOfferBizModel($offer){
                if ($offer->type == 1)  return "CPI";
                else return "CPA";

    }

    public function getOfferCapType($offer){
             
        return 'daily_cap';
    }

    public function getOfferCap($offer){
  
     if (isset($offer->cap)  && $offer->cap > 0 ) return $offer->cap;
     return 0;
    }

    public function getOfferKpi($offer){
        
       
        return $offer->description;
        
    }

    public function getOfferMinOsVersion($offer){
          if(strstr($offer->preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";

    }
    
     public function getOfferPreviewLink($offer){

        return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->payout;
    }
    
    public function getOfferCurrency($offer){
      return $offer->currency;
    }
    
    public function getOfferTrackingLink($offer){
       if (null != $offer->tracking_url)   {
           
           $arrmacro= explode("&", $offer->tracking_url);
         //  print_r($arrmacro); 
           foreach($arrmacro as $key=>$m){
              
               if(strstr($m,"offer_id=") == FALSE && strstr($m,"aff_id=") == FALSE ){
                
                   unset($arrmacro[$key] );
               }
           }
           //print_r($arrmacro); exit(0);
//           echo implode("&", $arrmacro).PHP_EOL;
          return implode("&", $arrmacro);
       }
        else return "";
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
       return $offer->logo_url;
    }
     public function getOfferActionRequire($offer){ 
        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

  
    public function getOffers(){

           $limit = 500;
           $page = 1;  
    do{
              $curl = \Fuel\Core\Request::forge($this->_url.'&limit='.$limit.'&page='.$page,'curl');
              $curl->set_method('get')  ;
              $curlResult = $curl->execute();

       
         $offersResult=json_decode($curlResult->response()->body); 
         if (!isset($offersResult->offers) || count($offersResult->offers) == 0) break;
         if (isset($offersResult->offers) && (count($offersResult->offers) > 0) ){
            $arrChanged = array();
            foreach ($offersResult->offers as $_offer){
               if(strstr($_offer->tracking_url, 'http://') == FALSE 
                        && strstr($_offer->tracking_url,'https://') ==FALSE 
                        && strstr($_offer->tracking_url, 'itms-apps://') == FALSE 
                        && strstr($_offer->tracking_url,'market://') == FALSE 
                        && strstr($_offer->tracking_url, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();
                    

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = "active";
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;
               // print_r($this->_offers); break;
                
            }
           
        }
        if($offersResult->total < $limit) break;
        $page++;
    } while(1);
//print_r($this->_offers); exit(0);
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);

         if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                       //   break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                 //  echo($data).PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged);                 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
      
}
