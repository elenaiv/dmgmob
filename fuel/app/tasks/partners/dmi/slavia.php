<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class slavia  extends Shared {
    //put your code here
    private $_url = 'http://api.slaviamobile.com/affiliate/offer/findAll/?token=flSJvYBIejWSZU6ZHjU6P6KOqgmzeYRV&approved=1'; 
 
    
    public $_affiseadvertiserId = '5d482ea1c91b1882008b45af';
     
    public $_macros = '&aff_sub={clickid}&aff_sub2={pid}_{sub2}_{sub1}&aff_sub3={sub4}&aff_sub4={sub5}';
    

    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->ID;
    }
 
    public function getOfferName($offer){
       return $offer->Name;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->Description;
    }  
   
    public function getOfferGeos($offer){ 
      return $offer->Countries;
    }

    public function getOfferOs($offer){
       $_os= array();
        if (isset($offer->Platforms) )
            $pl = explode (",", $offer->Platforms);
            foreach($pl as $os){
             if(strtolower($os)== 'ios' || strtolower($os)== 'ipad' || strtolower($os)== 'iphone') {
              if(!in_array("iOS",$_os)) array_push($_os, "iOS");
             }
             else
               if(!in_array("Android",$_os))  array_push($_os, "Android"); 
            }
            if (empty($_os)) return "";
            return implode(",", $_os);
    }


   public function getOfferBizModel($offer){

            return "CPI";

    }

    public function getOfferCapType($offer){

       if($offer->Daily_cap > 0 ) $capType = 'daily_cap';
       elseif($offer->Monthly_cap > 0 ) $capType = 'mounthly_cap';
       else $capType = 'daily_cap';
        return $capType;
    }

    public function getOfferCap($offer){
  
      if($offer->Daily_cap > 0 ) return $offer->Daily_cap;
       elseif($offer->Monthly_cap > 0 ) return $offer->Monthly_cap;
       return 0;
    }

    public function getOfferKpi($offer){
      $restrictions = array();
      
        foreach($offer->Restrictions as $rest){
           array_push($restrictions, $rest->name) ;
        }
        if (count($restrictions) == 0 ) $kpi = "";
        else   $kpi = "Restrictions: ".implode(",", $restrictions);
       
        return $kpi;
        
        
    }

    public function getOfferMinOsVersion($offer){
          if(strstr($offer->Preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";
            
    }
    
     public function getOfferPreviewLink($offer){

        return $offer->Preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->Payout;
    }
    
    public function getOfferCurrency($offer){
      return $offer->Currency;
    }
    
    public function getOfferTrackingLink($offer){
        return $offer->Tracking_url;
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
        return $offer->Icon_url;
    }
     public function getOfferActionRequire($offer){ 
        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";

    }

     public function getBlacklistedAffiliates($offer){
         return "";
    }

  
    public function getOffers(){

           $_options = array(
            CURLOPT_COOKIE => 1,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
            CURLOPT_RETURNTRANSFER => 1,
            );       
         
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          $curl->set_method('get')  ;
          $curl->set_options($_options);  
          //$curl->set_params(array('requestParams' => '{"affId":"25349","placementId":"2005","packageName":"","subid1":"","subid2":"","subid3":"","subid4":"","subid5":"", "requestSource":"PUBLIC"}'));
          $curlResult = $curl->execute();
        
       
         $offersResult=json_decode($curlResult->response()->body); 
  
        if (isset($offersResult->offers) && (count($offersResult->offers) > 0) ){
            $arrChanged = array();
            foreach ($offersResult->offers as $_offer){
                if(strstr($_offer->Preview_url, 'http://') == FALSE 
                        && strstr($_offer->Preview_url,'https://') ==FALSE 
                        && strstr($_offer->Preview_url, 'itms-apps://') == FALSE 
                        && strstr($_offer->Preview_url,'market://') == FALSE 
                        && strstr($_offer->Preview_url, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
// print_r($this->_offers); 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                         // break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                 //  echo($data).PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
