<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class inmobi  extends Shared {
    //put your code here
    private $_url = 'https://api.inmobi.com/iap/v0/json?api_key=2bf80893-defa-40a5-8baf-86b10a051298&target=affiliate_offer&method=findMyApprovedOffers'; 
 
    
    public $_affiseadvertiserId = '5d493f90c91b18c2008b45de';
     
    public $_macros = '&aff_sub={clickid}&aff_sub2={pid}_{sub1}&ios_ifa={sub4}&google_aid={sub4}&aff_sub4={sub5}';
    

    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
//        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){
       return $offer->name;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->description;
    }  
   
    public function getOfferGeos($country){ 
        
//        print_r($country);
      $geos = array();
       if (isset($country) && !empty($country)){
                foreach ($country as $c=>$value){
                  array_push($geos, $c);
            }
  
            }
            if (count($geos) == 0 ) return "";
         return implode(",", $geos);
    }
    
    public function getOfferOs($offer){
       
            return $offer->os;
    }


   public function getOfferBizModel($offer){

            return "CPI";

    }

    public function getOfferCapType($offer){

       
        $capType = 'daily_cap';
        return $capType;
    }

    public function getOfferCap($offer){
  
      if($offer->conversion_cap > 0 ) return $offer->conversion_cap;
       return 0;
    }

    public function getOfferKpi($offer){
      
       
        return $offer->description;
        
        
    }

    public function getOfferMinOsVersion($offer){
          if(strstr($offer->preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";
            
    }
    
     public function getOfferPreviewLink($offer){

        return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->default_payout;
    }
    
    public function getOfferCurrency($offer){
      return $offer->currency;
    }
    
    public function getOfferTrackingLink($offer){
        return $offer->click_url;
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){
        return "";
    }
     public function getOfferActionRequire($offer){ 
        return  $offer->goal_name;
    }
    

     public function getBlacklistedSources($offer){
         return "";

    }

     public function getBlacklistedAffiliates($offer){
         return "";
    }

  
    public function getOffers(){

//           $_options = array(
//            CURLOPT_COOKIE => 1,
//            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
//            CURLOPT_RETURNTRANSFER => 1,
//            );       
        
        $curl = \Fuel\Core\Request::forge('https://api.inmobi.com/v1.0/generatesession/generate','curl');
          $curl->set_method('get')  ;
          $curl->set_header('userName', 'kerengol@dsnrmg.com');
          $curl->set_header('secretKey', '9d019cb4dae64db59c7117c28eaaeae5');
          $curlResult = $curl->execute();
          //print_r($curlResult); exit(0);
          $offersResult=json_decode($curlResult->response()->body);       
          $session = $offersResult->respList[0]->sessionId;
          
          
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          $curl->set_method('get')  ;
          $curl->set_header('Sessionid', $session);
          $curl->set_header('Accountid', 'd7d91e2fdffd4e70a84369cea885d091');
          $curl->set_header('secretKey', '9d019cb4dae64db59c7117c28eaaeae5');
            
        //  $curl->set_options($_options);  
        
          $curlResult = $curl->execute();
       
         if (isset($curlResult->response()->body)) $offersResult=json_decode($curlResult->response()->body); 
  
        if (isset($offersResult->response->data) && (count($offersResult->response->data->data) > 0) ){
            $arrChanged = array();
            foreach ($offersResult->response->data->data as $_offer){
//                if(strstr($_offer->Preview_url, 'http://') == FALSE 
//                        && strstr($_offer->Preview_url,'https://') ==FALSE 
//                        && strstr($_offer->Preview_url, 'itms-apps://') == FALSE 
//                        && strstr($_offer->Preview_url,'market://') == FALSE 
//                        && strstr($_offer->Preview_url, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();
                    $offer->offerId = $this->getOfferId($_offer->Offer);
                    $offer->title = $this->getOfferName($_offer->Offer);
                    $offer->description = $this->getOfferDescription($_offer->Offer);
                    $offer->geos = $this->getOfferGeos($_offer->Country);                    
                    $offer->os = $this->getOfferOs($_offer->Offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer->Offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer->Offer);
                    $offer->capType = $this->getOfferCapType($_offer->Offer);
                    $offer->cap = $this->getOfferCap($_offer->Offer); 
                    $offer->kpi = $this->getOfferKpi($_offer->Offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer->Offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer->Offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer->Offer);
                    $offer->payout = $this->getOfferPayout($_offer->Offer);
                    $offer->currency = $this->getOfferCurrency($_offer->Offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer->Offer);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer->Offer);
                    $offer->logo = $this->getOfferLogo($_offer->Offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
// print_r($this->_offers); exit(0);
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
// 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                         // break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                 //  echo($data).PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
