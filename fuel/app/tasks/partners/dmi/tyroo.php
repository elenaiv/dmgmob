<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class tyroo  extends Shared {
    //put your code here
    private $_url = 'http://video.tyroo.com/image'; 
 
    
    public $_affiseadvertiserId = '5cf4e2bdc91b1879008b4567';
     
    public $_macros = '&gaid={sub4}&androidId={sub4}&IDFA={sub4}&subId1={clickid}&subId2={pid}_{sub1}&subId5={sub5}&pkgName={sub5}';
    

    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise('dmi');
        $this->_categories = $this->_getAffiseCategories();
        
        
    } 
    public function getOfferId($offer){

        return $offer->campaignId;
    }
 
    public function getOfferName($offer){
       return $offer->campaignName;
    }
    
    public function getOfferDescription($offer){
        
        return $offer->title." \n".$offer->subtitle;
    }  
   
    public function getOfferGeos($offer){ 
       $geos = array();
       if (isset($offer->include->geo) && !empty($offer->include->geo) ){
                foreach ($offer->include->geo as $country){
                 if (strlen($country) < 3) array_push($geos, $country);
            }
  
        }
 
         if (count($geos) == 0 ) return "";
         return implode(",", $geos);
    }

    public function getOfferOs($offer){
                $os = array();
         if (isset($offer->include->os) && !empty($offer->include->os)){
                foreach ($offer->include->os as $key=>$value){
                  array_push($os, $key);
            }
            if (count($os) == 0 ) return "";
             return implode(",", $os);
         }
         return '';
    
    }


   public function getOfferBizModel($offer){

            return strtoupper($offer->model);

    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      return isset($offer->Install) ? $offer->Install : 0;
    }

    public function getOfferKpi($offer){
     $kpi = $offer->KPI;
       
        return $kpi;
        
    }

    public function getOfferMinOsVersion($offer){
          $osVer = array();
         if (isset($offer->include->os) && !empty($offer->include->os)){
                foreach ($offer->include->os as $key=>$value){
                 $osVer =$value;
            }
            if (count($osVer) == 0 ) return "";
             return implode(",", $osVer);
         }
         return '';

    }
    
     public function getOfferPreviewLink($offer){

        return $offer->storeURL;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->pricing;
    }
    
    public function getOfferCurrency($offer){
      return $offer->currency;
    }
    
    public function getOfferTrackingLink($offer){
        foreach($offer as $o){
       if (isset($o->clickURL) )  {
           
           $arrmacro= explode("&", $o->clickURL);
          // print_r($arrmacro); 
           foreach($arrmacro as $key=>$m){
               //echo $key."   ".(string)strpos($m,"gaid=").PHP_EOL;
               if(strstr($m,"gaid=") != FALSE || strstr($m,"androidId=") != FALSE || 
                  strstr($m,"IDFA=")!= FALSE || strstr($m,"subId1=") !=FALSE  ||
                  strstr($m,"subId2=") !=FALSE || 
                  strstr($m,"subId5=") !=FALSE || strstr($m,"pkgName=") !=FALSE ||
                  strstr($m,"subId3=") !=FALSE || strstr($m,"subId4=") !=FALSE || strstr($m,"optionalParams=") !=FALSE    
                       ){
                   //echo $key."   ".$m.PHP_EOL;
                   unset($arrmacro[$key] );
               }
           }
          // print_r($arrmacro); exit(0);
          return implode("&", $arrmacro);
       }
        else return "";
        }
    }


    public function getOfferCategories($offer){

                   return $offer->category;
    }
    
     public function getOfferLogo($offer){
        foreach($offer as $o){
                 return $o->imageURL;
        }
    }
     public function getOfferActionRequire($offer){ 
         foreach($offer as $o){

        return $o->offerType;
         }
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

  
    public function getOffers(){

           $_options = array(
            CURLOPT_COOKIE => 1,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
            CURLOPT_RETURNTRANSFER => 1,
            );       
         
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          $curl->set_method('get')  ;
          $curl->set_options($_options);  
          $curl->set_params(array('requestParams' => '{"affId":"25349","placementId":"2005","packageName":"","subid1":"","subid2":"","subid3":"","subid4":"","subid5":"", "requestSource":"PUBLIC"}'));
          $curlResult = $curl->execute();
        
       
         $offersResult=json_decode($curlResult->response()->body); 
  
        if (isset($offersResult->data->offers) && (count($offersResult->data->offers) > 0) ){
            $arrChanged = array();
            foreach ($offersResult->data->offers as $_offer){
                if(strstr($_offer->advertiser->storeURL, 'http://') == FALSE 
                        && strstr($_offer->advertiser->storeURL,'https://') ==FALSE 
                        && strstr($_offer->advertiser->storeURL, 'itms-apps://') == FALSE 
                        && strstr($_offer->advertiser->storeURL,'market://') == FALSE 
                        && strstr($_offer->advertiser->storeURL, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer->advertiser);
                    $offer->title = $this->getOfferName($_offer->advertiser);
                    $offer->description = $this->getOfferDescription($_offer->advertiser);
                    $offer->geos = $this->getOfferGeos($_offer->targeting);                    
                    $offer->os = $this->getOfferOs($_offer->targeting);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer->creatives);
                    $offer->bizModel = $this->getOfferBizModel($_offer->pricing);
                    $offer->capType = $this->getOfferCapType($_offer->capping);
                    $offer->cap = $this->getOfferCap($_offer->capping); 
                    $offer->kpi = $this->getOfferKpi($_offer->advertiser); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer->targeting); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer->advertiser);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer->pricing);
                    $offer->payout = $this->getOfferPayout($_offer->pricing);
                    $offer->currency = $this->getOfferCurrency($_offer->pricing);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer->creatives);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer->advertiser);
                    $offer->logo = $this->getOfferLogo($_offer->creatives);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
 //print_r($this->_offers); 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
 
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
              
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                         // break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged); 
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateDmiAffiseOfferData($arr);
                                 //  echo($data).PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                         
                 break;

                }
                
            }
          
            foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("dmi",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
