<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class koneo  extends Shared {
    //put your code here
    public $_url = "https://system.koneomobile.com/feed/campaigns?api_key=12o5zw3b072848cok84k04gksgkcgckk0ow8wcsggkocoos48o&format=json";
 
    
    public $_affiseadvertiserId = '5c6c0672668f0f69008b4c13';
    
    public $_macros = '&click_id={clickid}&sub_id1={pid}_{sub3}&sub_id5={sub4}&sub_id3={sub5}';
    

    
     public function __construct(){
        $this->_affise = new \Platforms\Affise();
//        $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

       return $offer->name;
    }
    public function getOfferDescription($offer){
        if(isset($offer->description) && strlen($offer->description) > 0 )    return $offer->description;
        return $offer->name;
    }  
    public function getOfferGeos($offer){ 
       
        $geos = array();
       if (isset($offer->country_codes)){
            foreach ($offer->country_codes as $_geoTargeting){
            
                    array_push($geos, $_geoTargeting);
                
            }
        }
        
        return implode(",", $geos);
    }

    public function getOfferOs($offer){
        
       if (strtolower($offer->platform) == "ios"){

            return "iOS";
        }

        return "Android";

      
    }


   public function getOfferBizModel($offer){

         if($offer->payout_cpc > 0) return "CPC";
         if($offer->payout_cpm > 0) return "CPM";
         if($offer->payout_cpo > 0) return "CPO";
         if($offer->payout_cpl > 0) return "CPL";
         if($offer->payout_cpi > 0) return "CPI";
         return 'CPI';
    }

    public function getOfferCapType($offer){
      if (isset($offer->daily_conversion_capping)){
                return "daily_cap";
            }
            if (isset($offer->daily_money_capping)){
                return "daily_cap_budget";
            }
            return "daily_cap";
    }

    public function getOfferCap($offer){
  
     if (isset($offer->daily_conversion_capping)){
                return $offer->daily_conversion_capping; 
            }
            if (isset($offer->daily_money_capping)){
                return $offer->daily_money_capping; 
            }
            return 0;
    }

    public function getOfferKpi($offer){
       return $offer->restrictions;
    }

    public function getOfferMinOsVersion($offer){
         if (strtolower($offer->platform) == "ios") $minOs ="4.4" ;
                  else $minOs = "8" ;
                      
    }
    
     public function getOfferPreviewLink($offer){

         if(strtolower($offer->platform) == "ios")
       {
           return "http://itunes.apple.com/app/".$offer->ios_bundle_id;
       }
       elseif(strtolower($offer->platform) == "android"){
           return "https://play.google.com/store/apps/details?id=".$offer->android_package_name;
       }
       else return "";
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
       
        return $payoutType;
    }

    public function getOfferPayout($offer){
         if($offer->payout_cpc > 0) return $offer->payout_cpc;
         if($offer->payout_cpm > 0) return $offer->payout_cpm;
         if($offer->payout_cpo > 0) return $offer->payout_cpo;
         if($offer->payout_cpl > 0) return $offer->payout_cpl;
         if($offer->payout_cpi > 0) return $offer->payout_cpi;
    }
    
    public function getOfferCurrency($offer){
       return 'USD';
    }
    
    public function getOfferTrackingLink($offer){
      return $offer->tracking_link;
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){

      
        return "";
    }
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

    
    public function getOffers(){

           $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;             
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
        if (isset($offersResult) && isset($offersResult) && isset($offersResult) ){
             $arrChanged = array();
            foreach ($offersResult as $_offer){

                  $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;  
                $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = $_offer->status;
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $offer->is_redirect_overcap = 1;
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                       
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
            $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
            $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "Is Active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                        $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                            $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;

                default:
                  echo "offer suspend  should be paused ".$externalOfferId.PHP_EOL;
                   $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return  $e->external_offer_id == $externalOfferId;    }));  
                   if (isset($neededObject) && is_object($neededObject)) {
                   $arr=json_decode(json_encode($neededObject), True);
                  // print_r($arr);
                   $data ="status=suspended";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);

                   }
                    break;
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                   print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);                  
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
