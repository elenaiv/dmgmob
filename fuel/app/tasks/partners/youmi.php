<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobusi
 *
 * @author lenaivch
 */
namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class youmi  extends Shared {
    //put your code here
    public $_url = "http://ad.api.yyapi.net/v2/offline?app_id=b8310b61b7478cdeff18a7e16f7322f8";

    public $_affiseadvertiserId = '5d230350668f0f9d008b484a';
    
    public $_macros = '&aff_sub={clickid}&source={pid}_{sub3}&advid={sub4}&idfa={sub4}&app_name={sub5}';
                    

     public function __construct(){
        $this->_affise = new \Platforms\Affise();
         
    } 
    

    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

        return $offer->name;
    }
    public function getOfferDescription($offer){

        return $offer->adtxt;
    }  
    public function getOfferGeos($offer){ 
       $geos = array();
        if (isset($offer->country)){
            foreach ($offer->country as $_country){
                    array_push($geos, $_country);
                }
        }
           return implode(",", $geos);
    }

    public function getOfferOs($offer){
        $_os = array();
        if (isset($offer->os) && count($offer->os) > 0){

           foreach ($offer->os as $os){
               if($os == 'android') array_push($_os, 'Android');else array_push($_os, 'iOS');
                } 
 // echo implode(",", $_os).PHP_EOL;
            return implode(",", $_os);
        }
       if(strstr($offer->preview_url,"play.google.com") != FALSE)  $platform = "Android"; 
       else   $platform = "iOS";
       return $platform;
    }



   public function getOfferBizModel($offer){
       
         return $offer->payout_type;
 
    }

    public function getOfferCapType($offer){
         return "daily_cap";
    }

      
    

    public function getOfferCap($offer){
    
        if (isset($offer->cap) && $offer->cap != ""){

            return $offer->cap;
        }

        return 5000;

       }

    public function getOfferKpi($offer){
     return $offer->kpi;
    }

    public function getOfferMinOsVersion($offer){
        if (isset($offer->os_version) && $offer->os_version != "") return $offer->os_version;
       
             if(strstr($offer->preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";
            
     }
    
     public function getOfferPreviewLink($offer){

         return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        return "fixed";
    }

    public function getOfferPayout($offer){

         return $offer->payout;
    }
    
    public function getOfferCurrency($offer){
        
            return "USD";
    }
      
    
    public function getOfferTrackingLink($offer){

         return $offer->trackinglink;
    }



    public function getOfferCategories($offer){
          return "";
      }
    
     public function getOfferLogo($offer){

        return $offer->icon_url;
    }
    
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    



     public function getBlacklistedSources($offer){
         return "";
         $blacklisted_sources =array();
        if(isset($offer->subsource_blacklist) && !empty($offer->subsource_blacklist)) {
            foreach($offer->subsource_blacklist as $value)   {
                $pos=stripos("$value","_");
                $sources= substr($value,$pos + 1);
                $blacklisted_sources[] = $sources;
           }

        }
       
        $tmp = implode(",", $blacklisted_sources);
        $tmpstr = str_replace("?", "", $tmp);
        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
        
       $affiliates =array();
        if(isset($offer->subsource_blacklist) && !empty($offer->subsource_blacklist)) {
            foreach($offer->subsource_blacklist as $value)   {
                $pos=stripos("$value","_");
                $aff = substr($value,0,$pos);
                $affiliates[] = $aff;
           }
         }
       
        return implode(",", $affiliates);
    }

    
    public function getOffers(){
        $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;             
 
         $loop = true;
         $page = 1;
         $limit=500;
      while ($loop){

            $page_url = "{$this->_url}&page={$page}&page_size={$limit}";
           //echo $page_url.PHP_EOL;  
           
         $curl = \Fuel\Core\Request::forge($page_url,'curl');

         $curl->set_method("get");
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
         if (!isset($offersResult->offers) || (count($offersResult->offers) == 0 ) ) break;
        if (isset($offersResult->total) && $offersResult->n < $limit){
                $loop = false;
            }
            else{
                $page++;
            }   

        if (isset($offersResult->offers) && (count($offersResult->offers) > 0 ) ){

            foreach ($offersResult->offers as $_offer){
 
                  $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;   
                
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                   
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer);  
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_bundle->targeting);
                    }
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
      }
    //  echo count($this->_offerIds).PHP_EOL;exit(0);
     // print_r($this->_offers); exit(0);
       
        if(count($this->_offerIds) > 0 ) {
             $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
             $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
            $arrChanged = array();
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                       // var_dump($this->_offers[$externalOfferId]); 
                        $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){ 
                              echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                               $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);

                               }
                        //edit
                         }
                         else {
                             echo "create new offer".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
//                                   echo $data.PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;
                default :
                     echo "offer inactive  - should be stopped ".$externalOfferId.PHP_EOL;
                  //search in active -if exists =>stop it
                  $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));  
        
                if (isset($neededObject) && is_object($neededObject)) {
                    $arr=json_decode(json_encode($neededObject), True);
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);
                                      
                }
                    break;
             
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);                 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        
  
}   
        
}

