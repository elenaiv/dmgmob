<?php
namespace Fuel\Tasks;
use \Model\Affisechanges;
Class Shared{
    
    
    
    public $_offerIds = array();
    public $_offers =array();
   // public $_macros = '';
    public $_activeAffiseOffers =   array();
    public $_stoppedAffiseOffers = array();
    public $_affise;  
    public $_categories = array();
    
public function _getAffiseCategories(){
        $limit = 500;
        $page = 1;
        do{
        $data = array(
                "limit" => $limit,
                "page" => $page,
            );
            $tmpArr = $this->_affise->_getCategories($data);
            $this->_categories =  array_merge( $this->_categories , $tmpArr->categories);
        //    echo $page.PHP_EOL;
            if(!isset($tmpArr->pagination->next_page)){
                break;
            }
            $page = $tmpArr->pagination->next_page;
           // break;
            
         } while(1);

    }

    
   public function getActiveAdvertiserOffers($affiseadvertiserId){
        $limit = 500;
        $page = 1;
        do{
        $data = array(
                "advertiser" => array($affiseadvertiserId),
                "status" => array("active"),
                "limit" => $limit,
                "page" => $page,
            );
            
            $tmpArr = $this->_affise->_getAffiseOffersByList($data);
           if(count($tmpArr) > 0)   $this->_activeAffiseOffers =  array_merge( $this->_activeAffiseOffers , $tmpArr->offers);
//            echo $page.PHP_EOL;
            if(!isset($tmpArr->pagination->next_page)){
                break;
            }
            $page = $tmpArr->pagination->next_page;
           // break;
            
         } while(1);
         //print_r( $this->_activeAffiseOffers);
    }
    
    
    
    public function getInActiveAdvertiserOffers($affiseadvertiserId){
        $limit = 500;
        $page = 1;
        do{
        $data = array(
                "advertiser" => array($affiseadvertiserId),
                "status" => array("stopped","suspended"),
                "limit" => $limit,
                "page" => $page,
            );
            $tmpArr = $this->_affise->_getAffiseOffersByList($data);
             if(count($tmpArr) > 0) $this->_stoppedAffiseOffers =  array_merge( $this->_stoppedAffiseOffers , $tmpArr->offers);
         //   echo $page.PHP_EOL;
            if(!isset($tmpArr->pagination->next_page)){
                break;
            }
            $page = $tmpArr->pagination->next_page;
           // break;
            
         } while(1);
    }

    
  public function CreateAffiseOfferData($offer,$update = false){
  $title =  urlencode($offer['title']);
  $description = urlencode($offer['description']);
  $trackingLink = isset($offer['macros']) ? urlencode($offer['trackingLink'] . $offer['macros']) : urlencode($offer['trackingLink'] );
  $previewLink = urlencode($offer['previewLink']);
  $strictlyOs = strtolower($offer['os']);
  $external_offer_id = $offer['offerId'];
  $status = "active";

  //print_r(urldecode($trackingLink));die;
    if(!$update ){
      $data = "title={$title}&advertiser={$offer['affiseAdvertiserId']}&url={$trackingLink}&url_preview={$previewLink}&description_lang[en]={$description}&status={$status}&privacy={$offer['privacy']}&strictly_country=1&sources[]=51f531f53b7d9b1e0382f6d9&sources[]=5adf1fd454528908407f0ac6&redirect_type=http302hidden&external_offer_id={$external_offer_id}&uniqIpOnly=1&rejectNotUniqIp=1&click_session=6h&minimal_click_session=15s";
    } else {
      $data = "status={$status}&uniqIpOnly=1&rejectNotUniqIp=1&is_redirect_overcap=1";//&url={$trackingLink}&url_preview={$previewLink}&status={$status}&strictly_os[os][0]={$strictlyOs}&advertiser={$offer['affiseAdvertiserId']}";
    }


      
  $geos = explode(",", $offer['geos']);
 //print_r($geos)  ;
  foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
     
      if ( strlen($geo) > 1) 
          {$data .= "&countries[]={$geo}"; }
      
  }

  
  if ($offer['bizModel'] == 'CPI'){
      $data .= "&is_cpi=1";
  }
  else{
      $data .= "&is_cpi=0";
  }


  $revenue = $offer['payout'] * 0.7;
  switch ($offer['actionRequire']){

      case "Install + Open":
          $goal = 1;
          break;
      deafult:
          $goal = 1;
          break;
  }
  $goal = 1;
  if(isset($offer['os']) && strlen($offer['os']) > 0)
      $data .= "&payments[0][os][]={$offer['os']}&payments[0][goal]={$goal}&payments[0][total]={$offer['payout']}&payments[0][revenue]={$revenue}&payments[0][currency]={$offer['currency']}&payments[0][type]={$offer['payoutType']}&payments[0][devices][]=mobile&payments[0][devices][]=tablet";
  else 
      $data .= "&payments[0][goal]={$goal}&payments[0][total]={$offer['payout']}&payments[0][revenue]={$revenue}&payments[0][currency]={$offer['currency']}&payments[0][type]={$offer['payoutType']}&payments[0][devices][]=mobile&payments[0][devices][]=tablet";
      foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
      if ( strlen($geo) > 1){
      $geo = strtoupper($geo);
      $data .= "&payments[0][countries][]={$geo}";
      }
  }
//  if ($offer['capType'] == "daily_cap"){
//      $data .= "&daily_cap={$offer['cap']}";
//  }
if((isset($offer['cap']) && $offer['cap'] != 0 ) || isset($offer['budget']) && $offer['budget'] > 0){

 if(isset($offer['cap']) && $offer['cap'] != 0 ){
    $newCap[] = [
      "period" => "day",
      "type" => "conversions",
      "goal_type" => "all",
      "affiliates" => [],
      "affiliate_type" => "all",
      "value" => $offer['cap'],
      "goals" => []
    ];
 }
 elseif(isset($offer['budget']) && $offer['budget'] > 0){
      $newCap[] = [
        "period" => "day",
        "type" => "budget",
        "goal_type" => "all",
        "affiliates" => [],
        "affiliate_type" => "all",
        "value" => $offer['budget'],
        "goals" => []
      ];
    }
   
    $caps = array("caps" => $newCap);
 
    $caps = urldecode(http_build_query($caps));
    $data .= "&" . $caps;
 
}


  $kpi = urlencode($offer['kpi']);
  $data .= "&kpi[en]={$kpi}";
  if(isset($offer['is_redirect_overcap'])) $data .= "&is_redirect_overcap=1";    
if(isset($offer['categories']) && !empty($offer['categories']) && strlen($offer['categories'] > 0)){
  $categories = explode(",",$offer['categories']);
        foreach ($categories as $_category){
            $category = urlencode($_category);
            $data .= "&categories[]={$category}";
        }
}

      if (isset($offer['subAccounts']) && strlen($offer['subAccounts']) > 0 && $offer['subAccountsExcept'] == 1){
          $data .= "&sub_account_1={$offer['subAccounts']}&sub_account_1_except={$offer['subAccountsExcept']}" ;
       }

 return $data;


        }
        
   public function CreateMobAffiseOfferData($offer,$update = false){
  $title =  urlencode($offer['title']);
  $description = urlencode($offer['description']);
  $trackingLink = isset($offer['macros']) ? urlencode($offer['trackingLink'] . $offer['macros']) : urlencode($offer['trackingLink'] );
  $previewLink = urlencode($offer['previewLink']);
  $strictlyOs = strtolower($offer['os']);
  $external_offer_id = $offer['offerId'];
  $status = "active";

  //print_r(urldecode($trackingLink));die;
    if(!$update ){
      if(strlen($previewLink ) > 0 )
            $data = "title={$title}&advertiser={$offer['affiseAdvertiserId']}&url={$trackingLink}&url_preview={$previewLink}&description_lang[en]={$description}&status={$status}&privacy={$offer['privacy']}&strictly_country=1&redirect_type=http302hidden&external_offer_id={$external_offer_id}&uniqIpOnly=1&rejectNotUniqIp=1&click_session=3d&minimal_click_session=15s";
      else
            $data = "title={$title}&advertiser={$offer['affiseAdvertiserId']}&url={$trackingLink}&description_lang[en]={$description}&status={$status}&privacy={$offer['privacy']}&strictly_country=1&redirect_type=http302hidden&external_offer_id={$external_offer_id}&uniqIpOnly=1&rejectNotUniqIp=1&click_session=3d&minimal_click_session=15s";
    } else {
      $data = "status={$status}&uniqIpOnly=1&rejectNotUniqIp=1&is_redirect_overcap=1";//&url={$trackingLink}&url_preview={$previewLink}&status={$status}&strictly_os[os][0]={$strictlyOs}&advertiser={$offer['affiseAdvertiserId']}";
    }


  $geos = explode(",", $offer['geos']);
 //print_r($geos)  ;
  foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
     
      if ( strlen($geo) > 1) 
          {$data .= "&countries[]={$geo}"; }
      
  }

  
  if ($offer['bizModel'] == 'CPI'){
      $data .= "&is_cpi=1";
  }
  else{
      $data .= "&is_cpi=0";
  }


  $revenue = $offer['payout'] * 0.7;
  switch ($offer['actionRequire']){

      case "Install + Open":
          $goal = 1;
          break;
      deafult:
          $goal = 1;
          break;
  }
  $goal = 1;
  if(isset($offer['os']) && strlen($offer['os']) > 0)
      $data .= "&payments[0][os][]={$offer['os']}&payments[0][goal]={$goal}&payments[0][total]={$offer['payout']}&payments[0][revenue]={$revenue}&payments[0][currency]={$offer['currency']}&payments[0][type]={$offer['payoutType']}&payments[0][devices][]=mobile&payments[0][devices][]=tablet";
  else 
      $data .= "&payments[0][goal]={$goal}&payments[0][total]={$offer['payout']}&payments[0][revenue]={$revenue}&payments[0][currency]={$offer['currency']}&payments[0][type]={$offer['payoutType']}&payments[0][devices][]=mobile&payments[0][devices][]=tablet";
  foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
      if ( strlen($geo) > 1){
      $geo = strtoupper($geo);
      $data .= "&payments[0][countries][]={$geo}";
      }
  }
if((isset($offer['cap']) && $offer['cap'] != 0 ) || isset($offer['budget']) && $offer['budget'] > 0){

 if(isset($offer['cap']) && $offer['cap'] != 0 ){
    $newCap[] = [
      "period" => "day",
      "type" => "conversions",
      "goal_type" => "all",
      "affiliates" => [],
      "affiliate_type" => "all",
      "value" => $offer['cap'],
      "goals" => []
    ];
 }
 elseif(isset($offer['budget']) && $offer['budget'] > 0){
      $newCap[] = [
        "period" => "day",
        "type" => "budget",
        "goal_type" => "all",
        "affiliates" => [],
        "affiliate_type" => "all",
        "value" => $offer['budget'],
        "goals" => []
      ];
    }
   
    $caps = array("caps" => $newCap);
 
    $caps = urldecode(http_build_query($caps));
    $data .= "&" . $caps;
 
}



  $kpi = urlencode($offer['kpi']);
  $data .= "&kpi[en]={$kpi}";

if(isset($offer['categories']) && !empty($offer['categories']) && strlen($offer['categories'] > 0)){
  $categories = explode(",",$offer['categories']);
        foreach ($categories as $_category){
            $category = urlencode($_category);
            $data .= "&categories[]={$category}";
        }
}

      if (isset($offer['subAccounts']) && strlen($offer['subAccounts']) > 0 && $offer['subAccountsExcept'] == 1){
          $data .= "&sub_account_1={$offer['subAccounts']}&sub_account_1_except={$offer['subAccountsExcept']}" ;
       }

 return $data;


        }
            
        
  public function CreateDmiAffiseOfferData($offer,$update = false){
  $title =  urlencode($offer['title']);
  $description = urlencode($offer['description']);
  $trackingLink = isset($offer['macros']) ? urlencode($offer['trackingLink'] . $offer['macros']) : urlencode($offer['trackingLink'] );
  $previewLink = urlencode($offer['previewLink']);
  $strictlyOs = strtolower($offer['os']);
  $external_offer_id = $offer['offerId'];
  $status = "active";

  //print_r(urldecode($trackingLink));die;
    if(!$update ){
      $data = "title={$title}&advertiser={$offer['affiseAdvertiserId']}&url={$trackingLink}&url_preview={$previewLink}&description_lang[en]={$description}&status={$status}&privacy={$offer['privacy']}&strictly_country=1&redirect_type=http302hidden&external_offer_id={$external_offer_id}&uniqIpOnly=1&rejectNotUniqIp=1&click_session=1d&minimal_click_session=15s";
    } else {
      $data = "status={$status}&uniqIpOnly=1&rejectNotUniqIp=1&is_redirect_overcap=1";//&url={$trackingLink}&url_preview={$previewLink}&status={$status}&strictly_os[os][0]={$strictlyOs}&advertiser={$offer['affiseAdvertiserId']}";
    }


      
  $geos = explode(",", $offer['geos']);
 //print_r($geos)  ;
  foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
     
      if ( strlen($geo) > 1) 
          {$data .= "&countries[]={$geo}"; }
      
  }

  
  if ($offer['bizModel'] == 'CPI'){
      $data .= "&is_cpi=1";
  }
  else{
      $data .= "&is_cpi=0";
  }


  $revenue = $offer['payout'] * 0.7;
  switch ($offer['actionRequire']){

      case "Install + Open":
          $goal = 1;
          break;
      deafult:
          $goal = 1;
          break;
  }
  $goal = 1;
  if(isset($offer['os']) && strlen($offer['os']) > 0)
      $data .= "&payments[0][os][]={$offer['os']}&payments[0][goal]={$goal}&payments[0][total]={$offer['payout']}&payments[0][revenue]={$revenue}&payments[0][currency]={$offer['currency']}&payments[0][type]={$offer['payoutType']}&payments[0][devices][]=mobile&payments[0][devices][]=tablet";
  else 
      $data .= "&payments[0][goal]={$goal}&payments[0][total]={$offer['payout']}&payments[0][revenue]={$revenue}&payments[0][currency]={$offer['currency']}&payments[0][type]={$offer['payoutType']}&payments[0][devices][]=mobile&payments[0][devices][]=tablet";
  foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
      if ( strlen($geo) > 1){
      $geo = strtoupper($geo);
      $data .= "&payments[0][countries][]={$geo}";
      }
  }
if((isset($offer['cap']) && $offer['cap'] != 0 ) || isset($offer['budget']) && $offer['budget'] > 0){

 if(isset($offer['cap']) && $offer['cap'] != 0 ){
    $newCap[] = [
      "period" => "day",
      "type" => "conversions",
      "goal_type" => "all",
      "affiliates" => [],
      "affiliate_type" => "all",
      "value" => $offer['cap'],
      "goals" => []
    ];
 }
 elseif(isset($offer['budget']) && $offer['budget'] > 0){
      $newCap[] = [
        "period" => "day",
        "type" => "budget",
        "goal_type" => "all",
        "affiliates" => [],
        "affiliate_type" => "all",
        "value" => $offer['budget'],
        "goals" => []
      ];
    }
   
    $caps = array("caps" => $newCap);
 
    $caps = urldecode(http_build_query($caps));
    $data .= "&" . $caps;
 
}

if(isset($offer['is_redirect_overcap'])) $data .= "&is_redirect_overcap=1";
  $kpi = urlencode($offer['kpi']);
  $data .= "&kpi[en]={$kpi}";
if(isset($offer['is_redirect_overcap'])) $data .= "&is_redirect_overcap=1";
if(isset($offer['categories']) && !empty($offer['categories']) && strlen($offer['categories'] > 0)){
  $categories = explode(",",$offer['categories']);
        foreach ($categories as $_category){
            $category = urlencode($_category);
            $data .= "&categories[]={$category}";
        }
}

      if (isset($offer['subAccounts']) && strlen($offer['subAccounts']) > 0 && $offer['subAccountsExcept'] == 1){
          $data .= "&sub_account_1={$offer['subAccounts']}&sub_account_1_except={$offer['subAccountsExcept']}" ;
       }

 return $data;


        }
                 
public function sendAlert($data){

    $url = 'https://hooks.slack.com/services/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';
    $curl = \Fuel\Core\Request::forge($url,'curl');
    
 $curl->set_method('post'); 
 $curl->set_params(array('form-data' => $data));
 $curl->set_header('Content-Type', 'application/json');
 $curlResult =$curl->execute();
  $curlRes=json_decode($curlResult->response()->body);    
        return $curlRes;
 
  
}        
        
public function checkPaymentChanges($affiseOffer, $cap, $payout, &$arrChanged) {
   
 
       if ($affiseOffer->payments[0]->total !=$payout || 
            (count($affiseOffer->caps) > 0 &&  ($affiseOffer->caps[0]->affiliate_type == 'all' && $affiseOffer->caps[0]->value !=$cap && $cap > 0)) || 
            (count($affiseOffer->caps) > 0 &&  ($affiseOffer->caps[0]->affiliate_type != 'all' && $cap > 0))){
//   echo $affiseOffer->payments[0]->total." ".$payout."\n ";
//   echo $affiseOffer->caps[0]->affiliate_type."\n ";
//   echo $affiseOffer->caps[0]->value."  ".$cap."\n ";
            $arrChanged[] = $affiseOffer->id;
            }
} 
public function saveChanges($mode,$arrChanged){
 
    foreach($arrChanged as $offerId){
 
        \Model\Affisechanges::add($mode, $offerId);
    }
}
           
}