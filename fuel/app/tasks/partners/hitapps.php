<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class hitapps  extends Shared {
    //put your code here
    public $_url = "https://api-hitapps.affise.com/3.0/partner/offers?api-key=a8364bd3f7112517264c7ba35c179b3b38b9e2f1&limit=500";

    public $_affiseadvertiserId = '5cf52bab668f0f49008b46ea';
    
    public $_macros = '&sub1={clickid}&sub2={pid}_{sub3}&sub3={sub4}&sub4={sub5}';

     public function __construct(){
        $this->_affise = new \Platforms\Affise();
        //$this->_categories = $this->_getAffiseCategories();
        
    } 
    
     public function _CreateAffiseOfferData($offer,$update = false){
  $title =  urlencode($offer['title']);
  $description = urlencode($offer['description']);
  $trackingLink = isset($offer['macros']) ? urlencode($offer['trackingLink'] . $offer['macros']) : urlencode($offer['trackingLink'] );
  $previewLink = urlencode($offer['previewLink']);
//  $strictlyOs = strtolower($offer['os']);
  $external_offer_id = $offer['offerId'];
  $status = "active";

  //print_r(urldecode($trackingLink));die;
    if(!$update ){
      $data = "title={$title}&advertiser={$offer['affiseAdvertiserId']}&url={$trackingLink}&url_preview={$previewLink}&description_lang[en]={$description}&status={$status}&privacy={$offer['privacy']}&strictly_country=1&sources[]=51f531f53b7d9b1e0382f6d9&sources[]=5adf1fd454528908407f0ac6&redirect_type=http302hidden&external_offer_id={$external_offer_id}&uniqIpOnly=1&rejectNotUniqIp=1&click_session=6h&minimal_click_session=15s";
    } else {
      $data = "status={$status}&uniqIpOnly=1&rejectNotUniqIp=1&is_redirect_overcap=1";//&url={$trackingLink}&url_preview={$previewLink}&status={$status}&strictly_os[os][0]={$strictlyOs}&advertiser={$offer['affiseAdvertiserId']}";
    }


      
  $geos = explode(",", $offer['geos']);
 //print_r($geos)  ;
  foreach ($geos as $geo){
      if(strtolower($geo) == 'uk') $geo = "GB";
     
      if ( strlen($geo) > 1) 
          {$data .= "&countries[]={$geo}"; }
      
  }

  
  if ($offer['bizModel'] == 'CPI'){
      $data .= "&is_cpi=1";
  }
  else{
      $data .= "&is_cpi=0";
  }


  //$revenue = $offer['payout'] * 0.7;
  switch ($offer['actionRequire']){

      case "Install + Open":
          $goal = 1;
          break;
      deafult:
          $goal = 1;
          break;
  }
  $goal = 1;
  $i = 0 ;
  //print_r($offer['payments']);
  foreach($offer['payments'] as $payment) {
      //if( $payment['goal'] == 1) continue;
      if (count($payment['countries']) == 0) continue;
      $curOs1 = $payment['os'];
      if(count($curOs1) > 0) $curOs = implode(",",$curOs1 ); else $curOs='';
      //echo ($curOs).PHP_EOL;
      $curGoal = $payment['goal'];
      $curTotal = $payment['revenue'] ;
      $curRevenue = $payment['revenue'] * 0.7;
      $curCurrency = $payment['currency'];
      $curPayoutType = $payment['type'];
      
      if(strlen($curOs) > 0)
          $data .= "&payments[{$i}][os][]={$curOs}&payments[{$i}][goal]={$curGoal}&payments[{$i}][total]={$curTotal}&payments[{$i}][revenue]={$curRevenue}&payments[{$i}][currency]={$curCurrency}&payments[{$i}][type]={$curPayoutType}&payments[{$i}][devices][]=mobile&payments[{$i}][devices][]=tablet";
      else 
          $data .= "&payments[{$i}][goal]={$curGoal}&payments[{$i}][total]={$curTotal}&payments[{$i}][revenue]={$curRevenue}&payments[{$i}][currency]={$curCurrency}&payments[{$i}][type]={$curPayoutType}&payments[{$i}][devices][]=mobile&payments[{$i}][devices][]=tablet";
      foreach ($payment['countries'] as $geo){
          if(strtolower($geo) == 'uk') $geo = "GB";
          if ( strlen($geo) > 1){
          $geo = strtoupper($geo);
          $data .= "&payments[{$i}][countries][]={$geo}";
          }
      }
//      if ($offer['capType'] == "daily_cap"){
//          $data .= "&daily_cap={$offer['cap']}";
//      }
      $i++;
  } 
if((isset($offer['cap']) && $offer['cap'] != 0 ) || isset($offer['budget']) && $offer['budget'] > 0){

 if(isset($offer['cap']) && $offer['cap'] != 0 ){
    $newCap[] = [
      "period" => "day",
      "type" => "conversions",
      "goal_type" => "all",
      "affiliates" => [],
      "affiliate_type" => "all",
      "value" => $offer['cap'],
      "goals" => []
    ];
 }
 elseif(isset($offer['budget']) && $offer['budget'] > 0){
      $newCap[] = [
        "period" => "day",
        "type" => "budget",
        "goal_type" => "all",
        "affiliates" => [],
        "affiliate_type" => "all",
        "value" => $offer['budget'],
        "goals" => []
      ];
    }
   
    $caps = array("caps" => $newCap);
 
    $caps = urldecode(http_build_query($caps));
    $data .= "&" . $caps;
 
}



  $kpi = urlencode($offer['kpi']);
  $data .= "&kpi[en]={$kpi}";
  if(isset($offer['is_redirect_overcap'])) $data .= "&is_redirect_overcap=1";    
if(isset($offer['categories']) && !empty($offer['categories']) && strlen($offer['categories'] > 0)){
  $categories = explode(",",$offer['categories']);
        foreach ($categories as $_category){
            $category = urlencode($_category);
            $data .= "&categories[]={$category}";
        }
}

      if (isset($offer['subAccounts']) && strlen($offer['subAccounts']) > 0 && $offer['subAccountsExcept'] == 1){
          $data .= "&sub_account_1={$offer['subAccounts']}&sub_account_1_except={$offer['subAccountsExcept']}" ;
       }

 return $data;


        }
        
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

        return $offer->title;
    }
    public function getOfferDescription($offer){

        return $offer->description_lang->en;
    }  
    public function getOfferGeos($offer){ 
       $geos = array();
        if (isset($offer->countries)){
            foreach ($offer->countries as $_country){
                    array_push($geos, $_country);
                }
        }
           return implode(",", $geos);
    }

    public function getOfferOs($offer){
        
        if (isset($offer->strictly_os) && isset($offer->strictly_os->items) && count($offer->strictly_os->items) > 0){
            $_os = array();
           foreach ($offer->strictly_os->items as $os => $value){
                    array_push($_os, $os);
                } 
               
            return implode(",", $_os);
        }
       if(strstr($offer->preview_url,"play.google.com") != FALSE)  $platform = "Android"; 
       else   $platform = "iOS";
       return $platform;
    }



   public function getOfferBizModel($offer){/////////////////////////////////////////////////////////////////
       
        if ($offer->is_cpi == TRUE) return "CPI";
        return "CPA";
    }

    public function getOfferCapType($offer){

            $capType = "daily_cap";
        if (isset($offer->caps[0]->period))   { 
            switch ($offer->caps[0]->period){

               case "month":
                    $capType = "monthly_cap";
                    break;
                case "week":
                    $capType = "weekly_cap";
                    break;
                case "day":
                default:
                    $capType = "daily_cap";
                    break;

            }
        }

        return $capType;
 
        }

      
    

    public function getOfferCap($offer){
    
        if (isset($offer->caps[0]->value) && $offer->caps[0]->value != ""){

            return $offer->caps[0]->value;
        }

        return 50;

       }

    public function getOfferKpi($offer){
     return $offer->kpi->en;
    }

    public function getOfferMinOsVersion($offer){
       
             if(strstr($offer->preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";
            
     }
    
     public function getOfferPreviewLink($offer){

         return $offer->preview_url;
    }

    public function getOfferPayments($offer){

      //  $payoutType = "fixed";
        
        return $offer->payments;
    }

//    public function getOfferPayout($offer){
//
//         return substr($offer->payout_value, 1);
//    }
    
//    public function getOfferCurrency($offer){
//        
//            return "USD";
//    }
      
    
    public function getOfferTrackingLink($offer){

         return $offer->link;
    }



    public function getOfferCategories($offer){

        
      
        $categories = array();
//        foreach ($offer->categories as $tag){
//            $neededObject = current(array_filter( $this->_categories, function ($e)  use ($tag) { return $e->title == $tag;    }));  
//            if(isset($neededObject) && is_object($neededObject)){
//               $categories[] = $neededObject->id; 
//            }
//            else $categories[] = "5a3b86ac375b1239000000b6";
//            
//        }
          return implode(",", $categories);
    }
    
     public function getOfferLogo($offer){

        return $offer->logo;
    }
    
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    



     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
//            foreach($offer->black_list_sources as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
        
        return "";
    }

    
    public function getOffers(){
     $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;
  
         $loop = true;
         $page = 1;
      while ($loop){
            $this->_url = "{$this->_url}&page={$page}";
            
           
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');

         $curl->set_method("get");
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
        
        if (isset($offersResult->pagination->next_link)){
                $page++;
            }
            else{
                $loop = false;
            }   

        if (!isset($offersResult->offers) || (count($offersResult->offers) == 0 ) ) break;
        if (isset($offersResult->offers) && (count($offersResult->offers) > 0 ) ){

            foreach ($offersResult->offers as $_offer){
                 $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                   
                    //$offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer);  
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payments = $this->getOfferPayments($_offer);
                    //$offer->payout = $this->getOfferPayout($_offer);
                    //$offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = 'active';
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_bundle->targeting);
                    }
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
      }
      // print_r($this->_offers); 
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
         $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);           
   $arrChanged = array();
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
//                        var_dump($this->_offers[$externalOfferId]->payments); 
                        $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, round($this->_offers[$externalOfferId]->payments[0]->revenue / 0.7,4) , $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->_CreateAffiseOfferData($arr,true);
                                   
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){ 
                              echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                              $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payments[0]->revenue, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->_CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);

                               }
                        //edit
                         }
                         else {
                             echo "create new offer".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->_CreateAffiseOfferData($arr);
//                                   echo $data.PHP_EOL; 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;
                default :
                     echo "offer inactive  - should be stopped ".$externalOfferId.PHP_EOL;
                  //search in active -if exists =>stop it
                  $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));  
        
                if (isset($neededObject) && is_object($neededObject)) {
                    $arr=json_decode(json_encode($neededObject), True);
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);
                                      
                }
                    break;
             
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);                 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
 
        
}

