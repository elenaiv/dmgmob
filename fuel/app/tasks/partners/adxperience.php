<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class adxperience  extends Shared {
    //put your code here
    public $_url = "https://admin.adxperience.com/api/offers/assigned?api_key=ccefb1c4cca44a1b971661377534981a59f7295b02ed2";
 
    
    public $_affiseadvertiserId = '59f832ee668f0fb6118b4600';
    
    public $_macros = '&pub_sub={clickid}&sub_pub_id={pid}&placement_id={sub3}&ios_ifa={sub4}&google_id={sub4}&placement_name={sub5}';
    

    
     public function __construct(){
        $this->_affise = new \Platforms\Affise();
//        $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

       return $offer->name;
    }
    public function getOfferDescription($offer){
        if(isset($offer->description) && strlen($offer->description) > 0 )    return $offer->description;
        return $offer->name;
    }  
    public function getOfferGeos($offer){ 
        $geos = array();
        if (isset($offer->countries)){
            $geos = $offer->countries;
        }

        return implode(",", $geos);
    }

    public function getOfferOs($offer){
        
         if (strtolower($offer->os[0]->name) == 'ios'){
            return "iOS";
        }
        return "Android";

      
    }


   public function getOfferBizModel($offer){

      return strtoupper($offer->payout_type);
    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      if (isset($offer->daily_cap) && $offer->daily_cap != ""){

            return $offer->daily_cap;
        }

        return 50;
    }

    public function getOfferKpi($offer){
      return "{$offer->kpi} {$offer->restrictions}";
    }

    public function getOfferMinOsVersion($offer){

                  if (isset($offer->os[0]) ){
                      $minOs = substr($offer->os[0]->version,2);
                  }
                  else return 0;
                      
    }
    
     public function getOfferPreviewLink($offer){

         return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
       
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->payout_amount;
    }
    
    public function getOfferCurrency($offer){
       return $offer->payout_cur;
    }
    
    public function getOfferTrackingLink($offer){
       $trackingUrl = substr($offer->tracking_url, 0, strpos($offer->tracking_url, "&pub_sub"));
        return $trackingUrl;
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){

        if (isset($offer->icon)){

            return $offer->icon;
        }

        return "";
    }
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");$allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

    
    public function getOffers(){
        $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;
         
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
        if (isset($offersResult->response) && isset($offersResult->response->data) && isset($offersResult->response->data->offers) ){

            foreach ($offersResult->response->data->offers as $_offer){
                 $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = $_offer->status;
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                       
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);            
            $arrChanged = array();
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "running":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;

                default:
                  echo "offer suspend  should be paused ".$externalOfferId.PHP_EOL;
                   $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return  $e->external_offer_id == $externalOfferId;    }));  
                   if (isset($neededObject) && is_object($neededObject)) {
                   $arr=json_decode(json_encode($neededObject), True);
                  // print_r($arr);
                   $data ="status=suspended";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);

                   }
                    break;
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
