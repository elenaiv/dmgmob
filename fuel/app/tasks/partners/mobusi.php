<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobusi
 *
 * @author lenaivch
 */
namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class mobusi  extends Shared {
    //put your code here
    public $_url = "http://api.leadzu.com/offer.find";
    public $_body = array(
            "user_id" => 4325,
            "api_key" => "fa8392b288060fbd66822502352e7f5e",
            "adult" => false,
            "approved" => true
        ); 
//    private $_offerIds = array();
    public $_affiseadvertiserId = '59c8e6e8668f0f97298b45ac';
    
    public $_macros = '';


     public function __construct(){
        $this->_affise = new \Platforms\Affise();
//        $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

        return $offer->title;
    }
    public function getOfferDescription($offer){

        return $offer->description;
    }  
    public function getOfferGeos($offer){ 

        $geos = array();
       if (isset($offer->countries)){
         foreach ($offer->countries as $country => $info){
//            echo $country;
            array_push($geos, $country);
        }

        return implode(",", $geos);
        }
        
        return implode(",", $geos);
    }

    public function getOfferOs($offer){
        if(strpos($offer->preview,'itunes') > 0){// - Optional
           $osName = "iOS";
    } else {
           $osName = "Android";
    }
       return $osName;
    }


   public function getOfferBizModel($offer){

        $bizModel = '';
        foreach ($offer->countries as $info){

            $bizModel = $info->payoutType;
            break;
        }

        return $bizModel;
    }

    public function getOfferCapType($offer){

        $capType = '';
        foreach ($offer->countries as $info){

            if ($info->capping === true){
                $capType = "daily_cap";
            }
            break;
        }

        return $capType;
    }

    public function getOfferCap($offer){
    
        $cap = 0;
        foreach ($offer->countries as $info){

            if ($info->capping === true){
                if ($info->capValue == "ask_for_capping"){
                    $cap = 100;
                }
                else{
                    $cap = $info->capValue->daily;
                }
            }
        }

        return $cap;
    }

    public function getOfferKpi($offer){
    $kpi = array();
       if(isset($offer->os_version->kpi)) $kpi = array(
            $offer->os_version->kpi
        );
        foreach ($offer->restrictions as $restriction){
            
            array_push($kpi, $restriction);
        }

        return implode(",", $kpi);
    }

    public function getOfferMinOsVersion($offer){

        $minOs = array();
        foreach ($offer->os_version as $os){

            if (isset($os->ge)){
                array_push($minOs, $os->ge);
            }
            else{
                  if(strpos($offer->preview,'itunes') > 0){
                           array_push( $minOs , "4.4");
                    } else {
                          array_push( $minOs ,"8");
                    }
            }
        }

        return implode(",", $minOs);
    }
    
     public function getOfferPreviewLink($offer){

        return $offer->preview;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        foreach ($offer->countries as $info){

            if ($info->payout == "REV/SHARE"){

                $payoutType = "percent";
            }
        }

        return $payoutType;
    }

    public function getOfferPayout($offer){

        $payout = 0;
        foreach ($offer->countries as $info){

            $payout = $info->payout;
            if ($info->payout == "REV/SHARE"){
                $payout = 100;
            }
            break;
        }

        return $payout;
    }
    
    public function getOfferCurrency($offer){

        $currency = "EUR";
        foreach ($offer->countries as $info){

            $currency = $info->payoutCurrency;
            break;
        }

        return $currency;
    }
    
    public function getOfferTrackingLink($offer){

        $trackingLink = "";
        foreach ($offer->countries as $info){

            $trackingLink = $info->url;
            break;
        }

        return $trackingLink;
    }


    public function getOfferCategories($offer){

        $categories = array();
//        foreach ($offer->tags as $tag){
////            $title = $tag->title;
//            $neededObject = current(array_filter( $this->_categories, function ($e)  use ($tag) { return $e->title == $tag;    }));  
//            if(isset($neededObject) && is_object($neededObject)){
//               $categories[] = $neededObject->id; 
//            }
//            else $categories[] = "5a3b86ac375b1239000000b6";
//            
//        }
          return implode(",", $categories);
    }
    
     public function getOfferLogo($offer){

      if (isset($offer->icon)){

            return $offer->icon;
        }

        return "";
    }
     public function getOfferActionRequire($offer){ //KPMBRO

        if (isset($offer->action_require)){
            return $offer->action_require;
        }

        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         $blacklisted_sources =array();
//        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
//            foreach($offer->black_list_sources as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//              //  $aff = substr($value,0,$pos -1);
//                $blacklisted_sources[] = $sources;
//           }
////            print_r($blacklisted_sources); exit(0);
//        }
       
        return implode(",", $blacklisted_sources);
    }

     public function getBlacklistedAffiliates($offer){
         $affiliates =array();
//        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
//            foreach($offer->black_list_sources as $value)   {
//                $pos=stripos("$value","_");
//               // $sources= substr($value,$pos + 1);
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//            //print_r($affiliates); exit(0);
//        }
       
        return implode(",", $affiliates);
    }

    
    public function getOffers(){
        
        $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;             
       
        $_options = array(
            CURLOPT_COOKIE => 1,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
            CURLOPT_RETURNTRANSFER => 1,
            CURLOPT_CUSTOMREQUEST => "post",
            CURLOPT_URL => $this->_url,
            CURLOPT_POSTFIELDS => json_encode($this->_body),
            CURLOPT_VERBOSE => false);
        
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
 
	 $curl->set_options($_options);
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
       

        if (isset($offersResult->answer) && (count($offersResult->answer) > 0) ){
             $arrChanged = array();
            foreach ($offersResult->answer as $_offer){
                 $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;  
                 
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = $_offer->status;
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                       
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId); 
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                        $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                             $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);                           
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;
                case "inactive":
                     echo "offer inactive  - should be stopped ".$externalOfferId.PHP_EOL;
                  //search in active -if exists =>stop it
                  $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));  
        
                if (isset($neededObject) && is_object($neededObject)) {
                    $arr=json_decode(json_encode($neededObject), True);
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);
                   // print_r($tmpArr);
                //edit
                        
                }
                    break;
                case "overcap":
                  echo "offer overcap  should be paused ".$externalOfferId.PHP_EOL;
                   $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return  $e->external_offer_id == $externalOfferId;    }));  
                   if (isset($neededObject) && is_object($neededObject)) {
                   $arr=json_decode(json_encode($neededObject), True);
                  // print_r($arr);
                   $data ="status=suspended";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);

                   }
                    break;
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                                 print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged); 
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}

