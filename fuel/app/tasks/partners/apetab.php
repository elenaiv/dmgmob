<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class apetab  extends Shared {
    //put your code here
    public $_url = "http://app.apetab.com/api/v2?token=1358dd1c3167e87dbf05805c2478024dbfd68415585164471917";
 
    
    public $_affiseadvertiserId = '5cebd3ee668f0f4a008b4a45'; 
    
    public $_macros = '&clickid={clickid}&subchannelid={pid}&appbundle={sub5}&d1={sub1}';
    

    
     public function __construct(){
        
        $this->_affise = new \Platforms\Affise();
//        $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

       if(isset($offer->campaign_name) && strlen($offer->campaign_name) > 0) return $offer->campaign_name;
       return $offer->app_data->app_name;
    }
    public function getOfferDescription($offer){
        if(isset($offer->description) && $offer->description != null && strlen($offer->description) > 0)  return $offer->description;      
        if(isset($offer->campaign_name) && strlen($offer->campaign_name) > 0) return $offer->campaign_name;
        return $offer->app_data->app_name;
    }  
    public function getOfferGeos($offer){ 
       $geos = array();
       if (isset($offer->location_targeting->targeted_geos) && !empty($offer->location_targeting->targeted_geos)){
                foreach ($offer->location_targeting->targeted_geos as $country){
                  array_push($geos, $country);
            }
  
        }
         if (empty($geos)) return "";
         return implode(",", $geos);
    }

    public function getOfferOs($offer){
        
         if (strtolower($offer->platform) == 'ios'){
            return "iOS";
        }
        return "Android";

      
    }


   public function getOfferBizModel($offer){

            return strtoupper($offer->payout_type);

    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      return isset($offer->budget_capping->capping_type) ? $offer->budget_capping->capping_type : 0;
    }

    public function getOfferKpi($offer){
     if(isset($offer->description) && $offer->description != null && strlen($offer->description) > 0)  return $offer->description;      
        if(isset($offer->campaign_name) && strlen($offer->campaign_name) > 0) return $offer->campaign_name;
        return $offer->app_data->app_name;
        
    }

    public function getOfferMinOsVersion($offer){

                  if (strtolower($offer->platform) == "ios") $minOs = "4.4"  ;
                  else $minOs = "8" ;
  
        return  $minOs;
    }
    
     public function getOfferPreviewLink($offer){

        return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
       
        return $payoutType;
    }

    public function getOfferPayout($offer){
         return $offer->payout;
    }
    
    public function getOfferCurrency($offer){
      return 'USD';
    }
    
    public function getOfferTrackingLink($offer){
       if (isset($offer->tracking_url) )  return $offer->tracking_url;
        else return "";
    }


    public function getOfferCategories($offer){

                   return "";
    }
    
     public function getOfferLogo($offer){

         return "";
    }
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         return "";
//         $blacklisted_sources =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $sources= substr($value,$pos + 1);
//                $blacklisted_sources[] = $sources;
//           }
//        }
//       
//        $tmp = implode(",", $blacklisted_sources);
//        $tmpstr = str_replace("?", "", $tmp);
//        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         return "";
//         $affiliates =array();
//        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
//            foreach($offer->blacklisted_subpublishers as $value)   {
//                $pos=stripos("$value","_");
//                $aff = substr($value,0,$pos);
//                $affiliates[] = $aff;
//           }
//         }
//       
//        return implode(",", $affiliates);
    }

    
    public function getOffers(){
            $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;        
          $_options = array(
            CURLOPT_COOKIE => 1,
            CURLOPT_USERAGENT => "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/33.0.1750",
            CURLOPT_RETURNTRANSFER => 1,
            );       
         
          $curl = \Fuel\Core\Request::forge($this->_url,'curl');
          $curl->set_method('get')  ;
          $curl->set_options($_options);  
          $curl->set_header('Content-Type', 'text/html');
          $curlResult = $curl->execute();
        
         $offersResult=json_decode($curlResult->response()->body); 
   
        if (isset($offersResult->campaigns) && (count($offersResult->campaigns) > 0) ){
//        $arrChanged = array();
            foreach ($offersResult->campaigns as $_offer){
                 $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;   
                 if(strstr($_offer->preview_url, 'http://') == FALSE 
                        && strstr($_offer->preview_url,'https://') ==FALSE 
                        && strstr($_offer->preview_url, 'itms-apps://') == FALSE 
                        && strstr($_offer->preview_url,'market://') == FALSE 
                        && strstr($_offer->preview_url, 'links\\n')  == FALSE ) continue;
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = 'live';
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
      //  print_r($this->_offers); 

        if(count($this->_offerIds) > 0 ) {
                    $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
                    $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
             $arrChanged = array();
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "live":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                        $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);   
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo "active offers count: ".count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                            $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);   
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      
                                  }
                               }
                         }
                         else {
                              echo "create new offer ".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   //print_r($data); 
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
                 break;

                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                   print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);                      
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}
