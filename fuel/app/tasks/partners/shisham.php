<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobusi
 *
 * @author lenaivch
 */
namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class shisham  extends Shared {
    //put your code here
    public $_url = "https://api.shishamdigital.com/affiliate/v1/offers";

    public $_affiseadvertiserId = '5d5a55f5668f0fae008b493c';//??????????????????????????
    
    public $_macros = '&aff_sub1={clickid}&aff_sub2={pid}_{sub3}&ifa={sub4}&gid={sub4}&aff_sub3={sub5}';//???????????????????????????????????????????

     public function __construct(){
        $this->_affise = new \Platforms\Affise();
       // $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->offer_id;
    }
 
    public function getOfferName($offer){

        return $offer->offer_name;
    }
    public function getOfferDescription($offer){

        return $offer->description;
    }  
    public function getOfferGeos($offer){ 
       $geos = array();
        if (isset($offer->targeting->countries->values)){
            foreach ($offer->targeting->countries->values as $_rule){
                    array_push($geos, $_rule->iso_code2);
             }
        }

        return implode(",", $geos);
    
    }

    public function getOfferOs($offer){
       if(strstr($offer->preview_url,"play.google.com") != FALSE)  $platform = "Android"; 
       else   $platform = "iOS";
       return $platform;
    }



   public function getOfferBizModel($offer){
       
        return strtoupper($offer->payout_type);
    }

    public function getOfferCapType($offer){
         $capType = "daily_cap";
         if (isset($offer->isCapEnabled) && $offer->daily_convs != 0){

            return "daily_cap";
        }
        if (isset($offer->isCapEnabled) && $offer->monthly_convs != 0){

            return "monthly_cap";
        }

        return $capType;
 
        }

 
    public function getOfferCap($offer){
    
        if (isset($offer->isCapEnabled) && $offer->daily_convs != 0){

            return $offer->daily_convs;
        }
        if (isset($offer->isCapEnabled) && $offer->monthly_convs != 0){

            return $offer->monthly_convs;
        }
        return 0;

       }

    public function getOfferKpi($offer){
     return $offer->description;
    }

    public function getOfferMinOsVersion($offer){
       
             if(strstr($offer->preview_url,"play.google.com") != FALSE) return "4.4";
             else   return "8";
            
     }
    
     public function getOfferPreviewLink($offer){

         return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        
        return $payoutType;
    }

    public function getOfferPayout($offer){

         return $offer->payout;
    }
    
    public function getOfferCurrency($offer){
        
            return $offer->currency;
    }
      
    
    public function getOfferTrackingLink($offer){
        if (null != $offer->tracking_url)   {
           
           $arrmacro= explode("&", $offer->tracking_url);
//           print_r($arrmacro); 
 
           foreach($arrmacro as $key=>$m){
              
               if(strstr($m,"aff_sub1=") != FALSE || strstr($m,"aff_sub2=") != FALSE ){
                
                   unset($arrmacro[$key] );
               }
           }
//           print_r($arrmacro); 
//           echo implode("&", $arrmacro).PHP_EOL;exit(0);
          return implode("&", $arrmacro);
       }
        else return "";

    }



    public function getOfferCategories($offer){

    
          return "";
    }
    
     public function getOfferLogo($offer){

        return "";
    }
    
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    



     public function getBlacklistedSources($offer){
       
        return "";
    }

     public function getBlacklistedAffiliates($offer){
     return "";
    }

    
    public function getOffers(){
        
        
 
        $loop = true;
        $page = 1;
        $limit = 50;
      while ($loop){
            $this->_url = "{$this->_url}?limit={$limit}&page={$page}";
            
           
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
         $curl->set_header("Authorization" , "Bearer 1131aa3d3f373aee4509dce7cfc32d") ;  
         $curl->set_header("Content-Type" , "application/json") ; 
         $curl->set_method("get");
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
      if(!isset($offersResult->data)) {
          echo "Integration Data is not set\n"; break;
      }
        if ($offersResult->data->totalPages > $page){
                $page++;
            }
            else{
                $loop = false;
            }   
        if (!isset($offersResult->data->items) || (count($offersResult->data->items) == 0 ) ) break;
        if (isset($offersResult->data->items) && (count($offersResult->data->items) > 0 ) ){
                          $arrChanged = array();
            foreach ($offersResult->data->items as $_offer){
               if($_offer->offer_status != 2) continue;
                  $tmpof = $this->getOfferId($_offer);
                 
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                   
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer);  
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = "active";
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_bundle->targeting);
                    }
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
      }
      //  print_r($this->_offers); echo count($this->_offers);exit(0);
       
        if(count($this->_offerIds) > 0 ) {
                 $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
                 $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){ 
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                            $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);

                               }
                        //edit
                         }
                         else {
                             echo "create new offer".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;
                default :
                     echo "offer inactive  - should be stopped ".$externalOfferId.PHP_EOL;
                  //search in active -if exists =>stop it
                  $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));  
        
                if (isset($neededObject) && is_object($neededObject)) {
                    $arr=json_decode(json_encode($neededObject), True);
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);
                                      
                }
                    break;
             
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
       
  
}   
        
}

