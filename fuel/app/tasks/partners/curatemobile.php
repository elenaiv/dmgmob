<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of mobusi
 *
 * @author lenaivch
 */
namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class curateMobile  extends Shared {
    //put your code here
    public $_url = "https://api.curatemobile.com:8080/v1/api/approved_offers?p=1517";

    public $_affiseadvertiserId = '5b2917b8668f0f9fb58b50ba';
    
    public $_macros = '&sub={clickid}&sub2={sub6}&sub3={pid}_{sub3}&sub4={sub4}&sub5={sub5}';

     public function __construct(){
        $this->_affise = new \Platforms\Affise();
        $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->id;
    }
 
    public function getOfferName($offer){

        return $offer->name;
    }
    public function getOfferDescription($offer){

        return $offer->description;
    }  
    public function getOfferGeos($offer){ 
       $geos = array();
        if (isset($offer->targeting_rules)){
            foreach ($offer->targeting_rules as $_rule){

                if ($_rule->type == "Country" && $_rule->logic == "allow"){
                    array_push($geos, $_rule->value);
                }
            }
        }

        return implode(",", $geos);
    
    }

    public function getOfferOs($offer){
       if(strstr($offer->preview_url,"play.google.com") != FALSE)  $platform = "Android"; 
       else   $platform = "iOS";
       return $platform;
    }



   public function getOfferBizModel($offer){
       
        return strtoupper($offer->payout_type);
    }

    public function getOfferCapType($offer){

            $capType = "daily_cap";
        if (isset($offer->cap_type))   { 
            switch ($offer->cap_type){

               case "monthly":
                    $capType = "monthly_cap";
                    break;
                case "weekly":
                    $capType = "weekly_cap";
                    break;
                case "daily":
                default:
                    $capType = "daily_cap";
                    break;

            }
        }

        return $capType;
 
        }

      
    

    public function getOfferCap($offer){
    
        if (isset($offer->cap_value) && $offer->cap_value != ""){

            return $offer->cap_value;
        }

        return 50;

       }

    public function getOfferKpi($offer){
     return $offer->description;
    }

    public function getOfferMinOsVersion($offer){
       
             if(strstr($offer->preview_url,"play.google.com") != FALSE) return "8";
             else   return "4.4";
            
     }
    
     public function getOfferPreviewLink($offer){

         return $offer->preview_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
        
        return $payoutType;
    }

    public function getOfferPayout($offer){

         return substr($offer->payout_value, 1);
    }
    
    public function getOfferCurrency($offer){
        
            return "USD";
    }
      
    
    public function getOfferTrackingLink($offer){

         return $offer->tracking_url;
    }



    public function getOfferCategories($offer){

        $categories = array();
        foreach ($offer->categories as $tag){
            $neededObject = current(array_filter( $this->_categories, function ($e)  use ($tag) { return $e->title == $tag;    }));  
            if(isset($neededObject) && is_object($neededObject)){
               $categories[] = $neededObject->id; 
            }
            else $categories[] = "5a3b86ac375b1239000000b6";
            
        }
          return implode(",", $categories);
    }
    
     public function getOfferLogo($offer){

        return "";
    }
    
     public function getOfferActionRequire($offer){ //KPMBRO

        return "Install&Open";
    }
    



     public function getBlacklistedSources($offer){
         $blacklisted_sources =array();
        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
            foreach($offer->black_list_sources as $value)   {
                $pos=stripos("$value","_");
                $sources= substr($value,$pos + 1);
                $blacklisted_sources[] = $sources;
           }

        }
       
        $tmp = implode(",", $blacklisted_sources);
        $tmpstr = str_replace("?", "", $tmp);
        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         $affiliates =array();
        if(isset($offer->black_list_sources) && !empty($offer->black_list_sources)) {
            foreach($offer->black_list_sources as $value)   {
                $pos=stripos("$value","_");
                $aff = substr($value,0,$pos);
                $affiliates[] = $aff;
           }

        }
       
        return implode(",", $affiliates);
    }

    
    public function getOffers(){
        
        
           $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;    
         $_headers = array(
        "Authorization: Token token=b930e870391c2bd9c7d0707ce46a9242",
        "Content-Type: application/json"    
    ); 
         $loop = true;
        $page = 1;
      while ($loop){
            $this->_url = "{$this->_url}&page={$page}";
            
           
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
         $curl->set_header("Authorization" , "Token token=b930e870391c2bd9c7d0707ce46a9242") ;  
         $curl->set_header("Content-Type" , "application/json") ; 
	 //$curl->set_options($_options);
         $curl->set_method("get");
         $curlResult = $curl->execute();
         $offersResult=json_decode($curlResult->response()->body); 
        
        if (isset($offersResult->pagination->next_link)){
                $page++;
            }
            else{
                $loop = false;
            }   
if (!isset($offersResult->data->offers) || (count($offersResult->data->offers) == 0 ) ) break;
        if (isset($offersResult->data->offers) && (count($offersResult->data->offers) > 0 ) ){
                          $arrChanged = array();
            foreach ($offersResult->data->offers as $_offer){
                  $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;  
                 
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = $this->getOfferName($_offer);
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                   
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer);  
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = $_offer->status;
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_bundle->targeting);
                    }
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
      }
//        print_r($this->_offers); exit(0);
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
            $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
            $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);            
 
            foreach ($this->_offerIds as $externalOfferId => $status){
               
                switch ($status){
                case "active":
                    
                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){ 
                            echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                            $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);

                               }
                        //edit
                         }
                         else {
                             echo "create new offer".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
               
                
                break;
                default :
                     echo "offer inactive  - should be stopped ".$externalOfferId.PHP_EOL;
                  //search in active -if exists =>stop it
                  $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));  
        
                if (isset($neededObject) && is_object($neededObject)) {
                    $arr=json_decode(json_encode($neededObject), True);
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($arr['id'], $data);
                                      
                }
                    break;
             
                }
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
       
  
}   
        
}

