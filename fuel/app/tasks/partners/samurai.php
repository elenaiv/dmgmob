<?php


namespace Fuel\Tasks;

use \Fuel\Core\Request as Curl;
//require  APPPATH.'tasks'.DS.'partners'.DS.'shared.php';

class samurai  extends Shared {
    //put your code here
    public $_url = "http://api.appsamurai.com/api/campaign-pull/list/as9GmllBRxEpdMvt3b25GRIvNgaArxflN6M3bMoys";
 
    
    public $_affiseadvertiserId = '5aeac9da54528925493a3f76';
    
    public $_macros = '';

    
     public function __construct(){
        $this->_affise = new \Platforms\Affise();
      //  $this->_categories = $this->_getAffiseCategories();
        
    } 
    public function getOfferId($offer){

        return $offer->campaign_id;
    }
 
    public function getOfferName($offer){

       return $offer->app_title;
    }
    public function getOfferDescription($offer){

        return $offer->campaign_name;
    }  
    public function getOfferGeos($offer){ 
       $geos = array();
       if (isset($offer->countries) && !empty($offer->countries)){
                foreach ($offer->countries as $country){
                  array_push($geos, $country);
            }
        }
        
        return implode(",", $geos);
    }

    public function getOfferOs($offer){
        
        $os = "";
        if (strtolower($offer->platform) == "play"){
            $os = "Android";
        }
        else if (strtolower($offer->platform) == "ios"){
            $os = "iOS";
        }

       return  $os;
    }


   public function getOfferBizModel($offer){

       $bizModel = 'CPI';
       if(isset($offer->payout_type))   $bizModel = strtoupper ($offer->payout_type); 
         return $bizModel;
    }

    public function getOfferCapType($offer){

        $capType = 'daily_cap';

        return $capType;
    }

    public function getOfferCap($offer){
  
      $cap = $offer->daily_cap; 
      return $cap;
    }

    public function getOfferKpi($offer){
    $kpi = array();
       if(isset($offer->hardKpi)) $kpi[] = $offer->hardKpi;
       if(isset($offer->softKpi)) $kpi[] = $offer->softKpi;
 
        return implode(",", $kpi);
    }

    public function getOfferMinOsVersion($offer){

                  if (strtolower($offer->platform) == "ios") $minOs ="4.4" ;
                  else $minOs = "8" ;
  
        return  $minOs;
    }
    
     public function getOfferPreviewLink($offer){

        return $offer->app_store_url;
    }

    public function getOfferPayoutType($offer){

        $payoutType = "fixed";
       
        return $payoutType;
    }

    public function getOfferPayout($offer){
         $payout = $offer->cpi;
         return $payout;
    }
    
    public function getOfferCurrency($offer){
       $currency = "USD";
       return $currency;
    }
    
    public function getOfferTrackingLink($offer){
        $trackingLink = $offer->tracking_url;
        return $trackingLink;
    }


    public function getOfferCategories($offer){

        $categories = array();
        
            $tag = $offer->app_category;
            $neededObject = current(array_filter( $this->_categories, function ($e)  use ($tag) { return $e->title == $tag;    }));  
            if(isset($neededObject) && is_object($neededObject)){
               $categories[] = $neededObject->id; 
            }
            else $categories[] = "5a3b86ac375b1239000000b6";
            
        
          return implode(",", $categories);
    }
    
     public function getOfferLogo($offer){

        return "";
    }
     public function getOfferActionRequire($offer){ //KPMBRO

       

        return "Install&Open";
    }
    

     public function getBlacklistedSources($offer){
         $blacklisted_sources =array();
        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
            foreach($offer->blacklisted_subpublishers as $value)   {
                $pos=stripos("$value","_");
                $sources= substr($value,$pos + 1);
                $blacklisted_sources[] = $sources;
           }
        }
       
        $tmp = implode(",", $blacklisted_sources);
        $tmpstr = str_replace("?", "", $tmp);
        return str_replace(" ", "", $tmpstr);
    }

     public function getBlacklistedAffiliates($offer){
         $affiliates =array();
        if(isset($offer->blacklisted_subpublishers) && !empty($offer->blacklisted_subpublishers)) {
            foreach($offer->blacklisted_subpublishers as $value)   {
                $pos=stripos("$value","_");
                $aff = substr($value,0,$pos);
                $affiliates[] = $aff;
           }
         }
       
        return implode(",", $affiliates);
    }

    
    public function getOffers(){
        $allowedIntegrations =   \Model\Integrationofficer::fetchAllIntegrationFeeds($this->_affiseadvertiserId) ;             
 
         $curl = \Fuel\Core\Request::forge($this->_url,'curl');
         $curl->set_method('get')  ;
         $curlResult = $curl->execute();
 
        if($curlResult->response()->body == "") {
            exit(0);
        }
         $offersResult=json_decode($curlResult->response()->body); 
        //  print_r($offersResult); exit(0);
        if (isset($offersResult) && (count($offersResult) > 0) ){

            foreach ($offersResult as $_offer){
                 $tmpof = $this->getOfferId($_offer);
                 if(isset($allowedIntegrations)  && array_key_exists($tmpof,$allowedIntegrations) && $allowedIntegrations[$tmpof] == 0) continue;   
                    $offer = new \stdClass();

                    $offer->offerId = $this->getOfferId($_offer);
                    $offer->title = str_replace("-DMG","",$this->getOfferName($_offer));
                    $offer->description = $this->getOfferDescription($_offer);
                    $offer->geos = $this->getOfferGeos($_offer);                    
                    $offer->os = $this->getOfferOs($_offer);
                    $offer->actionRequire = $this->getOfferActionRequire($_offer);
                    $offer->bizModel = $this->getOfferBizModel($_offer);
                    $offer->capType = $this->getOfferCapType($_offer);
                    $offer->cap = $this->getOfferCap($_offer); 
                    $offer->kpi = $this->getOfferKpi($_offer); 
                    $offer->minOsVersion = $this->getOfferMinOsVersion($_offer); 
                    $offer->previewLink = $this->getOfferPreviewLink($_offer);
                    $offer->macros = $this->_macros;
                    $offer->payoutType = $this->getOfferPayoutType($_offer);
                    $offer->payout = $this->getOfferPayout($_offer);
                    $offer->currency = $this->getOfferCurrency($_offer);
                    $offer->trackingLink = $this->getOfferTrackingLink($_offer);
                    $offer->status = "active";
                    $offer->categories = $this->getOfferCategories($_offer);
                    $offer->logo = $this->getOfferLogo($_offer);
                    $offer->affiseAdvertiserId = $this->_affiseadvertiserId;
                    $offer->privacy = "protected";
                    $blacklisted_sources = $this->getBlacklistedSources($_offer);
                    $offer->subAccounts = $blacklisted_sources;
                    if(!empty($blacklisted_sources)){
                        $offer->subAccountsExcept = 1;
                        $offer->subAccountsAffiliates = $this->getBlacklistedAffiliates($_offer);
                       
                    }
                    
                    else  {
                        $offer->subAccountsExcept = 0;
                        $offer->subAccountsAffiliates = '';
                    }
                    $this->_offerIds[$offer->offerId] = $offer->status;
                    $this->_offers[$offer->offerId] = $offer;

                
            }
           
        }
//        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
//        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
        if(count($this->_offerIds) > 0 ) {
        $this->getActiveAdvertiserOffers($this->_affiseadvertiserId); 
        $this->getInActiveAdvertiserOffers($this->_affiseadvertiserId);
           $arrChanged = array();
            foreach ($this->_offerIds as $externalOfferId => $status){
               

                    $neededObject = current(array_filter( $this->_activeAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                    if (isset($neededObject) && is_object($neededObject)){
                        echo count($this->_activeAffiseOffers)."offer exists as active ".$externalOfferId.PHP_EOL;
                         $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                       
                        $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);
                                  if ($tmpArr->status == 1){
                                      foreach($this->_activeAffiseOffers as $key => $value) {
                                          if($value->external_offer_id == $externalOfferId) {
                                           unset($this->_activeAffiseOffers[$key]);
                                          break;
                                          }
                                        };
                                  }
                               }
                                echo count($this->_activeAffiseOffers).PHP_EOL; 
                    }
                    else {
                        
                        $neededObject = current(array_filter( $this->_stoppedAffiseOffers, function ($e)  use ($externalOfferId) { return $e->external_offer_id == $externalOfferId;    }));
                        if (isset($neededObject) && is_object($neededObject)){ 
                              echo "offer exists as inactive ".$externalOfferId.PHP_EOL;
                              $this->checkPaymentChanges($neededObject, $this->_offers[$externalOfferId]->cap, $this->_offers[$externalOfferId]->payout, $arrChanged);
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr,true);
                                   $arr=json_decode(json_encode($neededObject), True);
                                   $tmpArr = $this->_affise->_updateOffer($arr['id'],$data);

                               }
                         }
                         else {
                             echo "create new offer".$externalOfferId.PHP_EOL;
                               $tmpOffer = $this->_offers[$externalOfferId]; 
                               if(isset($tmpOffer) && !empty($tmpOffer)){
                                   $arr=json_decode(json_encode($tmpOffer), True);
                                   $data = $this->CreateAffiseOfferData($arr);
                                   $tmpArr = $this->_affise->_createOffer( $data);
                                }
                               
                             }
                         }
              
                
            }
                 foreach($this->_activeAffiseOffers as $key => $value){
                    $data ="status=stopped";
                    $tmpArr = $this->_affise->_updateOffer($value->id, $data);
                 }
                  print_r($arrChanged);
                  if(count($arrChanged) > 0) $this->saveChanges("app",$arrChanged);
            }
            else    \Fuel\Core\Log::write ("Warning", "one of the arrays is empty\n");
        }
  
        
        
}