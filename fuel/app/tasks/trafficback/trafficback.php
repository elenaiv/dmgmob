<?php

namespace Fuel\Tasks;
use \Fuel\Core\Request as Curl;
use Fuel\Core\Package;
use Platforms\Affise; 
//use Model\Affisetrafficback;
use Fuel\Core\Model;
use \Model\Affisetrafficback;
class TrafficBack {
    private $_offers;
    private $_conversions;
    public function getConversions($id,$country=''){
        echo "conversions for offer id =".$id." from ".date('Y-m-d',strtotime("yesterday"))." to ".date('Y-m-d',strtotime("today"))." country = ".$country.PHP_EOL;
         $limit = 500;
         $page = 1;
         $dbRow =array();
        $from = date('Y-m-d',strtotime("today"));
        if(strlen($country) > 0)
        $data = array(
                "limit" => $limit,
                "page" => $page,
                "date_from" => date('Y-m-d',strtotime("yesterday")),
                "date_to" => date('Y-m-d',strtotime("today")),
                "status"  => array(1),
                "offer"  => array($id),
                "country" => array($country),
            ); 
        else 
         $data = array(
                "limit" => $limit,
                "page" => $page,
                "date_from" => date('Y-m-d',strtotime("yesterday")),
                "date_to" => date('Y-m-d',strtotime("today")),
                "status"  => array(1),
                "offer"  => array($id),
            );            
        $tmpArr = $this->_affise->_getConversions($data);
         return $tmpArr;
    }
    public function getOffers($mode){
        $this->_offers = array();
        $limit = 500;
        $page = 1;
         switch ($mode){
         case 'app':
            $key = "dace404d173c69e86cc84d823d9cfb88aae77475";
             break;
         case 'dmi':
             $key = "39ab3b372f26e65f4caa4f36e953b912d460343b";
             break;
         case 'mob':
             $key = "39ab3b372f26e65f4caa4f36e953b912d460343b";
             break;
     }
                  
echo "start get ".date('h:i:s').PHP_EOL;
        do{
        $data = array(
                "limit" => $limit,
                "page" => $page,
                "API-Key" => $key,
            
            );
            $this->_affise = new \Platforms\Affise($mode);
            
            $tmpArr = $this->_affise->_getAffiliateOffers($data);
            $this->_offers =  array_merge( $this->_offers , $tmpArr->offers);
            echo 'page = '.$page.PHP_EOL;
            if(!isset($tmpArr->pagination->next_page)){
                break;
            }
            $page = $tmpArr->pagination->next_page;
           
         } while(1);
        
               
         echo "finish get ".date('h:i:s').PHP_EOL;         
         foreach($this->_offers as $offer){
             if(!is_object($offer)) continue;
  
             $updateTime =  \Fuel\Core\Date::forge()->format("%m/%d/%Y %H:%M");
  
             $id = $offer->id;
             $status = 'active';
             $payout = $offer->payments[0]->revenue;
             $os = implode(",",$offer->payments[0]->os);
             $devices = implode(",",$offer->payments[0]->devices);
             if(count($offer->countries) > 0 ){
                 foreach($offer->countries as $country){
                    $conv = $this->getConversions($id,$country);
                    $conversions = $conv->pagination->total_count;
                    $dbRow[] = array($mode, $id, $country, $status, $payout, $conversions,$os,$devices, $updateTime);
  
                 } 
             }
            else { 
                $country ='all';
                $conv = $this->getConversions($id);
                $conversions = $conv->pagination->total_count;
                $dbRow[] = array($mode, $id, $country, $status, $payout, $conversions,$os,$devices,$updateTime);
 
            }
 
         }
                      
echo "start db ".date('h:i:s').PHP_EOL;

          \Model\Affisetrafficback::add_new($dbRow);
          \Model\Affisetrafficback::delete($mode,$updateTime);
            
echo "finish db ".date('h:i:s').PHP_EOL;
    }
    public function getConversionsforAff($mode){
        
        $this->_conversions = array();
        $limit = 500;
        $page = 1;
         switch ($mode){
         case 'app':
            $partner = "260";
             break;
         case 'dmi':
             $partner = "1";
             break;
         case 'mob':
             $partner = "1";
             break;
         }
        echo "start get ".date('h:i:s').PHP_EOL;
        do{
        $data = array(
                "limit" => $limit,
                "page" => $page,
                "date_from" => date('Y-m-d',strtotime("yesterday")),
                "date_to" => date('Y-m-d',strtotime("today")),
                "status"  => array(1),
               // "partner"  => array($partner),
  
            ); 
 
            $this->_affise = new \Platforms\Affise($mode);
            
            $tmpArr = $this->_affise->_getConversionsForAff($data);
            $this->_conversions =  array_merge( $this->_conversions , $tmpArr->conversions);
            echo 'page = '.$page.PHP_EOL;
            if(!isset($tmpArr->pagination->next_page)){
                break;
            }
            $page = $tmpArr->pagination->next_page;
           
            
         } while(1);
        
        //  print_r($this->_conversions)   ;  
         echo "finish get ".date('h:i:s').PHP_EOL;    
         $dbRow = array();
         if(!isset($this->_conversions) || count($this->_conversions) == 0) exit(0);
         
         foreach($this->_conversions as $conv){
            $key = $conv->offer_id.$conv->country;
             if(array_key_exists($key, $dbRow) ){
                $dbRow[$key][5] = $dbRow[$key][5] + 1;
             }
             else{
                 $conversions =1;
                 $updateTime =  \Fuel\Core\Date::forge()->format("%m/%d/%Y %H:%M");
                 $os='';
                 if(strstr($conv->os,"Android") != FALSE ) $os="Android";
                 elseif (strstr($conv->os,"iOS") != FALSE ) $os="iOS";
                 
                 $dbRow[$key] = array($mode,$conv->offer_id,$conv->country,$conv->status,$conv->revenue,$conversions,$os,$conv->device,$updateTime);
                
             }
            
         }
     echo "Conversion Rows:".count($dbRow ).PHP_EOL;
     echo "start db ".date('h:i:s').PHP_EOL;

     foreach($dbRow as $row){
          \Model\Affisetrafficback::updateConversions($row[0], $row[2], $row[1], $row[5], $row[8]);
     }
     echo "end db ".date('h:i:s').PHP_EOL;

    }
}
