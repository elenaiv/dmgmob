<?php



namespace Fuel\Tasks;

use Model\Partner;
use Model\Offer;
use Model\Advertiser;
use Model\Subscription;
use Fuel\Core\Package;
use Fuel\Core\Session as Session;
use Platforms\Affise; 
use \Fuel\Core\Request as Curl;

class Mobintegrations {
    public static function run(){

      $partner =  func_get_args();
     
      switch ($partner[0]){
      case "Harrenmedia":
          require  APPPATH.'tasks'.DS.'partners'.DS.'mob'.DS.'harrenmedia.php';
          
          $tmp = new Harrenmedia;
              break;
     case "Lemmonet":
          require  APPPATH.'tasks'.DS.'partners'.DS.'mob'.DS.'lemmonet.php';
          
          $tmp = new Lemmonet;
              break;
      }
       $tmp->getOffers();
       
    }
}
